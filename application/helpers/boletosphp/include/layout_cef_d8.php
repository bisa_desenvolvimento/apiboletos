<?php
header("Content-type: text/html; charset=ISO-8859-1");
?>

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<HTML>
<HEAD>
<TITLE><?php echo $dadosboleto["identificacao"]; ?></TITLE>
<META http-equiv=Content-Type content=text/html charset=ISO-8859-1>
<meta name="Generator" content="Projeto BoletoPHP - www.boletophp.com.br - Licen�a GPL" />
<style type=text/css>
<!--.cp {  font: bold 10px Arial; color: black}
<!--.ti {  font: 9px Arial, Helvetica, sans-serif}
<!--.ld { font: bold 15px Arial; color: #000000}
<!--.ct { FONT: 9px "Arial Narrow"; COLOR: #000033} 
<!--.cn { FONT: 9px Arial; COLOR: black }
<!--.bc { font: bold 20px Arial; color: #000000 }
<!--.ld2 { font: bold 12px Arial; color: #000000 }
--></style> 
</head>

<BODY text=#000000 bgColor=#ffffff topMargin=0 rightMargin=0>
<table width=666 cellspacing=0 cellpadding=0 border=0><tr><td valign=top class=cp><DIV ALIGN="CENTER">Instru��es 
de Impress�o</DIV></TD></TR><TR><TD valign=top class=cp><DIV ALIGN="left">
<p>
<li>Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta (N�o use modo econ�mico).<br>
<li>Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens m�nimas � esquerda e � direita do formul�rio.<br>
<li>Corte na linha indicada. N�o rasure, risque, fure ou dobre a regi�o onde se encontra o c�digo de barras.<br>
<li>Caso n�o apare�a o c�digo de barras no final, clique em F5 para atualizar esta tela.
<li>Caso tenha problemas ao imprimir, copie a seq�encia num�rica abaixo e pague no caixa eletr�nico ou no internet banking:<br><br>
<span class="ld2">
&nbsp;&nbsp;&nbsp;&nbsp;Linha Digit�vel: &nbsp;<?php echo $dadosboleto["linha_digitavel"]?><br>
&nbsp;&nbsp;&nbsp;&nbsp;Valor: &nbsp;&nbsp;R$ <?php echo $dadosboleto["valor_boleto"]?><br>
</span>
</DIV></td></tr></table><br><table cellspacing=0 cellpadding=0 width=666 border=0><TBODY><TR><TD class=ct width=666><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0></TD></TR><TR><TD class=ct width=666><div align=right><b class=cp>
</b></div></TD></tr></tbody></table><table width=666 cellspacing=5 cellpadding=0 border=0><tr><td width=41></TD></tr></table>


<!-- Gleydson Lins -->

<!-- cabe�alho do cliente -->
<table width=666 cellspacing=5 cellpadding=0 border=0 align=Default>
  <tr>
    <td class=cp width=150> 
      <span class="campo">
        <IMG src="<?php echo base_url('assets/boletos'); ?>/imagens/logocaixa.jpg" width="150" height="40" 
      border=0>
      </span>
    </td>
    <td class=ti align=CENTER width=501>Disque CAIXA 0800 726 0101 <br> Ouvidoria CAIXA 0800 725 7474 <br><br>
     
    </td>
    <td align=RIGHT width=50 class=ti>&nbsp;</td>
  </tr>

  <tr>    
    <td class=ti align=CENTER width=666 colspan="3"><h2><?php echo $dadosboleto["identificacao"]; ?></h2> 
     <h3>DOCUMENTO DE ARRECADA��O</h3>
    </td>
  </tr>
</table>
<br>
<!-- fim cabe�alho do cliente -->


<!-- primeira linha do layout -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td colspan=5>
        <img height=2 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=13>NOME</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>N� DE IDENTIFICA��O</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <!-- <td class=cp valign=top width=472 height=12>PREFERENCIALMENTE NAS CASAS LOT�RICAS AT� O VALOR LIMITE</td> -->
      <!-- <td class=ct valign=top width=472 height=12><b>At� o vencimento, preferencialmente nas ag�ncias da CEF e Lot�ricas. Ap�s o vencimento, somente na CEF.<b></td> -->
      <td class=ct valign=top width=472 height=12><b><?php echo $dadosboleto["cedente"]?><b></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12> 
        <span class="campo">
        <?php echo $dadosboleto["nosso_numero"]?>
        </span>
      </td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>
<!-- fim primeira linha do layout -->

<!-- segunda linha do layout -->
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top COLSPAN="3" height=13>DATA DE VALIDADE
    </td>
    <td class=ct valign=top height=13 width=7> 
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=ct valign=top width=83 height=13>DATA DE EMISS�O</td>
    <td class=ct valign=top height=13 width=7> 
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=ct valign=top width=43 height=13></td>
    <td class=ct valign=top height=13 width=7>
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=103 height=13>EXERC�CIO
    </td>
    <td class=ct valign=top height=13 width=7>
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=102 height=13>PARCELA
    </td>
    <td class=ct valign=top width=7 height=13>
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=ct valign=top width=180 height=13>VENCIMENTO
    </td>
  </tr>
  <tr> 
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td valign=top class=cp height=12 COLSPAN="3">
      <div align=left>
       <span class="campo">
        <?php echo $dadosboleto["data_vencimento"]  ?>
        </span> 
      </div>
    </td>
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=83> 
      <div align=left> 
        <span class="campo">
        <?php echo date('d/m/Y') ?>
        </span>
      </div>
    </td>
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=43>
      <div align=left>
        <span class="campo">
        <!-- VALOR CAMPO -->
        </span> 
      </div>
    </td>
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=103>
      <span class="campo">
        <?php echo date('Y') ?>
      </span> 
   </td>
   <td class=cp valign=top width=7 height=12>
    <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top  width=102>
     <span class="campo">
     VALOR PARCELA
     </span>
   </td>
   <td class=cp valign=top width=7 height=12> 
    <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
   </td>
   <td class=cp valign=top align=right width=180 height=12> 
     <span class="campo">
     <?php echo $dadosboleto["data_vencimento"]  ?>
     </span>
   </td>
  </tr>
  <tr>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=75 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=31 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=31 border=0>
    </td>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=83 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=83 border=0>
    </td>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=43 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=43 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=103 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=103 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=102 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=102 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=180 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
    </td>
  </tr>
</tbody> 
</table>
<!-- segunda linha do layout -->
<table cellspacing=0 cellpadding=0 width=666 border=0>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>ENDERE�O
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- &nbsp;<?php echo $dadosboleto["sacado"]; ?><br>&nbsp; -->
              <?php echo $dadosboleto["endereco1"] ." / ".$dadosboleto["endereco2"]; ?>            
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>VALOR
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>
                  R$ <?php echo $dadosboleto["valor_boleto"]?>
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <?php echo $dadosboleto["instrucoes2"]; ?>
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>CORRE��O
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <?php echo $dadosboleto["instrucoes2"]; ?>
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>MULTA
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <?php echo $dadosboleto["instrucoes2"]; ?>
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>JUROS
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>DESCONTO
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>TOTAL
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>
</table>

<!-- linha de corte -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td valign=top width=666 height=1>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>
<TABLE cellSpacing=0 cellPadding=0 border=0 width=666>
  <TBODY>
    <TR>
      <TD class=ct  width=7 height=12></TD>
      <TD class=ct  width=409 ></TD>
      <TD class=ct  width=250 >
        <div align=right> 
          <b class=cp>VIA CLIENTE</b>
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct  colspan=3 ></TD>
    </tr>
  </tbody>
</table>
<BR>
<TABLE cellSpacing=0 cellPadding=0 width=666 border=0>
  <TR>
    <TD class=ct width=666></TD>
  </TR>
  <TBODY>
    <TR>
      <TD class=ct width=666>
        <div align=right>
          Corte na linha pontilhada
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct width=666>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0>
      </TD>
    </tr>
  </tbody>


</table>
<!-- fim linha de corte -->

<br>

<table cellspacing=0 cellpadding=0 width=666 border=0><tr><td class=cp width=150> 
  <span class="campo"><IMG 
      src="<?php echo base_url('assets/boletos'); ?>/imagens/logocaixa.jpg" width="150" height="40" 
      border=0></span></td>

<td class=ti align=CENTER width=555><h2><!-- <?php echo $dadosboleto["identificacao"]; ?> --></h2> 
     <h3>DOCUMENTO DE ARRECADA��O</h3>
    </td>
</tr>

</table>


<!-- primeira linha do layout -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td colspan=5>
        <img height=2 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=13>NOME</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>N� DE IDENTIFICA��O</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <!-- <td class=cp valign=top width=472 height=12>PREFERENCIALMENTE NAS CASAS LOT�RICAS AT� O VALOR LIMITE</td> -->
      <!-- <td class=ct valign=top width=472 height=12><b>At� o vencimento, preferencialmente nas ag�ncias da CEF e Lot�ricas. Ap�s o vencimento, somente na CEF.<b></td> -->
      <td class=ct valign=top width=472 height=12><b><?php echo $dadosboleto["cedente"]?><b></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12> 
        <span class="campo">
        <?php echo $dadosboleto["nosso_numero"]?>
        </span>
      </td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>
<!-- fim primeira linha do layout -->

<!-- segunda linha do layout -->
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top COLSPAN="3" height=13>DATA DE VALIDADE
    </td>
    <td class=ct valign=top height=13 width=7> 
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=ct valign=top width=83 height=13>DATA DE EMISS�O</td>
    <td class=ct valign=top height=13 width=7> 
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=ct valign=top width=43 height=13></td>
    <td class=ct valign=top height=13 width=7>
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=103 height=13>EXERC�CIO
    </td>
    <td class=ct valign=top height=13 width=7>
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=102 height=13>PARCELA
    </td>
    <td class=ct valign=top width=7 height=13>
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=ct valign=top width=180 height=13>VENCIMENTO
    </td>
  </tr>
  <tr> 
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td valign=top class=cp height=12 COLSPAN="3">
      <div align=left> 
      </div>
    </td>
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=83> 
      <div align=left> 
        <span class="campo">
        <?php echo date('d/m/Y') ?>
        </span>
      </div>
    </td>
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=43>
      <div align=left>
        <span class="campo">
        <!-- <?php echo $dadosboleto["especie"]?> -->
        </span> 
      </div>
    </td>
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=103>
      <span class="campo">
        <?php echo date('Y') ?>
      </span> 
   </td>
   <td class=cp valign=top width=7 height=12>
    <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top  width=102>
     <span class="campo">
     VALOR PARCELA
     </span>
   </td>
   <td class=cp valign=top width=7 height=12> 
    <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
   </td>
   <td class=cp valign=top align=right width=180 height=12> 
     <span class="campo">
     <?php echo $dadosboleto["data_vencimento"]  ?>
     </span>
   </td>
  </tr>
  <tr>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=75 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=31 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=31 border=0>
    </td>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=83 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=83 border=0>
    </td>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=43 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=43 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=103 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=103 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=102 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=102 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=180 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
    </td>
  </tr>
</tbody> 
</table>
<!-- segunda linha do layout -->
<table cellspacing=0 cellpadding=0 width=666 border=0>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>ENDERE�O
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- &nbsp;<?php echo $dadosboleto["sacado"]; ?><br>&nbsp; -->
              <?php echo $dadosboleto["endereco1"] ." / ".$dadosboleto["endereco2"]; ?>             
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>VALOR
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- - -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>
                  R$ <?php echo $dadosboleto["valor_boleto"]?>
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>CORRE��O
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>MULTA
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>JUROS
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>DESCONTO
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <!-- <?php echo $dadosboleto["instrucoes2"]; ?> -->
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

        <td align=right width=98>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13>
                  <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=98 height=13>TOTAL
                </td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12>
                  <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=98 height=12><!-- 123456 -->
                </td>
              </tr>   
              <tr> 
                <td valign=top width=7 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
                </td>
                <td valign=top width=98 height=1>
                  <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=98 border=0>
                </td>
              </tr>         
            </tbody>
        </table>
      </td>
      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13><!-- (-) 
                Desconto / Abatimentos -->
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>123456
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>
</table>

<!-- linha de corte -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td valign=top width=666 height=1>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>
<TABLE cellSpacing=0 cellPadding=0 border=0 width=666>
  <TBODY>
    <TR>
      <TD class=ct  width=7 height=12></TD>
      <TD class=ct  width=409 ></TD>
      <TD class=ct  width=250 >
        <div align=right> 
          <b class=cp>VIA BANCO</b>
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct  colspan=3 ></TD>
    </tr>
  </tbody>
</table>
<BR>

  <TABLE cellSpacing=0 cellPadding=0 border=0 width=666><TBODY><TR><TD class=ct  width=7 height=12></TD><TD class=ct  width=666 ><h2><?php echo $dadosboleto["linha_digitavel"]?></h2></TD>
  </TR><TR><TD class=ct  colspan=3 ></TD></tr></tbody></table><TABLE cellSpacing=0 cellPadding=0 width=666 border=0><TBODY><TR><TD vAlign=bottom align=left height=50><?php fbarcode($dadosboleto["codigo_barras"]); ?> 
 </TD>
</tr></tbody></table>

<TABLE cellSpacing=0 cellPadding=0 width=666 border=0>
  <TR>
    <TD class=ct width=666></TD>
  </TR>
  <TBODY>
    <TR>
      <TD class=ct width=666>
        <div align=right>
          Corte na linha pontilhada
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct width=666>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0>
      </TD>
    </tr>
  </tbody>


</table>
<!-- fim linha de corte -->



<div style="page-break-after: always;page-break-inside: avoid;" /></div>
</BODY></HTML>