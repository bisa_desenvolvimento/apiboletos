﻿<div class="widget-box">

    <div class="widget-title">

        <ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#tab1">Dados do Cliente</a></li>

            <li><a data-toggle="tab" href="#tab2">Ordens de Serviço</a></li>

            <li><a data-toggle="tab" href="#tab3">Localização</a></li>

            <div class="buttons">

                    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'eCliente')){

                        echo '<a title="Icon Title" class="btn btn-mini btn-info" href="'.base_url().'index.php/clientes/editar/'.$result->idClientes.'"><i class="icon-pencil icon-white"></i> Editar</a>'; 

                    } ?>

                    

            </div>

        </ul>

    </div>

    <div class="widget-content tab-content">

        <div id="tab1" class="tab-pane active" style="min-height: 300px">



            <div class="accordion" id="collapse-group">

                            <div class="accordion-group widget-box">

                                <div class="accordion-heading">

                                    <div class="widget-title">

                                        <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse">

                                            <span class="icon"><i class="icon-list"></i></span><h5>Dados Pessoais</h5>

                                        </a>

                                    </div>

                                </div>

                                <div class="collapse in accordion-body" id="collapseGOne">

                                    <div class="widget-content">

                                        <table class="table table-bordered">

                                            <tbody>

                                                <tr>

                                                    <td style="text-align: right; width: 30%"><strong>Nome</strong></td>

                                                    <td><?php echo $result->nomeCliente ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>CPF/CNPJ</strong></td>

                                                    <td><?php echo $result->documento ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>RG/IE</strong></td>

                                                    <td><?php echo $result->rgie ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>Data de Cadastro</strong></td>

                                                    <td><?php echo date('d/m/Y',  strtotime($result->dataCadastro)) ?></td>

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>

                            <div class="accordion-group widget-box">

                                <div class="accordion-heading">

                                    <div class="widget-title">

                                        <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">

                                            <span class="icon"><i class="icon-list"></i></span><h5>Contatos</h5>

                                        </a>

                                    </div>

                                </div>

                                <div class="collapse accordion-body" id="collapseGTwo">

                                    <div class="widget-content">

                                        <table class="table table-bordered">

                                            <tbody>

                                                <tr>

                                                    <td style="text-align: right; width: 30%"><strong>Telefone</strong></td>

                                                    <td><?php echo $result->telefone ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>Celular</strong></td>

                                                    <td><?php echo $result->celular ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>Email</strong></td>

                                                    <td><?php echo $result->email ?></td>

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>

                            <div class="accordion-group widget-box">

                                <div class="accordion-heading">

                                    <div class="widget-title">

                                        <a data-parent="#collapse-group" href="#collapseGThree" data-toggle="collapse">

                                            <span class="icon"><i class="icon-list"></i></span><h5>Endereço</h5>

                                        </a>

                                    </div>

                                </div>

                                <div class="collapse accordion-body" id="collapseGThree">

                                    <div class="widget-content">

                                        <table class="table table-bordered">

                                            <tbody>

                                                <tr>

                                                    <td style="text-align: right; width: 30%"><strong>Rua</strong></td>

                                                    <td><?php echo $result->rua ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>Número</strong></td>

                                                    <td><?php echo $result->numero ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>Bairro</strong></td>

                                                    <td><?php echo $result->bairro ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>Cidade</strong></td>

                                                    <td><?php echo $result->cidade ?> - <?php echo $result->estado ?></td>

                                                </tr>

                                                <tr>

                                                    <td style="text-align: right"><strong>CEP</strong></td>

                                                    <td><?php echo $result->cep ?></td>

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>

                        </div>







          

        </div>





        <!--Tab 2-->

        <div id="tab2" class="tab-pane" style="min-height: 300px">

            <?php if (!$results) { ?>

                

                        <table class="table table-bordered ">

                            <thead>

                                <tr style="backgroud-color: #2D335B">

                                    <th>#</th>
                                    <th>Data Inicial</th>
                                    <th>Data Final</th>
                                    <th>Equipamento</th>
                                    <th>Status</th>

                                    <th></th>

                                </tr>

                            </thead>

                            <tbody>



                                <tr>
                                    <td colspan="6">Nenhuma OS Cadastrada</td>
                                </tr>

                            </tbody>

                        </table>

                

                <?php } else { ?>





              



                        <table class="table table-bordered ">

                            <thead>

                                <tr style="backgroud-color: #2D335B">

                                    <th>#</th>
                                    <th>Data Inicial</th>
                                    <th>Data Final</th>
                                    <th>Equipamento</th>
                                    <th>Status</th>

                                    <th></th>

                                </tr>

                            </thead>

                            <tbody>
<?php
                foreach ($results as $r) {
                    $dataInicial = date(('d/m/Y'), strtotime($r->dataInicial));
                    $dataFinal = date(('d/m/Y'), strtotime($r->dataFinal));
                    echo '<tr>';
                    echo '<td>' . $r->idOs. '</td>';
                    echo '<td>' . $dataInicial. '</td>';
                    echo '<td>' . $dataFinal. '</td>';
                    echo '<td>' . $r->equi_nome. '</td>';
                    echo '<td>' . $r->status. '</td>';

                    echo '<td>';
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){
                        echo '<a href="' . base_url() . 'index.php/os/visualizar/' . $r->idOs . '" style="margin-right: 1%" class="btn tip-top" title="Ver mais detalhes"><i class="icon-eye-open"></i></a>'; 
                    }
                    if($this->permission->checkPermission($this->session->userdata('permissao'),'eOs')){
                        echo '<a href="' . base_url() . 'index.php/os/editar/' . $r->idOs . '" class="btn btn-info tip-top" title="Editar OS"><i class="icon-pencil icon-white"></i></a>'; 
                    }
                    
                    echo  '</td>';
                    echo '</tr>';
                } ?>


                            <tr>



                            </tr>

                        </tbody>


                    </table>

            



            <?php  } ?>



        </div>



        <!--Tab 3-->

        <div id="tab3" class="tab-pane" style="min-height: 300px">

            <div class="span12 alert alert-info">Funcionalidade em període de teste verifique se as informação do mapas estão corretas.</div>

            <script src="http://maps.google.com/maps?file=api&v=2.x&key=ABQIAAAAVr5mPPLDy_bfjIr5EGw_YRQJJTmMFgh90lBDn52esDHY_5eerhR4K2UH2MlX9dgLKYgdvssFxiowiQ"
                        type="text/javascript"></script><br />
            <br  /><br />
            <center><h4 id="distancia"></h4><br><div id="mapa" style="z-index: 102; width: 900px; height: 400px; "> </div><br /></center>
            <br />
            <script language="javascript">
            var map;
            var dados    = "<?php echo $dadosMap['lat'].",".$dadosMap['long']; ?>";
            var emitente = "<?php echo $dadosMapEmitente['lat'].",".$dadosMapEmitente['long']; ?>";
            
        
            function initialize()
            {
               // Carrega o Google Maps
                if (GBrowserIsCompatible()) {
                        map = new GMap2(document.getElementById("mapa"));
                        map.setCenter(new GLatLng(emitente), 12);
                        
                        

                        // Cria o objeto de roteamento
                         var dir = new GDirections(map);
                         var pt1 = emitente;
                         var pt2 = dados;
                         
                        // Carrega os pontos dados os endereços
                        dir.loadFromWaypoints([pt1,pt2], {locale:"pt-br", getSteps:true});
                        // O evento load do GDirections é executado quando chega o resultado do geocoding.
                        GEvent.addListener(dir,"load", function() {
                            for (var i=0; i<dir.getNumRoutes(); i++) {
                                      var route = dir.getRoute(i);
                                      var dist = route.getDistance();
                                      map.setZoom(6);
                                      $('#distancia').text("Distância de: " + (dist.meters/1000) + " km");//alert("Distância é de: " + dist.meters + " metros")
                             }
                       });

                    }
            }

            </script>

            <body onload="initialize()">

            </body>




        </div>

    </div>

</div>