<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Welcome_model');
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * [index description]Redireciona para a página do login
	 * @return [type] [description]
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
}
