<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_modulos extends CI_Migration {

	public function up() {

		//session
		$this->dbforge->add_field(array(
			'id'     => array(
								'type' => 'VARCHAR',
								'null' => FALSE,
								'constraint' => 40			
							),
			'ip_address'   => array(
								'type' => 'VARCHAR',
								'null' => FALSE,
								'constraint' => 45
						    ),
			'timestamp'    => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 10,
								'unsigned' => TRUE,
								'default' => 0
							),
			'data'    => array(
								'type' => 'BLOB',
								'null' => TRUE				
							)
		));
		
		$this->dbforge->add_field('KEY `ci_sessions_timestamp` (`timestamp`)');		
		$this->dbforge->create_table('ci_sessions');



		$this->dbforge->add_field(array(
			'API_codigo'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 50,
								'auto_increment' => true				
							),
			'CLIE_codigo'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 20			
							),
			'SIST_Sigla'     => array(
								'type' => 'CHAR',
								'null' => FALSE,
								'constraint' => 3,
								'default' => ''		
							),			
			'BANC_Codigo'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 4,
								'default' => ''
							),		
			'BANC_CodigoBanco'     => array(
								'type' => 'CHAR',
								'null' => TRUE,
								'constraint' => 3,
								'default' => ''
							),	
			'BOLE_Sequencial'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 25
							),	
			'BOLE_Titulo'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 50,
								'default' => ''
							),	
			'BOLE_Leiaute'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 10
							),	
			'BOLE_TipoBoleto'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 10,
							),	
			'MODE_AnoCompetencia'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 11
							),	
			'BOLE_TipoBoleto'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 10
							),	
			'BOLE_ChaveTabela'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 255
							),	
			'BOLE_ChaveCampos'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 255
							),	
			'BOLE_ChaveValores'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 255
							),	
			'BOLE_ValorDocumento'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),	
			'BOLE_Vencimento'     => array(
								'type' => 'DATE',
								'null' => TRUE
							),	
			'BOLE_DataDoc'     => array(
								'type' => 'DATE',
								'null' => TRUE
							),	
			'BOLE_NumDoc'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 20
							),
			'BOLE_Aceite'     => array(
								'type' => 'CHAR',
								'null' => TRUE,
								'constraint' => 1
							),
			'BOLE_DataProc'     => array(
								'type' => 'DATE',
								'null' => TRUE,
							),
			'BOLE_Qtd'     => array(
								'type' => 'CHAR',
								'null' => TRUE,
								'constraint' => 1
							),
			'BOLE_Valor'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'BOLE_Desconto'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'BOLE_Deducao'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'BOLE_Multa'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'BOLE_Acrescimo'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'BOLE_ValCobrado'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'FILI_Sequencial'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 11
							),
			'BOLE_MatriculaCedente'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 6
							),
			'BOLE_NomeCliente'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BOLE_EndeCliente'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			
			'BOLE_Bairro'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 50
							),
			
			'BOLE_CGC'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 20
							),
			'BOLE_Cidade'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 50
							),
			'BOLE_UF'     => array(
								'type' => 'CHAR',
								'null' => TRUE,
								'constraint' => 2
							),
			'BOLE_CEP'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 9
							),

			'BOLE_InstrucaoBoleta'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 250
							),
			'BOLE_Lote'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 11
							),
			'BOLE_Ocorrencias'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 20
							),
			'BOLE_Status'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 20
							),
			'CONT_Codigo'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 11
							),
			'TAXA_Codigo'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 2
							),
			'TAXA_Descricao'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 40
							),
			'BOLE_Agencia'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 11
							),
			'BOLE_DigAgencia'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 1
							),
			'BOLE_Conta'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 15
							),
			'BOLE_DigConta'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 1
							),
			'BOLE_NumCarteira'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 2
							),
			'BOLE_NumeroConvenio'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 20
							),
			'BOLE_LocalPagamento1'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BOLE_LocalPagamento2'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BISAUSUA_LastUpdate'     => array(
								'type' => 'DATETIME',
								'null' => TRUE
							),			
			'BISAUSUA_Sequencial'     => array(
								'type' => 'CHAR',
								'null' => TRUE,
								'constraint' => 3
							),
			'STATUS_boleto'     => array(
								'type' => 'int',
								'null' => TRUE,
								'constraint' => 3
							)							,
			'BOLE_CodigoBarras'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BOLE_LinhaDigitavel'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BOLE_AgenciaCedente'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 30
							),
			'BOLE_DescCarteira'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 10
							),
			'BOLE_NomeCedente'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BOLE_CNPJCPFCedente'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 20
							),
			'BOLE_EspecieDoc'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 10
							),
			'BOLE_OutrosDadosSacado'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 100
							),
			'BOLE_UsoBanco'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 50
							),
			'BOLE_QtdUnid'     => array(
								'type' => 'INT',
								'null' => TRUE,
								'constraint' => 11
							),
			'BOLE_ValorUnid'     => array(
								'type' => 'DECIMAL',
								'null' => TRUE,
								'constraint' => 10.2
							),
			'BANC_CodigoBancoComDigito'     => array(
								'type' => 'VARCHAR',
								'null' => TRUE,
								'constraint' => 10.2
							)
			));

			$this->dbforge->add_key('API_codigo');	
			$this->dbforge->create_table('abombole');


		
	}

	public function down() {
		$this->dbforge->drop_table('ci_sessions');
		$this->dbforge->drop_table('abombole');
	}

}