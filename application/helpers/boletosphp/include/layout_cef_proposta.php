<?php
header("Content-type: text/html; charset=ISO-8859-1");
?>

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<HTML>
<HEAD>
<TITLE><?php echo $dadosboleto["identificacao"]; ?></TITLE>
<META http-equiv=Content-Type content=text/html charset=ISO-8859-1>
<meta name="Generator" content="Projeto BoletoPHP - www.boletophp.com.br - Licença GPL" />
<style type=text/css>
<!--.cp {  font: bold 10px Arial; color: black}
<!--.ti {  font: 9px Arial, Helvetica, sans-serif}
<!--.ld { font: bold 15px Arial; color: #000000}
<!--.ct { FONT: 9px "Arial Narrow"; COLOR: #000033} 
<!--.cn { FONT: 9px Arial; COLOR: black }
<!--.bc { font: bold 20px Arial; color: #000000 }
<!--.ld2 { font: bold 12px Arial; color: #000000 }
--></style> 
</head>

<BODY text=#000000 bgColor=#ffffff topMargin=0 rightMargin=0>
<table width=666 cellspacing=0 cellpadding=0 border=0><tr><td valign=top class=cp><DIV ALIGN="CENTER">Instruções 
de Impressão</DIV></TD></TR><TR><TD valign=top class=cp><DIV ALIGN="left">
<p>
<!-- <li>Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta (Não use modo econômico).<br>
<li>Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens mínimas à esquerda e à direita do formulário.<br>
<li>Corte na linha indicada. Não rasure, risque, fure ou dobre a região onde se encontra o código de barras.<br>
<li>Caso não apareça o código de barras no final, clique em F5 para atualizar esta tela.
<li>Caso tenha problemas ao imprimir, copie a seqüencia numérica abaixo e pague no caixa eletrônico ou no internet banking:<br><br> -->
<span class="ld2">
&nbsp;&nbsp;&nbsp;&nbsp;Linha Digitável: &nbsp;<?php echo $dadosboleto["linha_digitavel"]?><br>
&nbsp;&nbsp;&nbsp;&nbsp;Valor: &nbsp;&nbsp;R$ <?php echo $dadosboleto["valor_boleto"]?><br>
</span>
</DIV></td></tr></table><br>

<table cellspacing=0 cellpadding=0 width=666 border=0><TBODY><TR><TD class=ct width=666><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0></TD></TR><TR><TD class=ct width=666><div align=right><b class=cp>
</b></div></TD></tr></tbody>
</table>
<table width=666 cellspacing=5 cellpadding=0 border=0><tr><td width=41></TD></tr>
</table>


<!-- Bruno Magnata -->

<!-- cabeçalho do cliente -->



<br>
<!-- fim cabeçalho do cliente -->

<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tr>
    <td class=cp width=150> 
    <span class="campo"><IMG 
        src="<?php echo base_url('assets/boletos'); ?>/imagens/logocaixa.jpg" width="150" height="40" 
        border=0></span>
    </td>
    <td width=3 valign=bottom>
      <img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0>
    </td>
    <td class=cpt width=58 valign=bottom>
      <div align=center>
        <font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"]?></font>
      </div>
    </td>
    <td width=3 valign=bottom>
      <img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0>
    </td>
    <td class=ld align=right width=453 valign=bottom>
      <span class=ld> 
        <span class="campotitulo">
          <?php echo $dadosboleto["linha_digitavel"]?>
        </span>
      </span>
    </td>
  </tr>
  <tbody>
    <tr>  
      <td colspan=5><img height=2 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0></td>  
    </tr>
  </tbody>
</table>

<!-- Magnata Begin -->
<table cellspacing=0 cellpadding=0 width=666 border=0>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
      
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct><div align=center><h1>BOLETO DE PROPOSTA</h1></div>
        </font>
        <span class=cp> 
          <FONT class=campo>
              <DIV ALIGN="left">
                <p>
                <spam>ESTE BOLETO SE REFERE A UMA PROPOSTA J&Agrave; FEITA A VOC&Ecirc; E O SEU PAGAMENTO N&Acirc;O OBRIGAT&Oacute;RIO.<br>
                <spam>Deixar de pagá-lo não dará causa a protesto, a cobrança judicial ou extrajudicial, nem a inserção de nome em cadastro de restrição ao crédito.<br>
                <spam>Pagar até a data de vecimento significa aceitar a proposta.<br>
                <spam>informações adicionais sobre a proposta e sobre o respectivo contrato poderão ser solicitadas a qualquer momento ao
                <spam>beneficiário, por meio de seus canais de atendimento.<br><br>             
              </DIV>          
          </FONT> 
        </span>
      </td>
    </tr>
  </tbody>

</table>
<!-- Magnata End -->

<!-- primeira linha do layout -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td colspan=5>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=13>Local de Pagamento</td>
      
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>

      <td class=ct valign=top width=472 height=12><b>TEXTO ???<b></td>

    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>

<!-- fim primeira linha do layout -->

<!-- segunda linha do layout -->

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=13>Nome do Beneficiário CNPJ/CPF</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>Data de Vencimento</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=12><b><?php echo $dadosboleto["cedente"]." CNPJ/CPF: ".$dadosboleto["cpf_cnpj"]?><b></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12> 
        <span class="campo">
        <?php echo $dadosboleto["data_vencimento"]  ?>
        </span>
      </td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>

<!-- fim segunda linha do layout -->

<!-- terceira linha do layout -->
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top COLSPAN="3" height=13>Data do Processamento</td>
      
      <td class=ct valign=top height=13 width=7> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=83 height=13>Nr do Documento</td>
     
      <td class=ct valign=top height=13 width=7>
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=102 height=13>Nosso-Número</td>
    
      <td class=ct valign=top height=13 width=7>
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=103 height=13>Agencia/Cod. Beneficiário</td>
      
     <td class=ct valign=top height=13 width=7> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=43 height=13>Carteira</td>   
      
      <td class=ct valign=top width=7 height=13>
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=180 height=13>Valor do Documento</td>

    </tr>

  <tr> 
    
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td valign=top class=cp height=12 COLSPAN="3">
      <div align=left>
       <span class="campo">
        <?php echo $dadosboleto["data_processamento"]?>
        </span> 
      </div>
    </td>
    
    <td class=cp valign=top width=7 height=12> 
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=83> 
      <div align=left> 
        <span class="campo">
        <?php echo $dadosboleto["numero_documento"]?>
        </span>
      </div>
    </td>
    
    <td class=cp valign=top width=7 height=12><!--CARTEIRA-->
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=102>
      <span class="campo">
      <?php echo $dadosboleto["nosso_numero"]?>
      </span>
    </td>
    
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=103>
      <span class="campo">
        <?php echo $dadosboleto["agencia_codigo"]?>
      </span> 
   </td>


       <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=43>
      <div align=left>
        <span class="campo">
        <?php echo $dadosboleto["carteira"] ?>
        </span> 
      </div>
    </td>
   

  
   <td class=cp valign=top width=7 height=12> 
    <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
   </td>
   <td class=cp valign=top align=right width=180 height=12> 
     <span class="campo">
     R$ <?php echo $dadosboleto["valor_boleto"]?>
     </span>
   </td>

  </tr>


  <tr>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=75 border=0>
    </td>
    
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=31 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=31 border=0>
    </td>
    
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=83 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=83 border=0>
    </td>
    <!--N NUMERO-->
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=102 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=102 border=0>
    </td>
    
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=103 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=103 border=0>
    </td>
    <!--CARTEIRA-->
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=43 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=43 border=0>
    </td>
    

    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=180 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
    </td>
  </tr>
</tbody> 
</table>
<!-- fim terceira linha do layout -->

<!-- quarta linha do layout -->

<table cellspacing=0 cellpadding=0 width=666 border=0>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>Informações de responsabilidade do beneficiário
        </font><br>
        <span class=cp> 
          <FONT class=campo>
              <?php echo $dadosboleto["instrucoes1"]; ?>
              <?php echo $dadosboleto["instrucoes2"]; ?>
              <?php echo $dadosboleto["instrucoes3"]; ?>
              <?php echo $dadosboleto["instrucoes4"]; ?>          
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(-) 
                Desconto / Abatimentos
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>
                  <!--Desconto-->
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
          <!--TESTE-->            
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(=) Valor Cobrado
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12><!-- 123456 -->
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    </tr>
  </tbody>

  <!-- primeira campo cpf/cnpj/end -->
  <table cellspacing=0 cellpadding=0 width=666 border=0>
    <tbody>
      <tr>
        <td colspan=5>
          <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
        </td>
      </tr>
    </tbody>
  </table>

  <table cellspacing=0 cellpadding=0 border=0>
    <tbody>
      <tr>
        <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        <td class=ct valign=top width=472 height=13>Pagador/CPF/CNPJ/endereço</td>       
      </tr>
      <tr>
        <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>

        <td class=ct valign=top width=472 height=12><b>
               <?php echo $dadosboleto["sacado"]?>
              <?php echo $dadosboleto["endereco1"]?>
              <?php echo $dadosboleto["endereco2"]?><b></td>

      </tr>
      <tr> 
        <td class=cp valign=top width=7 height=12>
          <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=left width=180 height=12>Sacado/avalista
        </td>
      </tr> 
      <tr>
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
      </tr>
    </tbody>
  </table>

<!-- fim campo cpf/cnpj/end-->


<!-- linha de corte -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td valign=top width=666 height=1>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>
<TABLE cellSpacing=0 cellPadding=0 border=0 width=666>
  <TBODY>
    <TR>
      <TD class=ct  width=7 height=12></TD>
      <TD class=ct  width=409 ></TD>
      <TD class=ct  width=250 >
        <div align=right> 
          <b class=cp>VIA CLIENTE</b>
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct  colspan=3 ></TD>
    </tr>
  </tbody>
</table>
<BR>
<TABLE cellSpacing=0 cellPadding=0 width=666 border=0>
  <TR>
    <TD class=ct width=666></TD>
  </TR>
  <TBODY>
    <TR>
      <TD class=ct width=666>
        <div align=right>
          Corte na linha pontilhada
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct width=666>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0>
      </TD>
    </tr>
  </tbody>


</table>
<!-- fim linha de corte -->

<br>

<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tr>
    <td class=cp width=150> 
    <span class="campo"><IMG 
        src="<?php echo base_url('assets/boletos'); ?>/imagens/logocaixa.jpg" width="150" height="40" 
        border=0></span>
    </td>
    <td width=3 valign=bottom>
      <img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0>
    </td>
    <td class=cpt width=58 valign=bottom>
      <div align=center>
        <font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"]?></font>
      </div>
    </td>
    <td width=3 valign=bottom>
      <img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0>
    </td>
    <td class=ld align=right width=453 valign=bottom>
      <span class=ld> 
        <span class="campotitulo">
          <?php echo $dadosboleto["linha_digitavel"]?>
        </span>
      </span>
    </td>
  </tr>
  <tbody>
    <tr>  
      <td colspan=5><img height=2 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0></td>  
    </tr>
  </tbody>
</table>

<!-- Magnata Begin -->
<table cellspacing=0 cellpadding=0 width=666 border=0>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
      
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct><div align=center><h1>BOLETO DE PROPOSTA</h1></div>
        </font>
        <span class=cp> 
          <FONT class=campo>
              <DIV ALIGN="left">
                <p>
                <spam>ESTE BOLETO SE REFERE A UMA PROPOSTA JÀ FEITA A VOCÊ E O SEU PAGAMENTO NÂO OBRIGATÓRIO.<br>
                <spam>Deixar de pagá-lo não dará causa a protesto, a cobrança judicial ou extrajudicial, nem a inserção de nome em cadastro de restrição ao crédito.<br>
                <spam>Pagar até a data de vecimento significa aceitar a proposta.<br>
                <spam>informações adicionais sobre a proposta e sobre o respectivo contrato poderão ser solicitadas a qualquer momento ao
                <spam>beneficiário, por meio de seus canais de atendimento.<br><br>             
              </DIV>          
          </FONT> 
        </span>
      </td>
    </tr>
  </tbody>

</table>
<!-- Magnata End -->

<!-- primeira linha do layout -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td colspan=5>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=13>Local de Pagamento</td>
      
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>

      <td class=ct valign=top width=472 height=12><b>TEXTO ???<b></td>

    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>

<!-- fim primeira linha do layout -->

<!-- segunda linha do layout -->

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=13>Nome do Beneficiário CNPJ/CPF</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>Data de Vencimento</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=472 height=12><b><?php echo $dadosboleto["cedente"]." CNPJ/CPF: ".$dadosboleto["cpf_cnpj"]?><b></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12> 
        <span class="campo">
        <?php echo $dadosboleto["data_vencimento"]  ?>
        </span>
      </td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>

<!-- fim segunda linha do layout -->

<!-- terceira linha do layout -->
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top COLSPAN="3" height=13>Data do Processamento</td>
      
      <td class=ct valign=top height=13 width=7> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=83 height=13>Nr do Documento</td>
     
      <td class=ct valign=top height=13 width=7>
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=102 height=13>Nosso-Número</td>
    
      <td class=ct valign=top height=13 width=7>
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=103 height=13>Agencia/Cod. Beneficiário</td>
      
     <td class=ct valign=top height=13 width=7> 
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=43 height=13>Carteira</td>   
      
      <td class=ct valign=top width=7 height=13>
        <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
      </td>
      <td class=ct valign=top width=180 height=13>Valor do Documento</td>

    </tr>

  <tr> 
    
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td valign=top class=cp height=12 COLSPAN="3">
      <div align=left>
       <span class="campo">
        <?php echo $dadosboleto["data_processamento"]?>
        </span> 
      </div>
    </td>
    
    <td class=cp valign=top width=7 height=12> 
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=83> 
      <div align=left> 
        <span class="campo">
        <?php echo $dadosboleto["numero_documento"]?>
        </span>
      </div>
    </td>
    
    <td class=cp valign=top width=7 height=12><!--CARTEIRA-->
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=102>
      <span class="campo">
      <?php echo $dadosboleto["nosso_numero"]?>
      </span>
    </td>
    
    <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=103>
      <span class="campo">
        <?php echo $dadosboleto["agencia_codigo"]?>
      </span> 
   </td>


       <td class=cp valign=top width=7 height=12>
      <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
    </td>
    <td class=cp valign=top  width=43>
      <div align=left>
        <span class="campo">
        <?php echo $dadosboleto["carteira"] ?>
        </span> 
      </div>
    </td>
   

  
   <td class=cp valign=top width=7 height=12> 
    <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
   </td>
   <td class=cp valign=top align=right width=180 height=12> 
     <span class="campo">
     R$ <?php echo $dadosboleto["valor_boleto"]?>
     </span>
   </td>

  </tr>


  <tr>
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=75 border=0>
    </td>
    
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=31 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=31 border=0>
    </td>
    
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=83 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=83 border=0>
    </td>
    <!--N NUMERO-->
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=102 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=102 border=0>
    </td>
    
    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=103 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=103 border=0>
    </td>
    <!--CARTEIRA-->
    <td valign=top width=7 height=1> 
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=43 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=43 border=0>
    </td>
    

    <td valign=top width=7 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
    </td>
    <td valign=top width=180 height=1>
      <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
    </td>
  </tr>
</tbody> 
</table>


<!-- fim terceira linha do layout -->

<!-- quarta linha do layout -->

<table cellspacing=0 cellpadding=0 width=666 border=0>

  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>Informações de responsabilidade do beneficiário
        </font><br>
        <span class=cp> 
          <FONT class=campo>   
              <?php echo $dadosboleto["instrucoes1"]; ?>
              <?php echo $dadosboleto["instrucoes2"]; ?>
              <?php echo $dadosboleto["instrucoes3"]; ?>
              <?php echo $dadosboleto["instrucoes4"]; ?>    
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(-) 
                Desconto / Abatimentos
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12>
                  <!--Desconto-->
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    <!-- fim duas colunas a esquerda -->
    </tr>
  </tbody>

    <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr> 
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=350 rowspan=6>
        <font class=ct>
        </font><br>
        <span class=cp> 
          <FONT class=campo>
 
          </FONT> 
        </span>
      </td>

  <!-- duas colunas a esquerda -->

      <td align=right width=175>
        <table cellspacing=0 cellpadding=0 border=0>
          <tbody>
            <tr>
              <td class=ct valign=top width=7 height=13>
                <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(=) Valor Cobrado
              </td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12>
                <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12><!-- 123456 -->
              </td>
            </tr>   
            <tr> 
              <td valign=top width=7 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=7 border=0>
              </td>
              <td valign=top width=180 height=1>
                <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/.png width=180 border=0>
              </td>
            </tr>         
          </tbody>
        </table>
      </td>

    </tr>
  </tbody>

  <!-- primeira campo cpf/cnpj/end -->
  <table cellspacing=0 cellpadding=0 width=666 border=0>
    <tbody>
      <tr>
        <td colspan=5>
          <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
        </td>
      </tr>
    </tbody>
  </table>

  <table cellspacing=0 cellpadding=0 border=0>
    <tbody>
      <tr>
        <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        <td class=ct valign=top width=472 height=13>Pagador/CPF/CNPJ/endereço</td>       
      </tr>
      <tr>
        <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>

        <td class=ct valign=top width=472 height=12><b>
              <?php echo $dadosboleto["sacado"]?>
              <?php echo $dadosboleto["endereco1"]?>
              <?php echo $dadosboleto["endereco2"]?>
               <b></td>

      </tr>
      <tr> 
        <td class=cp valign=top width=7 height=12>
          <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=left width=180 height=12>Sacado/avalista
        </td>
      </tr> 
      <tr>
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
      </tr>
    </tbody>
  </table>

<!-- fim campo cpf/cnpj/end-->


<!-- linha de corte -->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td valign=top width=666 height=1>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0>
      </td>
    </tr>
  </tbody>
</table>
<TABLE cellSpacing=0 cellPadding=0 border=0 width=666>
  <TBODY>
    <TR>
      <TD class=ct  width=7 height=12></TD>
      <TD class=ct  width=409 ></TD>
      <TD class=ct  width=250 >
        <div align=right> 
          <b class=cp>VIA BANCO</b>
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct  colspan=3 ></TD>
    </tr>
  </tbody>
</table>
<BR>

  <table cellSpacing=0 cellPadding=0 width=666 border=0>
    <tbody>
      <tr>
        <td vAlign=bottom align=left height=50><?php fbarcode($dadosboleto["codigo_barras"]); ?></td>
      </tr>
    </tbody>
  </table>




<TABLE cellSpacing=0 cellPadding=0 width=666 border=0>
  <TR>
    <TD class=ct width=666></TD>
  </TR>
  <TBODY>
    <TR>
      <TD class=ct width=666>
        <div align=right>
          Corte na linha pontilhada
        </div>
      </TD>
    </TR>
    <TR>
      <TD class=ct width=666>
        <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0>
      </TD>
    </tr>
  </tbody>


</table>
<!-- fim linha de corte -->


<div style="page-break-after: always;page-break-inside: avoid;" /></div>
</BODY></HTML>