<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Boletos_model extends CI_Model {

	private $tabela = TABELACLIENTE;
	private $chave  = "API_codigo";

	public function cadastrarBoletos($data)
	{
		$this->db->insert($this->tabela, $data);

		if ($this->db->affected_rows() == '1')
		{		
			return $this->db->insert_id();
		}
		
		return FALSE; 
	}

	public function pegarBoletoId($data)
	{
		$this->db->select('*');
	    $this->db->from($this->tabela);
	    $this->db->where($data);
	    $this->db->where_not_in('STATUS_boleto',array(0,2,4,5));
	    return $this->db->get()->result();
	}

	
	public function verificaBoleto($data) {
		$this->db->select('	*,
							DATE_FORMAT(`BOLE_DataDoc`,"%d-%m-%Y") AS BOLE_DataDoc,
							DATE_FORMAT(`BOLE_Vencimento`,"%d-%m-%Y") AS BOLE_Vencimento ');
	    $this->db->from($this->tabela);
	    $this->db->where('month(BOLE_DataProc)',$data['mes']);
		$this->db->where('year(BOLE_DataProc)',$data['ano']);
	    return $this->db->get()->result();
	}

	public function preVisualizarBoletoId($data)
	{
		$this->db->select('*');
	    $this->db->from($this->tabela);
	    $this->db->where($data);
	   // $this->db->where_not_in('STATUS_boleto',array(0,2,4,5));
	    return $this->db->get()->result();
	}

	public function gerarBoletosMassa($dados)
	{
		$this->db->select('*');
	    $this->db->from($this->tabela);	 
		$this->db->where($dados); 	    	    
	    return $this->db->get()->result();
	}

    public function preGerarBoletosMassa($dados)
    {
        $this->db->select('*');
        $this->db->from($this->tabela);
        $this->db->where($dados);
        $this->db->where_in('STATUS_boleto', array(0,1));
        return $this->db->get()->result();
    }

	public function cancelarBoletos($codigoApi, $data)
	{
		$this->db->where($this->chave,$codigoApi);
		$this->db->where_not_in('STATUS_boleto',array(0,2,4,5));
		$this->db->update($this->tabela,$data);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function alterarBoletos($codigoApi, $data)
	{		
		$this->db->where($this->chave,$codigoApi);
		$this->db->where_not_in('STATUS_boleto',array(0,2,4,5));
		$this->db->update($this->tabela,$data);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function baixaBoleto($where,$data)
    {
        $this->db->where($where);
        $this->db->update($this->tabela,$data);
        
        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }

	public function pegarCodigoAPi($where)
	{
		$this->db->select('API_codigo,BOLE_ValorDocumento,BOLE_Sequencial,BOLE_Vencimento');
	    $this->db->from($this->tabela);	 
	    $this->db->where($where);	    
	    return $this->db->get()->result();
	}

	public function dadosArquivosRemessaCaixa($where)
	{
		$this->db->select('*');
	    $this->db->from($this->tabela);	 
	    $this->db->where($where);	    
	    return $this->db->get()->result();
	}

	public function pegarSequenciaClienteUpdate($codigo)
	{	

		$this->db->select('sequencia_remessa');
	    $this->db->from('cliente_sequencia_remessa');	 
	    $this->db->where('codigo_cliente',$codigo);	    
	    $sequencia = $this->db->get()->result();
	    
	    $sequencia = $sequencia[0]->sequencia_remessa + 1;

		$data = array(
						'sequencia_remessa' => $sequencia
					);

		$this->db->where('codigo_cliente',$codigo);
		$this->db->set($data);
		$this->db->update('cliente_sequencia_remessa');
		
		if($this->db->affected_rows() == '1')
		{
			return $sequencia;
		}

		return false;
		
	}

	public function pegarSequenciaCliente($codigo)
	{	

		$this->db->select('sequencia_remessa');
	    $this->db->from('cliente_sequencia_remessa');	 
	    $this->db->where('codigo_cliente',$codigo);	    
	    $sequencia = $this->db->get()->result();

	    return  $sequencia[0]->sequencia_remessa;
	   
	}

	public function atualizarStatusArquivosRemessaCaixa($where,$status)
	{

		$data = array(
						'STATUS_boleto' => $status
					);

		$this->db->where($where);

		$this->db->update($this->tabela,$data);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function atualizarStatusArquivosRemessaBB($where,$status)
	{

		$data = array(
						'STATUS_boleto' => $status
					);

		$this->db->where($where);

		$this->db->update($this->tabela,$data);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function dadosArquivosRemessaBB($where)
	{
		$this->db->select('*');
	    $this->db->from($this->tabela);	 
	    $this->db->where($where);	    
	    return $this->db->get()->result();
	}

	public function gerarChaveOab($value='')
	{
		ini_set('max_execution_time', 0);
		set_time_limit(0);

		$this->db->select('id');		
	    $this->db->from('abombole_x');
	    $this->db->where('API_codigo =""');

	    $dados = $this->db->get()->result();


	    foreach ($dados as $key) {
	    	$where = array('id' => $key->id);
	    	$data = array('API_codigo' => urlencode(base64_encode($key->id)));

	    	$this->db->where($where);
	    	$this->db->update('abombole_x',$data);
	    }

	    
	}

}

/* End of file Boletos_model.php */
/* Location: ./application/models/Boletos_model.php */