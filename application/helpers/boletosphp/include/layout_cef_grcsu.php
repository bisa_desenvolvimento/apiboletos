<?php
header("Content-type: text/html; charset=ISO-8859-1");
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <desenvolvimento10@bisa.com.br>|
// | Desenvolvimento Boleto CEF: Bruno Magnata                            |
// +----------------------------------------------------------------------+
?>

<?php

  $patronal = ($dadosboleto["BOLE_GRCSUcategoria"] == 'PATRONAL_EMPREGADOR') ? base_url('assets/boletos').'/imagens_grcsu/icoSelecaoChecked.gif'  : base_url('assets/boletos').'/imagens_grcsu/icoSelecao.gif';  
  $empregador = ($dadosboleto["BOLE_GRCSUcategoria"] == 'EMPREGADOS') ? base_url('assets/boletos').'/imagens_grcsu/icoSelecaoChecked.gif'  : base_url('assets/boletos').'/imagens_grcsu/icoSelecao.gif';
  $liberal = ($dadosboleto["BOLE_GRCSUcategoria"] == 'PROF_LIBERAL') ? base_url('assets/boletos').'/imagens_grcsu/icoSelecaoChecked.gif'  : base_url('assets/boletos').'/imagens_grcsu/icoSelecao.gif'; 
  $autonomo = ($dadosboleto["BOLE_GRCSUcategoria"] == 'AUTONOMO') ? base_url('assets/boletos').'/imagens_grcsu/icoSelecaoChecked.gif'  : base_url('assets/boletos').'/imagens_grcsu/icoSelecao.gif'; 

?> 

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<HTML>
<HEAD>
<TITLE><?php echo $dadosboleto["identificacao"]; ?></TITLE>
<META http-equiv=Content-Type content=text/html charset=ISO-8859-1>
<META name="KeyWords" content="GRCSU, nova guia sindical GRCSU, guia de sindical, imposto sindical, c�digo de barras, GRCS online">

<style type=text/css>
.SelectTopo {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; WIDTH: 151px; COLOR: #5f5f5f; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 17px; BACKGROUND-COLOR: #cecece
}
.CabecalhoNome {
  FONT-SIZE: 11px; BACKGROUND: #41a4d0; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.DetalheNome {
  BACKGROUND: #41a4d0; COLOR: #41a4d0
}
.MenuItemBold {
  FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.MenuItemBold:link {
  FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.MenuItemBold:visited {
  FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.MenuItemBold:hover {
  FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
.MenuItemInterno {
  FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.MenuItemInterno:hover {
  FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
.ConteudoTxtInfo {
  FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
A.ConteudoTxtLink:link {
  FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.ConteudoTxtLink:visited {
  FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.ConteudoTxtLink:hover {
  FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
.ConteudoTxtBoasVindas {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoTxtAvisoNavegacao {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoTxt {
  FONT-SIZE: 11px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoTxtBold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoTxtTituloBold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
.ConteudoTxtTituloBold2 {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #0e3674; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
A.ConteudoTxtTituloBold:hover {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
.texto {
  FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.textobold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.cantoclaroinfesq {
  BACKGROUND: url(../_images/dlt_canto_cbd9e1_ie.gif) #cbd9e1 fixed no-repeat left bottom
}
.cantoclaroinfdir {
  BACKGROUND: url(../_images/dlt_canto_cbd9e1_id.gif) #cbd9e1 fixed no-repeat right bottom
}
.cantoclarosupdir {
  BACKGROUND: url(../_images/dlt_canto_cbd9e1_sd.gif) #cbd9e1 fixed no-repeat right top
}
.cantoclarosupesq {
  BACKGROUND: url(../_images/dlt_canto_cbd9e1_se.gif) #cbd9e1 fixed no-repeat left top
}
.ConteudoTxtTituloTbl {
  FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoTxtConteudoCampoTbl {
  FONT-SIZE: 11px; BACKGROUND: #f6f6f6; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoSelect {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #cecece
}
.ConteudoTxtSubtituloBold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #227ea5; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.ConteudoTxtInfoBold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #f6f6f6
}
.ConteudoTxtInfo {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #f6f6f6
}
.BoxClaraTxt {
  FONT-SIZE: 11px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #cbd9e1
}
.BoxClaraTxtBold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #333333; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #cbd9e1
}
.CantoClaroInfEsq {
  BACKGROUND: url(../_images/dtlCantoBoxClaroEI.gif) #cbd9e1 fixed no-repeat left bottom
}
.ConteudoTxtAviso {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #0e3674; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.CantoClaroInfDir {
  BACKGROUND-POSITION: right bottom; BACKGROUND-IMAGE: url(../_images/dtlCantoBoxClaroDI.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-COLOR: #cbd9e1
}
.fa7p {
  FONT-SIZE: 7pt; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.fa11p {
  FONT-WEIGHT: normal; FONT-SIZE: 11pt; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.txtazul {
  FONT-SIZE: 8pt; COLOR: #005baa; FONT-FAMILY: Verdana
}
.fa9p {
  FONT-WEIGHT: normal; FONT-SIZE: 9pt; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.fa10p {
  FONT-WEIGHT: normal; FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.fa10pbold {
  FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.fa11pbold {
  FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: #000000; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.nohscroll {
  OVERFLOW-X: hidden
}
.nomeusuario {
  FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #333333; FONT-FAMILY: Verdana, Arial
}
.cpfusuario {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #333333; FONT-FAMILY: Verdana, Arial
}
.selectatendimento {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #5f5f5f; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 17px; BACKGROUND-COLOR: #cecece
}
.selectnavegue {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #5f5f5f; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 17px; BACKGROUND-COLOR: #cecece
}
.select {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #5f5f5f; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 17px; BACKGROUND-COLOR: #cecece
}
.input {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #000000; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 17px; BACKGROUND-COLOR: #cecece
}
.txtmenu {
  FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
.titcampo {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #f6f6f6
}
.subtitulo {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #227ea5; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.titcampobold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; BACKGROUND-ATTACHMENT: fixed; COLOR: #666666; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #f6f6f6
}
A.linkpbold:link {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkpbold:visited {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkbold:hover {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
A.linkp:link {
  FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkp:visited {
  FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkp:hover {
  FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
A.linkczbold12 {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #0e3674; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
A.linkczbold:link {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkczbold:visited {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkczbold:hover {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
A.linkcz:link {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkcz:visited {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
A.linkcz:hover {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: underline
}
.texto {
  FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.textobold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.textocabecalho {
  FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.textocabecalhobold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.fundopontilhado {
  BACKGROUND-POSITION: center 50%; BACKGROUND-ATTACHMENT: fixed; BACKGROUND-IMAGE: url(../_images/back/pontilhado.gif); BACKGROUND-REPEAT: repeat-x
}
.tittabela {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; BACKGROUND-ATTACHMENT: fixed; COLOR: #ffffff; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.textoconfirmacao {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #0e3674; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.cantoescurosupesq {
  BACKGROUND: url(../_images/dlt/dlt_canto_7d9fb4_se.gif) #7d9fb4 fixed no-repeat left top
}
.cantoescurosupdir {
  BACKGROUND: url(../_images/dlt/dlt_canto_7d9fb4_sd.gif) #7d9fb4 fixed no-repeat right top
}
.cantoescuroinfesq {
  BACKGROUND: url(../_images/dlt/dlt_canto_7d9fb4_ie.gif) #7d9fb4 fixed no-repeat left bottom
}
.cantoescuroinfdir {
  BACKGROUND: url(../_images/dlt/dlt_canto_7d9fb4_id.gif) #7d9fb4 fixed no-repeat right bottom
}
.cantoclaroinfesq {
  BACKGROUND: url(../_images/dlt/dlt_canto_cbd9e1_ie.gif) #cbd9e1 fixed no-repeat left bottom
}
.cantoclaroinfdir {
  BACKGROUND: url(../_images/dlt/dlt_canto_cbd9e1_id.gif) #cbd9e1 fixed no-repeat right bottom
}
.cantoclarosupdir {
  BACKGROUND: url(../_images/dlt/dlt_canto_cbd9e1_sd.gif) #cbd9e1 fixed no-repeat right top
}
.cantoclarosupesq {
  BACKGROUND: url(../_images/dlt/dlt_canto_cbd9e1_se.gif) #cbd9e1 fixed no-repeat left top
}
.level1 {
  FONT-WEIGHT: normal; FONT-SIZE: 12px; FONT-FAMILY: Arial, Helvetica, sans-serif
}
.level2 {
  FONT-SIZE: 10px; FONT-FAMILY: Arial, Helvetica, sans-serif
}
A.menu:link {
  COLOR: #990000; TEXT-DECORATION: underline
}
A.menu:hover {
  COLOR: #0000ff; TEXT-DECORATION: none
}
A.menu:active {
  COLOR: #ff0000; TEXT-DECORATION: none
}
A.menu:visited {
  COLOR: #666666
}
.paginacao {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; BACKGROUND-ATTACHMENT: fixed; COLOR: #666666; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.textarea {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-SIZE: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #000000; PADDING-TOP: 0px; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #cecece
}
.titcamposemfundo {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.titcampo2 {
  FONT-SIZE: 11px; COLOR: #666666; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #cbd9e1
}
.titcampobold2 {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; BACKGROUND-ATTACHMENT: fixed; COLOR: #666666; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #cbd9e1
}
.textoconfirmacaosembold {
  FONT-WEIGHT: normal; FONT-SIZE: 11px; COLOR: #0e3674; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.fv8lr {
  FONT-SIZE: 8pt; COLOR: #f37022; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.dadocli {
  FONT-SIZE: 11px; COLOR: black; FONT-FAMILY: Verdana, Arial; BACKGROUND-COLOR: #cbd9e1; TEXT-DECORATION: none
}
.dadoclibold {
  FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: black; FONT-FAMILY: Verdana, Arial; BACKGROUND-COLOR: #cbd9e1; TEXT-DECORATION: none
}
#mnuLadDiv {
  WIDTH: 140px; TEXT-ALIGN: left
}
#mnuLadDiv UL {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px; LIST-STYLE-TYPE: none
}
#mnuLadDiv LI {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 8px
}
#mnuLadDiv LI.rdpe2 {
  PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; VERTICAL-ALIGN: middle; PADDING-TOP: 0px
}
#mnuLadDiv A.nivel0 {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: 0px 0ex; PADDING-LEFT: 0px; FONT-WEIGHT: bold; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; BACKGROUND-REPEAT: no-repeat
}
#mnuLadDiv A.nivel0:hover {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: 0px 0ex; PADDING-LEFT: 0px; FONT-WEIGHT: bold; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; BACKGROUND-REPEAT: no-repeat; TEXT-DECORATION: none
}
#mnuLadDiv A.nivel1 {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left 3.2ex; PADDING-LEFT: 14px; FONT-WEIGHT: normal; FONT-SIZE: 10px; BACKGROUND-IMAGE: url(../_images/ico_nivel1.gif); PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 8px; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial
}
#mnuLadDiv A.nivel1:hover {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left 3.2ex; PADDING-LEFT: 14px; FONT-WEIGHT: normal; FONT-SIZE: 10px; BACKGROUND-IMAGE: url(../_images/ico_nivel1.gif); PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 8px; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline
}
#mnuLadDiv A.nivel3 {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left 1.5ex; PADDING-LEFT: 14px; FONT-WEIGHT: normal; FONT-SIZE: 10px; BACKGROUND-IMAGE: url(../_images/ico_nivel1.gif); PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 8px; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial
}
#mnuLadDiv A.nivel3:hover {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left 1.5ex; PADDING-LEFT: 14px; FONT-WEIGHT: normal; FONT-SIZE: 10px; BACKGROUND-IMAGE: url(../_images/ico_nivel1.gif); PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 8px; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline
}
#mnuLadDiv .seglinha {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left center; PADDING-LEFT: 14px; BACKGROUND-IMAGE: url(../_images/space_menu.jpg); PADDING-BOTTOM: 0px; PADDING-TOP: 0px
}
A {
  COLOR: #333333; TEXT-DECORATION: none
}
A:visited {
  TEXT-DECORATION: none
}
TD {
  FONT-SIZE: 10px; COLOR: #333333; FONT-FAMILY: Verdana,Arial,Helvetica,sans-serif
}
#mnuLadDiv A.nivel2 {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left 1.5ex; PADDING-LEFT: 14px; FONT-WEIGHT: normal; FONT-SIZE: 10px; BACKGROUND-IMAGE: url(../_images/ico_nivel2.gif); PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 8px; BACKGROUND-REPEAT: no-repeat; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: none
}
#mnuLadDiv A.nivel2:hover {
  PADDING-RIGHT: 0px; BACKGROUND-POSITION: left 2.5ex; PADDING-LEFT: 14px; FONT-WEIGHT: normal; FONT-SIZE: 10px; BACKGROUND-IMAGE: url(../_images/ico_nivel2.gif); PADDING-BOTTOM: 0px; MARGIN: 0px; COLOR: #333333; PADDING-TOP: 8px; BACKGROUND-REPEAT: repeat; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline
}

</style> 

<script language="JavaScript1.2">
function init() {
  window.focus();
}
function preloadImages()
{
 var d=document;
 if(d.images)
 {
  if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=preloadImages.arguments;
  for(i=0; i<a.length; i++)
  if(a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
 }
}
</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" 
onload="preloadImages(
  &#39;<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/P.gif&#39;,
  &#39;<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/B.gif&#39;,
  &#39;.<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/lgoCaixa2.gif&#39;,
  &#39;<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/lgoCaixaMenor.gif&#39;,
  &#39;<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif&#39;);">
<table cellspacing="0" cellpadding="0" width="645" border="0">
  <tbody>
  <tr>
    <td valign="top" width="12">
      <table height="540" cellspacing="0" cellpadding="0" width="12" border="0">
        <tbody>
        <tr><td valign="top" nowrap="" width="12"><img height="123" hspace="0" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/titViaContribuinte.gif width="12" vspace="60"></td></tr>
        <tr><td valign="bottom" nowrap="" width="12" height="513"><img height="150" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/titViaDocumentoDoBanco.gif width="12"></td></tr>
        </tbody>
      </table>
    </td>
    <td width="5">&nbsp;</td>
    <td valign="top">
      <table cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td width="130"><img height="34" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/lgoCaixa2.gif width="147" align="absMiddle"></td>
          <td valign="top" colspan="3" with="500">
             <table cellspacing="0" cellpadding="0" border="0">
               <tbody><tr><td class="fa11pbold" align="left">GRCSU - Guia de Recolhimento da Contribui��o Sindical Urbana</td></tr>
               <tr><td class="fa9pbold" align="left">SAC CAIXA: 0800 726 0101 (informa��es, reclama��es, sugest�es e elogios)</td></tr>
               <tr><td class="fa9pbold" align="left">Para pessoas com defici�ncia auditiva ou de fala: 0800 726 2496</td></tr>
         <tr><td class="fa9pbold" align="left">Ouvidoria: 0800 725 7474</td></tr>
             </tbody></table>
          </td>
        </tr>
        <tr>
          <td class="txtimpbold" valign="bottom"><img height="10" hspace="0" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/titDadosEntidadeSindical.gif width="160" vspace="2"></td>
          <td><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif width="20"></td>
          <td width="300" class="fa9pbold" align="center" valign="top">www.caixa.gov.br</td>
          <td class="txttitimp" align="right">
             <table class="fa7p" cellspacing="0" cellpadding="0" width="140" align="right" border="0">
              <tbody>
              <tr>
                <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
                <td class="fa7p" nowrap="" width="79">&nbsp;Vencimento</td>
                <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
                <td class="fa7p" nowrap="" width="58">&nbsp;Compet�ncia</td>
                <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
              </tr>
              <tr>
                <td class="fa7p">&nbsp;<?php echo ($data_venc != "") ? $dadosboleto["data_vencimento"] : "Contra Apresenta��o" ?></td>
                <td class="fa7p">&nbsp;<?php echo date('m').'/'.date('Y') ?></td>
              </tr>
              <tr><td bgcolor="#000000" colspan="5" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="140"></td></tr>
              <tr><td colspan="5" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif width="3"></td></tr>
              </tbody>
             </table>
          </td>
        </tr>
       </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td class="fa7p" nowrap="" width="1"><img height="14" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
          <td class="fa7p" nowrap="" width="449">&nbsp;Nome da Entidade</td>
          <td class="fa7p" width="1" bgcolor="#000000"><img height="14" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
          <td class="fa7p" nowrap="" width="178">&nbsp;C�digo da Entidade Sindical</td>
          <td class="fa7p" nowrap="" width="1" bgcolor="#000000"><img height="14" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="1"><img height="14" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
          <td class="fa7p" width="449" height="12">&nbsp;<?php echo $dadosboleto["cedente"]?></td>
          <td class="fa7p" width="1" bgcolor="#000000"><img height="14" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
          <td class="fa7p" width="178">&nbsp;<?php echo $dadosboleto['conta_cedente']; ?></td>
          <td class="fa7p" width="1" bgcolor="#000000"><img height="14" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
        </tr>
        <tr><td bgcolor="#000000" colspan="7" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="630"></td></tr>
        <tr><td colspan="7"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif width="1"></td>
          <td class="fa7p" nowrap="" width="250">&nbsp;Endere�o</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="80">&nbsp;N�mero</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="117">&nbsp;Complemento</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="178">&nbsp;CNPJ da Entidade</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p">&nbsp;<?php echo $dadosboleto["endereco"]; ?></td>
          <td class="fa7p" width="80">&nbsp; </td>
          <td class="fa7p" width="117">&nbsp; </td>
          <td class="fa7p" nowrap="" width="178">&nbsp;<?php echo $dadosboleto["cpf_cnpj"] ?></td>
        </tr>
        <tr><td bgcolor="#000000" colspan="9" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="9"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="230">&nbsp;Bairro/Distrito</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="100">&nbsp;CEP</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="265">&nbsp;Cidade/Munic�pio</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="30">&nbsp;UF</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="250">&nbsp;<?php echo $dadosboleto["BairroCedente"] ?></td> <!-- CENTRO -->
          <td class="fa7p" width="70">&nbsp;<?php echo $dadosboleto["CEPCedente"] ?></td> <!-- 03201-000 -->
          <td class="fa7p" width="275">&nbsp;<?php echo $dadosboleto["CidadeCedente"] ?></td> <!-- Imperatriz -->
          <td class="fa7p" nowrap="" width="1">&nbsp;<?php echo $dadosboleto["UFCedente"] ?></td> <!--  MA -->
        </tr>
        <tr><td bgcolor="#000000" colspan="9" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="9"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td valign="bottom" nowrap="" width="1" rowspan="3"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="txtimpbold" nowrap="" colspan="3"><img height="10" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/titDadosContribuinte.gif width="129"></td>
          <td valign="bottom" nowrap="" width="1" bgcolor="#000000" rowspan="3"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" nowrap="" width="469">&nbsp;Nome/Raz�o Social/Denomina��o Social</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="158">&nbsp;CPF/CNPJ/C�digo do Contribuinte</td>
        </tr>
        <tr>
          <td class="fa7p" width="469" height="12">&nbsp;<?php echo $dadosboleto["sacado"]?> </td>
          <td class="fa7p" nowrap="" width="158">&nbsp;<?php echo $dadosboleto["CPF_CNPJ"]?> </td>
        </tr>
        <tr>
          <td bgcolor="#000000" colspan="5" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td>
        </tr>
        <tr><td colspan="5"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="250">&nbsp;Endere�o</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="80">&nbsp;N�mero</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="296">&nbsp;Complemento</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="250">&nbsp;<?php echo $dadosboleto["EndeCliente"] ?></td>
          <td class="fa7p" width="70">&nbsp;  </td>
          <td class="fa7p" width="306">&nbsp; </td>
        </tr>
        <tr><td bgcolor="#000000" colspan="7" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="7"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="80">&nbsp;CEP</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="250">&nbsp;Bairro/Distrito</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="174">&nbsp;Cidade/Munic�pio</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="30">&nbsp;UF</td>
          <td class="fa7p" nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="90">&nbsp;C�digo Atividade</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="80">&nbsp;<?php echo $dadosboleto["Cep"]; ?></td>
          <td class="fa7p" width="240">&nbsp;<?php echo $dadosboleto["Bairro"]; ?></td>
          <td class="fa7p" width="184">&nbsp;<?php echo $dadosboleto["Cidade"]; ?></td>
          <td class="fa7p" nowrap="" width="30">&nbsp;<?php echo $dadosboleto["Uf"]; ?></td>

          <td class="fa7p" nowrap="" width="90" align="center">&nbsp;<?php echo $dadosboleto["BOLE_CNAE"]; ?></td> <!-- 011 -->
        </tr>
        <tr>
          <td bgcolor="#000000" colspan="11" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td>
        </tr>
        <tr><td colspan="11"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td nowrap="" width="1" rowspan="3"><img height="1" src="http://www.veloso.adm.br/grcsu/appPHP/grcsuMySQL.php" width="1"></td>
          <td class="txtimpbold" nowrap="" width="399"><img height="10" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/titDadosReferencia.gif width="214"></td>
          <td nowrap="" width="1"><img height="1" src="http://www.veloso.adm.br/grcsu/appPHP/grcsuMySQL.php" width="1"></td>
          <td class="txtimpbold" nowrap="" width="228"><img height="10" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/titDadosContribuicao.gif    width="130"></td>
          <td nowrap="" width="1"><img height="1" src="http://www.veloso.adm.br/grcsu/appPHP/grcsuMySQL.php" width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" nowrap="" width="399">&nbsp;<b>Categoria</b></td>
          <td valign="bottom" nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="228">&nbsp;(=) Valor do Documento</td>
          <td valign="bottom" nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" nowrap="" width="399">&nbsp;            
            <img height="14" src=<?php echo $patronal; ?>   width="14" align="absMiddle"> &nbsp;Patronal/Empregados&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img height="14" src=<?php echo $empregador; ?> width="14" align="absMiddle">  &nbsp;Empregados&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img height="14" src=<?php echo $liberal; ?>   width="14" align="absMiddle">  &nbsp;Prof. Liberal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img height="14" src=<?php echo $autonomo; ?>    width="14" align="absMiddle"> &nbsp;Aut�nomos
          </td><!--|PATRONAL| -->
          <td class="fa8p" nowrap="" width="218" align="right">&nbsp;<?php echo $dadosboleto["valor_boleto"] ?>&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <td nowrap="" height="1"></td>
          <td class="fa7p" nowrap="" height="1"></td>
          <td class="fa7p" nowrap="" bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="230"></td>
        </tr>
        <tr>
          <td nowrap="" height="1"></td>
          <td class="fa7p" nowrap="" colspan="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td>
          <td nowrap="" height="1"></td>
        </tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="210">&nbsp;Capital Social - Empresa</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="188">&nbsp;N� Empregados Contribuintes</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" nowrap="" width="218">&nbsp;(-) Desconto / Abatimento</td>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="210" align="left">&nbsp;</td>
          <td class="fa7p" width="188" align="left">&nbsp;</td>
          <td class="fa8p" width="218" align="right">&nbsp;&nbsp;</td>
        </tr>
        <tr><td bgcolor="#000000" colspan="7" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="7" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        <tr>
          <td nowrap="" width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="210">&nbsp;Capital Social - Estabelecimento</td>
          <td nowrap="" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="188">&nbsp;Total Remunera��o - Contribuintes</td>
          <td nowrap="" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="218">&nbsp;(-) Outras Dedu��es</td>
          <td nowrap="" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="210" align="left">&nbsp;</td>
          <td class="fa7p" width="188" align="left">&nbsp;</td>
          <td class="fa8p" width="218" align="right">&nbsp;&nbsp;</td></tr>
        <tr><td bgcolor="#000000" colspan="7"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="7"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        <tr>
          <td valign="bottom" nowrap="" width="1" bgcolor="#000000" rowspan="11"><img height="88" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="210">&nbsp;</td>
          <td nowrap="" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="188">&nbsp;Total Empregados - Estabelecimento</td>
          <td nowrap="" bgcolor="#000000" height="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="218">&nbsp;(+) Mora / Multa</td>
          <td nowrap="" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td class="fa7p" width="210">&nbsp;<i>MENSAGEM DESTINADA AO CONTRIBUINTE</i></td>
          <td class="fa7p" width="188" align="left">&nbsp;</td>
          <td class="fa8p" width="218" align="right">&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <td width="1"></td>
          <td width="1" colspan="5"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="419"></td>
        </tr>
        <tr><td colspan="5" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        <tr>
          <td class="fa7p" valign="top" colspan="3" rowspan="7">&nbsp;CONTRIBUI��O SINDICAL - <?php echo date('Y') ?></br>&nbsp;
            <?php echo $dadosboleto["instrucoes1"]; ?>
            <?php echo $dadosboleto["instrucoes2"]; ?>
            <?php echo $dadosboleto["instrucoes3"]; ?>
            <?php echo $dadosboleto["instrucoes4"]; ?>
          </td>
          <td valign="bottom" width="1" rowspan="7"><img height="65" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="218">&nbsp;(+) Outros Acrescimos</td>
          <td valign="bottom" width="1" rowspan="7"><img height="65" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr><td class="fa8p" width="218" align="right">&nbsp;&nbsp;</td></tr>
        <tr><td class="fa7p" bgcolor="#000000" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="228"></td></tr>
        <tr><td width="296"><img height="2" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="2"></td></tr>
        <tr><td class="fa7p" height="1"></td></tr>
        <tr><td class="fa7p" width="218">&nbsp;(=) Valor Cobrado</td></tr>
        <tr><td class="fa8p" width="218" align="right">&nbsp;&nbsp;</td></tr>
        <tr><td nowrap="" bgcolor="#000000" colspan="7" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td nowrap="" colspan="7" height="1"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td class="fa11pbold" width="60"><div align="center"><?php echo $dadosboleto["codigo_banco_com_dv"] ?></div></td>
          <td width="1" bgcolor="#000000"><img height="17" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa11pbold" width="568" align="right">&nbsp;<?php echo $dadosboleto["linha_digitavel"]?> </td>
        </tr>
        <tr><td class="fa11pbold" bgcolor="#000000" colspan="3"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td class="fa11pbold" colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="121" height="1">&nbsp;C�digo do Benefici�rio</td>
          <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="120" height="1">&nbsp;Nosso N�mero</td>
          <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="141" height="1">&nbsp;Valor do Documento</td>
          <td rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="144" height="1">&nbsp;Data Vencimento</td>
          <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" width="98" height="1">&nbsp;Compet�ncia</td>
          <td width="1" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <!--<td class="fa7p" width="121" height="1">&nbsp;<?php echo $dadosboleto["agencia_codigo"] ?></td> -->
          <td class="fa7p" width="121" height="1">&nbsp;<?php echo $dadosboleto["conta_cedente"] ?></td>
          <td class="fa7p" width="120" height="1">&nbsp;<?php echo  $dadosboleto["nosso_numero"] ?></td>
          <td class="fa7p" width="141" height="1" align="center">&nbsp;<?php echo $dadosboleto["valor_boleto"] ?></td>
          <td class="fa7p" width="144" height="1">&nbsp;<?php echo $dadosboleto["data_vencimento"]  ?></td>
          <td class="fa7p" width="98" height="1">&nbsp;<?php echo date('m').'/'.date('Y') ?></td>
        </tr>
        <tr><td colspan="11"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="11"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td width="350" height="1">&nbsp;</td>
          <td width="1" bgcolor="#000000"><img height="32" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa7p" valign="top" width="279">&nbsp;Autentica��o Mec�nica</td>
        </tr>
        </tbody>
      </table>
      <table cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr><td height="10"><p><img height="10" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/corte.gif  width="630"></p></td></tr>
        <tr><td><img height="10" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td></tr>
        </tbody>
      </table>
      <table cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td width="130"><img height="29" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/lgoCaixaMenor.gif width="125"></td>
          <td class="fa7p" width="1">&nbsp;<br><br></td>
          <td width="1"><img height="29" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa11pbold" valign="bottom" align="middle" width="50"> <?php echo $dadosboleto["codigo_banco_com_dv"]?> </td>
          <td width="1"><img height="29" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td class="fa11pbold" valign="bottom" width="447" aling="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dadosboleto["linha_digitavel"]?></td>
        </tr>
        <tr bgcolor="#000000"><td colspan="6" height="2"><img height="2" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="6"><img height="2" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td></tr>
        </tbody>
      </table>
      <table cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td valign="top">
            <table class="fa7p" cellspacing="0" cellpadding="0" width="420" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="420">&nbsp;Local de Pagamento</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa7p">&nbsp;PREFERENCIALMENTE NAS LOT�RICAS AT� O VALOR LIMITE.. </td></tr>
              <tr><td bgcolor="#000000" colspan="3"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="420"></td></tr>
              <tr><td colspan="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" width="420" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="418">&nbsp;Cedente</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa7p">&nbsp;<?php echo $dadosboleto["cedente"]?></td></tr>
              <tr><td bgcolor="#000000" colspan="3"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="420"></td></tr>
              <tr><td colspan="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" width="420" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="90">&nbsp;Data do Documento</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="140">&nbsp;N�mero do Documento</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="60">&nbsp;Esp. Docum.</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="40">&nbsp;Aceite</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="100">&nbsp;Data Processamento</td>
                <td bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr>
                <td class="fa7p">&nbsp;<?php echo $dadosboleto["data_documento"] ?></td>
                <td class="fa7p">&nbsp;<?php echo $dadosboleto["numero_documento"] ?></td>
                <td class="fa7p">&nbsp;GRCSU</td>
                <td class="fa7p">&nbsp;</td>
                <td class="fa7p">&nbsp;<?php echo $dadosboleto["data_processamento"] ?></td>
              </tr>
              <tr bgcolor="#000000"><td colspan="11"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="420"></td></tr>
              <tr><td colspan="11"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" width="420" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="87" bgcolor="#cccccc" rowspan="2">&nbsp;Uso do Banco<br>&nbsp;EXERC (<?php echo date('Y') ?>)</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="63">&nbsp;Carteira</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="69" rowspan="2">&nbsp;Esp�cie<br>&nbsp;R$(REAL) </td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="88">&nbsp;Quantidade</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="107">&nbsp;Valor</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr>
                <td class="fa7p">&nbsp;<?php echo $dadosboleto["carteira"]; ?></td>
                <td>&nbsp;</td>
                <td class="fa7p">&nbsp;</td>
              </tr>
              <tr bgcolor="#000000"><td colspan="11"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="420"></td></tr>
              <tr><td colspan="11"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" width="420" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="4"><img height="145" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" valign="top" width="418" height="3">&nbsp;Instru��es</td>
                <td bgcolor="#000000" rowspan="3"><img height="145" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa7p" valign="top" align="middle" height="20">BLOQUETO DE CONTRIBUI��O SINDICAL URBANA</td></tr>
              
              <?php if($dadosboleto["data_vencimento"] >= date('d/m/Y')){ ?>
              <tr>              
                <td class="fa7p" valign="top" align="left" height="80">&nbsp;<br>&nbsp;At� o vencimento, pag�vel nas Lot�ricas, Correspondentes CAIXA Aqui, Ag�ncias da CAIXA e Rede Banc�ria.<br>&nbsp;<br>&nbsp;Documento vencido pag�vel somente na CAIXA.<br>&nbsp;<br><br>&nbsp;<br><br>GUIA COM REGISTRO DE INFORMA��ES E CONTROLE DE EMISS�O</td>
              </tr>
              <?php }
              else{ ?>
              <tr>              
                <td class="fa7p" valign="top" align="left" height="80">&nbsp;<br>&nbsp;Pag�vel nas Lot�ricas, Correspondentes CAIXA Aqui e Ag�ncias da CAIXA.<br>&nbsp;<br><br>&nbsp;<br><br>GUIA COM REGISTRO DE INFORMA��ES E CONTROLE DE EMISS�O</td>
              </tr>
              <?php } ?>

              <tr><td bgcolor="#000000" colspan="3"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="419"></td></tr>
              <tr><td colspan="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td></tr>
              </tbody>
            </table>
          </td>
          <td><img height="10" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="10"></td>
          <td valign="top" width="250">
            <table cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;Vencimento</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;<?php echo $dadosboleto["data_vencimento"]; ?>&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;Agencia&nbsp;/&nbsp;C�digo Cedente</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <!--<tr><td class="fa8p" width="180" align="right">&nbsp;<?php echo $dadosboleto["agencia_codigo"]; ?> &nbsp;&nbsp;</td></tr> -->
              <tr><td class="fa8p" width="180" align="right">&nbsp;<?php echo $dadosboleto["agencia"].' / '.$dadosboleto["conta_cedente"]; ?> &nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;Nosso N�mero</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;<?php echo  $dadosboleto["nosso_numero"] ?>&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;(=) Valor do Documento</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp; <?php echo $dadosboleto["valor_boleto"]; ?> &nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;(-) Desconto / Abatimento</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;(-) Outras Dedu��es</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
             </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;(+) Mora / Multa</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;(+) Outros Acr�scimos</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
            <table class="fa7p" cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
                <td class="fa7p" width="198">&nbsp;(=) Valor Cobrado</td>
                <td width="1" bgcolor="#000000" rowspan="2"><img height="26" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
              </tr>
              <tr><td class="fa8p" width="180" align="right">&nbsp;&nbsp;</td></tr>
              <tr><td bgcolor="#000000" colspan="3" height="1"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="200"></td></tr>
              <tr><td colspan="3" height="3"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td width="1" bgcolor="#000000" rowspan="2"><img height="46" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
          <td width="1"><img height="15" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td>
          <td class="fa7p" valign="top" width="628" height="25">&nbsp;Sacado:<br>&nbsp;<?php echo $dadosboleto["sacado"]; ?><br>&nbsp; 
            <?php echo $dadosboleto["endereco1"] ." / ".$dadosboleto["endereco2"]; ?>          </td>
          <td width="1"><img height="15" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td>
          <td width="1" bgcolor="#000000" rowspan="2"><img height="46" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="1"></td>
        </tr>
        <tr>
          <td width="1"><img height="15" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td>
          <td class="fa7p" valign="bottom" width="628">&nbsp;Sacador / Avalista:&nbsp;</td>
          <td width="1"><img height="15" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="1"></td>
        </tr>
        <tr><td bgcolor="#000000" colspan="5"><img height="1" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltPreto.gif  width="630"></td></tr>
        <tr><td colspan="5"><img height="3" src=<?php echo base_url('assets/boletos'); ?>/imagens_grcsu/dltTransparente.gif   width="3"></td></tr>
        </tbody>
      </table>
      <table class="fa7p" cellspacing="0" cellpadding="0" width="630" border="0">
        <tbody>
        <tr>
          <td class="fa7p" width="428">&nbsp;C�digo de Barras</td>
          <!--<TD class=fa7p width=428>&nbsp;</TD>-->
          <td class="fa7p" align="right" width="300"><font face="times new roman">Ficha de Compensa��o / Autentica��o Mec�nica</font></td>
        </tr>
        </tbody>
      </table>
     
      <!-- Bruno - Gera��o C�d Barra -->
     <TABLE cellSpacing=0 cellPadding=0 width=630 border=0>
      <TBODY>
        <TR>
          </br>           
          <TD vAlign=bottom align=left height=50><?php fbarcode($dadosboleto["codigo_barras"]); ?></TD>
        </tr>
      </tbody>
    </table>


    </td>
   </tr>
  </tbody>
</table>

</body></html>