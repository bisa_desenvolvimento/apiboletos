<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function boleto_cef()
{

// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Você deve ter recebido uma cópia da GNU Public License junto com     |
// | esse pacote; se não, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de João Prado Maia e Pablo Martins F. Costa                |
// |                                                                      |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto CEF: Elizeu Alcantara                         |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 5;
$taxa_boleto = 2.95;
$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias  OU  informe data: "13/04/2006"  OU  informe "" se Contra Apresentacao;
$valor_cobrado = "2950,00"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["inicio_nosso_numero"] = "24";  // 24 - Padrão da Caixa Economica Federal
$dadosboleto["nosso_numero"] = "19525086";  // Nosso numero sem o DV - REGRA: Máximo de 8 caracteres!
$dadosboleto["numero_documento"] = "27.030195.10";	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = "Nome do seu Cliente";
$dadosboleto["endereco1"] = "Endereço do seu Cliente";
$dadosboleto["endereco2"] = "Cidade - Estado -  CEP: 00000-000";

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "Pagamento de Compra na Loja Nonononono";
$dadosboleto["demonstrativo2"] = "Mensalidade referente a nonon nonooon nononon<br>Taxa bancária - R$ ".number_format($taxa_boleto, 2, ',', '');
$dadosboleto["demonstrativo3"] = "BoletoPhp - http://www.boletophp.com.br";

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de 2% após o vencimento";
$dadosboleto["instrucoes2"] = "- Receber até 10 dias após o vencimento";
$dadosboleto["instrucoes3"] = "- Em caso de dúvidas entre em contato conosco: xxxx@xxxx.com.br";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelo sistema Projeto BoletoPhp - www.boletophp.com.br";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = "1565"; // Num da agencia, sem digito
$dadosboleto["conta"] = "13877"; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = "4"; 	// Digito do Num da conta

// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = "87000000414"; // ContaCedente do Cliente, sem digito (Somente Números)
$dadosboleto["conta_cedente_dv"] = "3"; // Digito da ContaCedente do Cliente
$dadosboleto["carteira"] = "SR";  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = "BoletoPhp - Código Aberto de Sistema de Boletos";
$dadosboleto["cpf_cnpj"] = "";
$dadosboleto["endereco"] = "Coloque o endereço da sua empresa aqui";
$dadosboleto["cidade_uf"] = "Cidade / Estado";
$dadosboleto["cedente"] = "Coloque a Razão Social da sua empresa aqui";



// NÃO ALTERAR!
include("boletosphp/include/funcoes_cef.php"); 
include("boletosphp/include/layout_cef.php");


}


function boleto_cef_sigcb($dados)
{	

header("Content-type: text/html; charset=utf-8");
	// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Você deve ter recebido uma cópia da GNU Public License junto com     |
// | esse pacote; se não, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de João Prado Maia e Pablo Martins F. Costa				  |
// | 														              |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto CEF SIGCB: Davi Nunes Camargo				  |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 30;
$taxa_boleto = 0;
$data_venc = date("d/m/Y", strtotime($dados->BOLE_Vencimento));  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = number_format($dados->BOLE_ValorDocumento, 2, ',', ''); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// Composição Nosso Numero - CEF SIGCB
$dadosboleto["nosso_numero1"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const1"] = "1"; //constanto 1 , 1=registrada , 2=sem registro
$dadosboleto["nosso_numero2"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const2"] = "4"; //constanto 2 , 4=emitido pelo proprio cliente
$dadosboleto["nosso_numero3"] = $dados->BOLE_Sequencial; // tamanho 9 -  Antes estava este campo:BOLE_Titulo

$dadosboleto["numero_documento"] = $dados->BOLE_NumDoc;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"]  = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"]   = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de processamento do boleto (opcional)

// Mantis: 6939
$vl_result =  explode(',',$valor_boleto,-1);

if(strlen($vl_result[0]) == 1){    
$vl_boleto = Mask('#,##',$valor_boleto);

} else if(strlen($vl_result[0]) == 2){    
$vl_boleto = Mask('##,##',$valor_boleto);

} else if(strlen($vl_result[0]) == 3){    
$vl_boleto = Mask('###,##',$valor_boleto);

} else if(strlen($vl_result[0]) == 4){    
$vl_boleto = Mask('#.###,##',$valor_boleto);

} else if(strlen($vl_result[0]) == 5){    
$vl_boleto = Mask('##.###,##',$valor_boleto);

}else if(strlen($vl_result[0]) == 6){    
$vl_boleto = Mask('###.###,##',$valor_boleto);

}
// $dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgul 
$dadosboleto["valor_boleto"] = $vl_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula


// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $dados->BOLE_NomeCliente;
$dadosboleto["endereco1"] = strtoupper($dados->BOLE_EndeCliente." - ".$dados->BOLE_Bairro);
$dadosboleto["endereco2"] = strtoupper($dados->BOLE_Cidade." - ".$dados->BOLE_UF." - CEP ".$dados->BOLE_CEP." - CPF/CNPJ ".$dados->BOLE_CGC);
$dadosboleto["CPF_CNPJ"] = $dados->BOLE_CGC;
// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $dados->BOLE_InstrucaoBoleta;
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
/*$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de ".$dados->BOLE_Multa."% ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "- Receber at&eacute; 30 dias ap&oacute;s o vencimento";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelos sistemas BisaWeb - http://www.bisaweb.com.br";*/

$dadosboleto["instrucoes1"] = $dados->BOLE_InstrucaoBoleta;
$dadosboleto["instrucoes2"] = "";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";



// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = $dados->BOLE_Aceite;		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "OU";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $dados->BOLE_Agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $dados->BOLE_Conta; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $dados->BOLE_DigConta; 	// Digito do Num da conta

//Tratamento do campo de cadente
/*$cedente = explode('/',$dados->BOLE_AgenciaCedente);
$cedente = $cedente = explode('-',$cedente[1]);*/
$cedente = $dados->BOLE_NumeroConvenio;


// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = $cedente; // Código Cedente do Cliente, com 6 digitos (Somente Números)
// $dadosboleto["carteira"] = "CR";  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)
//MANTIS 8227
$dadosboleto["carteira"] = "RG";  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = $dados->BOLE_NomeCedente;
$dadosboleto["cpf_cnpj"] = $dados->BOLE_CNPJCPFCedente;
$dadosboleto["endereco"] = strtoupper($dados->BOLE_EndeCedente);//Coloque o endereço da sua empresa aqui
$dadosboleto["cidade_uf"] = "";//Cidade / Estado
$dadosboleto["cedente"] = $dados->BOLE_NomeCedente;

// NÃO ALTERAR!
include_once("boletosphp/include/funcoes_cef_sigcb.php"); 
$dadosboleto = gerarBoleto($dadosboleto);
include("boletosphp/include/layout_cef.php");
}


function boleto_cef_grcsu($dados)
{	

header("Content-type: text/html; charset=utf-8");
	
// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <desenvolvimento10@bisa.com.br>|
// | Desenvolvimento Boleto CEF: Bruno Magnata                            |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 30;
$taxa_boleto = 0;
$data_venc = date("d/m/Y", strtotime($dados->BOLE_Vencimento));  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = number_format($dados->BOLE_ValorDocumento, 2, ',', '.'); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// Composição Nosso Numero - CEF SIGCB
$dadosboleto["nosso_numero1"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const1"] = "1"; //constanto 1 , 1=registrada , 2=sem registro
$dadosboleto["nosso_numero2"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const2"] = "4"; //constanto 2 , 4=emitido pelo proprio cliente
$dadosboleto["nosso_numero3"] = $dados->BOLE_Sequencial; // tamanho 9 -  Antes estava este campo:BOLE_Titulo

$dadosboleto["numero_documento"] = $dados->BOLE_NumDoc;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"]  = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"]   = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = utf8_decode($dados->BOLE_NomeCliente);
$dadosboleto["endereco1"] = utf8_decode(strtoupper($dados->BOLE_EndeCliente." - ".$dados->BOLE_Bairro));
$dadosboleto["endereco2"] = utf8_decode(strtoupper($dados->BOLE_Cidade." - ".$dados->BOLE_UF." - CEP ".$dados->BOLE_CEP." - CPF/CNPJ ".$dados->BOLE_CGC));
$dadosboleto["CPF_CNPJ"] = $dados->BOLE_CGC;
$dadosboleto["EndeCliente"] = utf8_decode(strtoupper($dados->BOLE_EndeCliente));
$dadosboleto["Bairro"] = utf8_decode(strtoupper($dados->BOLE_Bairro));
$dadosboleto["Cidade"] = utf8_decode(strtoupper($dados->BOLE_Cidade));
$dadosboleto["Uf"] = utf8_decode(strtoupper($dados->BOLE_UF));
$dadosboleto["Cep"] = utf8_decode(strtoupper($dados->BOLE_CEP));



// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = utf8_decode($dados->BOLE_InstrucaoBoleta );
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
/*$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de ".$dados->BOLE_Multa."% ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "- Receber at&eacute; 30 dias ap&oacute;s o vencimento";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelos sistemas BisaWeb - http://www.bisaweb.com.br";*/

$dadosboleto["instrucoes1"] = utf8_decode($dados->BOLE_InstrucaoBoleta );
$dadosboleto["instrucoes2"] = "";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";



// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $dados->BOLE_Agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $dados->BOLE_Conta; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $dados->BOLE_DigConta; 	// Digito do Num da conta

//Tratamento do campo de cadente
/*$cedente = explode('/',$dados->BOLE_AgenciaCedente);
$cedente = $cedente = explode('-',$cedente[1]);*/
$cedente = $dados->BOLE_NumeroConvenio;


// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = $cedente; // Código Cedente do Cliente, com 6 digitos (Somente Números)
$dadosboleto["carteira"] = "CR";  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = utf8_decode($dados->BOLE_NomeCedente);
$dadosboleto["cpf_cnpj"] = $dados->BOLE_CNPJCPFCedente;
$dadosboleto["endereco"] = utf8_decode(strtoupper($dados->BOLE_EndeCedente));//Coloque o endereço da sua empresa aqui

$dadosboleto["BairroCedente"] = $dados->BOLE_BairroCedente;
$dadosboleto["CidadeCedente"] = utf8_decode($dados->BOLE_CidadeCedente);
$dadosboleto["CEPCedente"] = $dados->BOLE_CEPCedente;
$dadosboleto["UFCedente"] = $dados->BOLE_UFCedente;


$dadosboleto["BOLE_GRCSUcategoria"] = $dados->BOLE_GRCSUcategoria;
$dadosboleto["BOLE_CNAE"] = $dados->BOLE_CNAE;



// $dadosboleto["cidade_uf"] = "";//Cidade / Estado



$dadosboleto["cedente"] = utf8_decode($dados->BOLE_NomeCedente);

// NÃO ALTERAR!
include_once("boletosphp/include/funcoes_cef_grcsu.php"); 
$dadosboleto = gerarBoleto($dadosboleto);
include("boletosphp/include/layout_cef_grcsu.php");
}

function boletos_bb($dados)
{
	// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Você deve ter recebido uma cópia da GNU Public License junto com     |
// | esse pacote; se não, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de João Prado Maia e Pablo Martins F. Costa				  |
// | 														              |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +--------------------------------------------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>              		             	|
// | Desenvolvimento Boleto Banco do Brasil: Daniel William Schultz / Leandro Maniezo / Rogério Dias Pereira|
// +--------------------------------------------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 30;
$taxa_boleto = 0;
$data_venc = date("d/m/Y", strtotime($dados->BOLE_Vencimento));  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = number_format($dados->BOLE_ValorDocumento, 2, ',', '.'); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = $dados->BOLE_Sequencial;
$dadosboleto["numero_documento"] = $dados->BOLE_NumDoc;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc));  // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc));  // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $dados->BOLE_NomeCliente;
$dadosboleto["endereco1"] = $dados->BOLE_EndeCliente;
$dadosboleto["endereco2"] = $dados->BOLE_Cidade." - ".$dados->BOLE_UF." - CEP ".$dados->BOLE_CEP." - CPF/CNPJ ".$dados->BOLE_CGC;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $dados->BOLE_InstrucaoBoleta;
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O BB
$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de ".$dados->BOLE_Multa."% ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "- Receber at&eacute; 30 dias ap&oacute;s o vencimento";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelos sistemas BisaWeb - http://www.bisaweb.com.br";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "10";
$dadosboleto["valor_unitario"] = "10";
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";// DM –Duplicata  Mercantil,  DS –Duplicata  de  Prestação  de  Serviços,  NP –Nota Promissória, BDP – Boleto de Proposta

$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "NP";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - BANCO DO BRASIL
$dadosboleto["agencia"] = $dados->BOLE_Agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $dados->BOLE_Conta; 	// Num da conta, sem digito

//Tratamento do campo de cadente
/*$cedente = explode('/',$dados->BOLE_AgenciaCedente);
$cedente = $cedente = explode('-',$cedente[1]);*/
$cedente = $dados->BOLE_NumeroConvenio;


//var_dump($dados->BOLE_NumCarteira);die();

// DADOS PERSONALIZADOS - BANCO DO BRASIL
$dadosboleto["convenio"] = $cedente;   // Num do convênio - REGRA: 6 ou 7 ou 8 dígitos
$dadosboleto["contrato"] = "999999"; // Num do seu contrato
$dadosboleto["carteira"] = $dados->BOLE_NumCarteira; // 17 - Com registro / 18 Sem registro
$dadosboleto["variacao_carteira"] = "027";  // Variação da Carteira, com traço (opcional) {Verificar no TabeBanc }

// TIPO DO BOLETO
$dadosboleto["formatacao_convenio"] = "7"; // REGRA: 8 p/ Convênio c/ 8 dígitos, 7 p/ Convênio c/ 7 dígitos, ou 6 se Convênio c/ 6 dígitos
$dadosboleto["formatacao_nosso_numero"] = "2"; // REGRA: Usado apenas p/ Convênio c/ 6 dígitos: informe 1 se for NossoNúmero de até 5 dígitos ou 2 para opção de até 17 dígitos


/*
#################################################
DESENVOLVIDO PARA CARTEIRA 18

- Carteira 18 com Convenio de 8 digitos
  Nosso número: pode ser até 9 dígitos

- Carteira 18 com Convenio de 7 digitos
  Nosso número: pode ser até 10 dígitos

- Carteira 18 com Convenio de 6 digitos
  Nosso número:
  de 1 a 99999 para opção de até 5 dígitos
  de 1 a 99999999999999999 para opção de até 17 dígitos

#################################################
*/


// SEUS DADOS
$dadosboleto["identificacao"] = $dados->BOLE_NomeCedente;
$dadosboleto["cpf_cnpj"] = $dados->BOLE_CNPJCPFCedente;
$dadosboleto["endereco"] =  $dados->BOLE_EndeCedente;//Coloque o endereço da sua empresa aqui
$dadosboleto["cidade_uf"] = "";//Cidade / Estado
$dadosboleto["cedente"] = $dados->BOLE_NomeCedente;

// NÃO ALTERAR!
include("boletosphp/include/funcoes_bb.php"); 
$dadosboleto = gerarBoleto($dadosboleto);
include("boletosphp/include/layout_bb.php");
}

function boleto_cef_d8($dados='')
{
header("Content-type: text/html; charset=utf-8");
	// +----------------------------------------------------------------------+
// | BoletoPhp - Versão Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Você deve ter recebido uma cópia da GNU Public License junto com     |
// | esse pacote; se não, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de João Prado Maia e Pablo Martins F. Costa				  |
// | 														              |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto CEF SIGCB: Davi Nunes Camargo				  |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 30;
$taxa_boleto = 0;
$data_venc = date("d/m/Y", strtotime($dados->BOLE_Vencimento));  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = number_format($dados->BOLE_ValorDocumento, 2, ',', '.'); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// Composição Nosso Numero - CEF SIGCB
$dadosboleto["nosso_numero1"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const1"] = "1"; //constanto 1 , 1=registrada , 2=sem registro
$dadosboleto["nosso_numero2"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const2"] = "4"; //constanto 2 , 4=emitido pelo proprio cliente
$dadosboleto["nosso_numero3"] =  $dados->BOLE_Sequencial; //'000015744'; // tamanho 9 -  Antes estava este campo:BOLE_Titulo

$dadosboleto["numero_documento"] = $dados->BOLE_NumDoc;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"]  = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"]   = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $dados->BOLE_NomeCliente;
$dadosboleto["endereco1"] = $dados->BOLE_EndeCliente;
$dadosboleto["endereco2"] = $dados->BOLE_Cidade." - ".$dados->BOLE_UF." - CEP ".$dados->BOLE_CEP;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = 'instuções do boleto';
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
/*$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de ".$dados->BOLE_Multa."% ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "- Receber at&eacute; 30 dias ap&oacute;s o vencimento";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelos sistemas BisaWeb - http://www.bisaweb.com.br";*/

$dadosboleto["instrucoes1"] = 'instruções do boleto';
$dadosboleto["instrucoes2"] = "";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";



// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //

// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $dados->BOLE_Agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $dados->BOLE_Conta; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $dados->BOLE_DigConta; 	// Digito do Num da conta

//Tratamento do campo de cadente
/*$cedente = explode('/',$dados->BOLE_AgenciaCedente);
$cedente = $cedente = explode('-',$cedente[1]);*/
$cedente = $dados->BOLE_NumeroConvenio;


// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = $cedente; // Código Cedente do Cliente, com 6 digitos (Somente Números)
$dadosboleto["carteira"] = "SR";  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = utf8_decode($dados->BOLE_NomeCedente);
$dadosboleto["cpf_cnpj"] = $dados->BOLE_CNPJCPFCedente;
$dadosboleto["endereco"] = utf8_decode(strtoupper($dados->BOLE_EndeCedente));//Coloque o endereço da sua empresa aqui
$dadosboleto["cidade_uf"] = "";//Cidade / Estado
$dadosboleto["cedente"] = utf8_decode($dados->BOLE_NomeCedente);

// NÃO ALTERAR!
include_once("boletosphp/include/funcoes_cef_d8.php"); 
$dadosboleto = gerarBoleto($dadosboleto);
include("boletosphp/include/layout_cef_d8.php");
}

function boleto_cef_proposta($dados='')
{
header("Content-type: text/html; charset=utf-8");

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <desenvlvimento10@bisa.com.br> |
// | Desenvolvimento Boleto PROPOSTA CEF SIGCB: Bruno Magnata			  |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 30;
$taxa_boleto = 0;
$data_venc = date("d/m/Y", strtotime($dados->BOLE_Vencimento));  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = number_format($dados->BOLE_ValorDocumento, 2, ',', '.'); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// Composição Nosso Numero - CEF SIGCB
$dadosboleto["nosso_numero1"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const1"] = "1"; //constanto 1 , 1=registrada , 2=sem registro
$dadosboleto["nosso_numero2"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const2"] = "4"; //constanto 2 , 4=emitido pelo proprio cliente
$dadosboleto["nosso_numero3"] =  $dados->BOLE_Sequencial; //'000015744'; // tamanho 9 -  Antes estava este campo:BOLE_Titulo

$dadosboleto["numero_documento"] = $dados->BOLE_NumDoc;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"]  = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"]   = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $dados->BOLE_NomeCliente." - CPF/CNPJ: ".$dados->BOLE_CGC;;
$dadosboleto["endereco1"] = $dados->BOLE_EndeCliente;
$dadosboleto["endereco2"] = $dados->BOLE_Cidade." - ".$dados->BOLE_UF." - CEP ".$dados->BOLE_CEP;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = utf8_decode($dados->BOLE_InstrucaoBoleta );
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
/*$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de ".$dados->BOLE_Multa."% ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "- Receber at&eacute; 30 dias ap&oacute;s o vencimento";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelos sistemas BisaWeb - http://www.bisaweb.com.br";*/

$dadosboleto["instrucoes1"] = utf8_decode($dados->BOLE_InstrucaoBoleta );
$dadosboleto["instrucoes2"] = "";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";



// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //

// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $dados->BOLE_Agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $dados->BOLE_Conta; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $dados->BOLE_DigConta; 	// Digito do Num da conta

//Tratamento do campo de cadente
/*$cedente = explode('/',$dados->BOLE_AgenciaCedente);
$cedente = $cedente = explode('-',$cedente[1]);*/
$cedente = $dados->BOLE_NumeroConvenio;


// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = $cedente; // Código Cedente do Cliente, com 6 digitos (Somente Números)
$dadosboleto["carteira"] = $dados->BOLE_NumCarteira;  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = utf8_decode($dados->BOLE_NomeCedente);
$dadosboleto["cpf_cnpj"] = $dados->BOLE_CNPJCPFCedente;
$dadosboleto["endereco"] = utf8_decode(strtoupper($dados->BOLE_EndeCedente));//Coloque o endereço da sua empresa aqui
$dadosboleto["cidade_uf"] = "";//Cidade / Estado
$dadosboleto["cedente"] = utf8_decode($dados->BOLE_NomeCedente);

// NÃO ALTERAR!
include_once("boletosphp/include/funcoes_cef_proposta.php"); 
$dadosboleto = gerarBoleto($dadosboleto);
include("boletosphp/include/layout_cef_proposta.php");
}

function boleto_cef_d10($dados='')
{
header("Content-type: text/html; charset=utf-8");

// +----------------------------------------------------------------------+
// | Equipe Coordenação Projeto BoletoPhp: <desenvlvimento10@bisa.com.br> |
// | Desenvolvimento Boleto PROPOSTA CEF SIGCB: Bruno Magnata			  |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 30;
$taxa_boleto = 0;
$data_venc = date("d/m/Y", strtotime($dados->BOLE_Vencimento));  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = number_format($dados->BOLE_ValorDocumento, 2, ',', '.'); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// Composição Nosso Numero - CEF SIGCB
$dadosboleto["nosso_numero1"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const1"] = "1"; //constanto 1 , 1=registrada , 2=sem registro
$dadosboleto["nosso_numero2"] = ""; // tamanho 3
$dadosboleto["nosso_numero_const2"] = "4"; //constanto 2 , 4=emitido pelo proprio cliente
$dadosboleto["nosso_numero3"] =  $dados->BOLE_Sequencial; //'000015744'; // tamanho 9 -  Antes estava este campo:BOLE_Titulo

$dadosboleto["numero_documento"] = $dados->BOLE_NumDoc;	// Num do pedido ou do documento
$dadosboleto["data_vencimento"]  = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"]   = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y", strtotime($dados->BOLE_DataProc)); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $dados->BOLE_NomeCliente;
$dadosboleto["endereco1"] = $dados->BOLE_EndeCliente;
$dadosboleto["endereco2"] = $dados->BOLE_Cidade." - ".$dados->BOLE_UF." - CEP ".$dados->BOLE_CEP;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = utf8_decode($dados->BOLE_InstrucaoBoleta );
$dadosboleto["demonstrativo2"] = "";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
/*$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de ".$dados->BOLE_Multa."% ap&oacute;s o vencimento";
$dadosboleto["instrucoes2"] = "- Receber at&eacute; 30 dias ap&oacute;s o vencimento";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelos sistemas BisaWeb - http://www.bisaweb.com.br";*/

$dadosboleto["instrucoes1"] = utf8_decode($dados->BOLE_InstrucaoBoleta );
$dadosboleto["instrucoes2"] = "";
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";



// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //

// DADOS DA SUA CONTA - CEF
$dadosboleto["agencia"] = $dados->BOLE_Agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $dados->BOLE_Conta; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $dados->BOLE_DigConta; 	// Digito do Num da conta

//Tratamento do campo de cadente
/*$cedente = explode('/',$dados->BOLE_AgenciaCedente);
$cedente = $cedente = explode('-',$cedente[1]);*/
$cedente = $dados->BOLE_NumeroConvenio;


// DADOS PERSONALIZADOS - CEF
$dadosboleto["conta_cedente"] = $cedente; // Código Cedente do Cliente, com 6 digitos (Somente Números)
$dadosboleto["carteira"] = $dados->BOLE_NumCarteira;  // Código da Carteira: pode ser SR (Sem Registro) ou CR (Com Registro) - (Confirmar com gerente qual usar)

// SEUS DADOS
$dadosboleto["identificacao"] = utf8_decode($dados->BOLE_NomeCedente);
$dadosboleto["cpf_cnpj"] = $dados->BOLE_CNPJCPFCedente;
$dadosboleto["endereco"] = utf8_decode(strtoupper($dados->BOLE_EndeCedente));//Coloque o endereço da sua empresa aqui
$dadosboleto["cidade_uf"] = "";//Cidade / Estado
$dadosboleto["cedente"] = utf8_decode($dados->BOLE_NomeCedente);

// NÃO ALTERAR!
include_once("boletosphp/include/funcoes_cef_d10.php"); 
$dadosboleto = gerarBoleto($dadosboleto);
include("boletosphp/include/layout_cef_d10.php");
}