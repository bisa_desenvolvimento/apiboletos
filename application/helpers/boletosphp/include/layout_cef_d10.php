<?php
header("Content-type: text/html; charset=ISO-8859-1");

// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto : <desenvolvimento10@bisa.com.br>         |
// | Desenvolvimento Boleto CEF - D10: Bruno Magnata                      |
// +----------------------------------------------------------------------+
?>

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<HTML>
<HEAD>
<TITLE><?php echo $dadosboleto["identificacao"]; ?></TITLE>
<META http-equiv=Content-Type content=text/html charset=ISO-8859-1>
<meta name="Generator" content="Projeto BoletoPHP - www.boletophp.com.br - Licen�a GPL" />
<style type=text/css>
<!--.cp {  font: bold 10px Arial; color: black}
<!--.ti {  font: 9px Arial, Helvetica, sans-serif}
<!--.ld { font: bold 15px Arial; color: #000000}
<!--.ct { FONT: 9px "Arial Narrow"; COLOR: #000033} 
<!--.cn { FONT: 9px Arial; COLOR: black }
<!--.bc { font: bold 20px Arial; color: #000000 }
<!--.ld2 { font: bold 12px Arial; color: #000000 }
--></style> 
</head>

<BODY text=#000000 bgColor=#ffffff topMargin=0 rightMargin=0>
<table width=666 cellspacing=0 cellpadding=0 border=0><tr><td valign=top class=cp><DIV ALIGN="CENTER">Instru��es 
de Impress�o</DIV></TD></TR><TR><TD valign=top class=cp><DIV ALIGN="left">
<p>
<li>Imprima em impressora jato de tinta (ink jet) ou laser em qualidade normal ou alta (N�o use modo econ�mico).<br>
<li>Utilize folha A4 (210 x 297 mm) ou Carta (216 x 279 mm) e margens m�nimas � esquerda e � direita do formul�rio.<br>
<li>Corte na linha indicada. N�o rasure, risque, fure ou dobre a regi�o onde se encontra o c�digo de barras.<br>
<li>Caso n�o apare�a o c�digo de barras no final, clique em F5 para atualizar esta tela.
<li>Caso tenha problemas ao imprimir, copie a seq�encia num�rica abaixo e pague no caixa eletr�nico ou no internet banking:<br><br>
<span class="ld2">
&nbsp;&nbsp;&nbsp;&nbsp;Linha Digit�vel: &nbsp;<?php echo $dadosboleto["linha_digitavel"]?><br>
&nbsp;&nbsp;&nbsp;&nbsp;Valor: &nbsp;&nbsp;R$ <?php echo $dadosboleto["valor_boleto"]?><br>
</span>
</DIV></td></tr>
</table><br>
<table cellspacing=0 cellpadding=0 width=666 border=0><TBODY><TR><TD class=ct width=666><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0></TD></TR><TR><TD class=ct width=666><div align=right><b class=cp>Recibo do Sacado</b></div></TD></tr></tbody></table><table width=666 cellspacing=5 cellpadding=0 border=0><tr><td width=41></TD></tr></table>
<BR>
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tr>
    <td class=cp width=150> 
      <span class="campo"><IMG src="<?php echo base_url('assets/boletos'); ?>/imagens/logocaixa.jpg" width="150" height="40" border=0></span>
    </td>
    <td width=3 valign=bottom>
      <img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0>
    </td>
    <td class=cpt width=58 valign=bottom>
      <div align=center>
        <font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"]?></font>
      </div>
    </td>
    <td width=3 valign=bottom>
      <img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0>
    </td>
    <td class=ld align=right width=453 valign=bottom>
      <span class=ld> 
        <span class="campotitulo">
          <?php echo $dadosboleto["linha_digitavel"]?>
        </span>
      </span>
    </td>
  </tr>
  <tbody>
    <tr>  
      <td colspan=5><img height=2 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0></td>  
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td width=7></td>
      <td  width=500 class=cp> 
      <br><br><br> 
    </td>
      <td width=159></td>
    </tr>
  </tbody>
</table>


<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=380 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=380 border=0></td>   
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=85 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=85 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>  
    </tr>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=380 height=13>Benefici�rio</td> 
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=85 height=13>CPF/CNPJ</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>Ag�ncia/C�digo do Cedente</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top width=380 height=12><span class="campo"><?php echo $dadosboleto["cedente"]; ?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=85 height=12><span class="campo"><?php echo $dadosboleto["cpf_cnpj"]?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=180 height=12><span class="campo"><?php echo $dadosboleto["agencia_codigo"]?></span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=380 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=380 border=0></td>   
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=85 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=85 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>  
    </tr>
  </tbody>
</table>
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=380 height=13>Endere�o Benefici�rio</td> 
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=85 height=13>UF</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>CEP</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top width=380 height=12><span class="campo"><?php echo $dadosboleto["endereco"]; ?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=85 height=12><span class="campo"><?php echo $dadosboleto["UFCedente"] ?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=180 height=12><span class="campo"><?php echo $dadosboleto["CEPCedente"] ?></span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=380 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=380 border=0></td>   
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=85 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=85 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>  
    </tr>
  </tbody>
</table>
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>Data do documento</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>N� do Documento</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>Esp�cie Documento</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=89 height=13>Carteira</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=85 height=13>Data do Processamento</td>


      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>Nosso N�mero</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=left width=90 height=12><?php echo $dadosboleto["data_documento"]?></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=left width=90 height=12><?php echo $dadosboleto["numero_documento"]?></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=left width=90 height=12><?php echo $dadosboleto["especie_doc"]?></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=left width=89 height=12><?php echo $dadosboleto["carteira"]?></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=left width=85 height=12><?php echo $dadosboleto["data_processamento"]?></td>

      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12><?php echo substr($dadosboleto["nosso_numero"], 0,strlen($dadosboleto["nosso_numero"])-1).'-'.substr($dadosboleto["nosso_numero"], -1)?></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=87 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=89 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=85 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=85 border=0></td>

      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=473 height=13>Pagador</td>       
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>CPF/CNPJ</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top width=473 height=12><span class="campo"><?php echo $dadosboleto["sacado"]; ?></span></td>     
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=180 height=12><span class="campo"><?php echo $dadosboleto["cnpjsacado"]?></span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=473 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=473 border=0></td>        
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>  
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=473 height=13>Endere�o do Pagador</td>       
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=80 height=13>UF</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=100 height=13>CEP</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top width=473 height=12><span class="campo"><?php echo $dadosboleto["endereco1"]; ?></span></td>     
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=80 height=12><span class="campo"><?php echo $dadosboleto["ufsacado"]?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=100 height=12><span class="campo"><?php echo $dadosboleto["cepsacado"]?></span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=473 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=473 border=0></td>        
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=80 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=80 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=94 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=94 border=0></td>   
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=473 height=13>Pagador/Avalista</td>       
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>CPF/CNPJ</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top width=473 height=12><span class="campo"></span></td>     
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=180 height=12><span class="campo"></span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=473 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=473 border=0></td>        
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>  
    </tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr>
              <td class=ct valign=top width=7 height=13><img height=40 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12><img height=40 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1><img height=40 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0></td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=468 rowspan=5><font class=ct>
      TEXTO DE RESPONSABILIDADE DO CEDENTE</font><br><span class=cp> <FONT class=ct>
        <br><br>        
        <h4>
        ESTE BOLETO SE REFERE A UMA PROPOSTA J&Agrave; FEITA A VOC&Ecirc; E O SEU PAGAMENTO N&Acirc;O � OBRIGAT&Oacute;RIO.<br>
        Deixar de pag�-lo n�o dar� causa a protesto, a cobran�a judicial ou extrajudicial,<br>
        nem a inser��o de seu nome em cadastro de restri��o ao cr�dito.<br>
        Pagar at� a data de vecimento significa aceitar a proposta.<br>
        informa��es adicionais sobre a proposta e sobre o respectivo contrato poder�o ser solicitadas a qualquer momento ao benefici�rio,<br>
        por meio de seus canais de atendimento.<br><br></h4>  
      </FONT> 
      </span></td>     
    </tr>
  </tbody>
</table>

<!--LINHA Footer BEGIN-->
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=80 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=80 border=0></td>

      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>Moeda</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>Quantidade</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>Valor</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=90 height=13>Vencimento</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=80 height=13>Valor do Documento</td>


      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>Autentica��o Mec�nica - Recibo do Sacado</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=90 height=12></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=90 height=12></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=90 height=12></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=90 height=12><?php echo $dadosboleto["data_vencimento"]?></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=80 height=12>R$<?php echo $dadosboleto["valor_boleto"]?></td>

      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=90 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=90 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=80 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=80 border=0></td>

      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>
<!--FIM-->
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody><tr><td width=7></td><td  width=500 class=cp> 
  <br><br><br> 
  </td><td width=159></td></tr>
  </tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666 border=0>
<tr><td class=ct width=666></td></tr>
  <tbody>
  <tr>
  <td class=ct width=666> 
  <div align=right>Corte na linha pontilhada</div></td></tr>

  <tr>
  <td class=ct width=666><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0></td>
  </tr>
  </tbody>
</table><br>

<table cellspacing=0 cellpadding=0 width=666 border=0><tr><td class=cp width=150> 
  <span class="campo"><IMG 
      src="<?php echo base_url('assets/boletos'); ?>/imagens/logocaixa.jpg" width="150" height="40" 
      border=0></span></td>
<td width=3 valign=bottom><img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0></td>
<td class=cpt width=58 valign=bottom><div align=center><font class=bc><?php echo $dadosboleto["codigo_banco_com_dv"]?></font></div></td>
<td width=3 valign=bottom><img height=22 src=<?php echo base_url('assets/boletos'); ?>/imagens/3.png width=2 border=0></td>
<td class=ld align=right width=453 valign=bottom><span class=ld> 
<span class="campotitulo">
<?php echo $dadosboleto["linha_digitavel"]?>
</span></span></td>
</tr>
<tbody><tr>
<td colspan=5><img height=2 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0></td></tr>
</tbody>
</table>
<table cellspacing=0 cellpadding=0 border=0><tbody>
<tr>
  <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
  <td class=ct valign=top width=472 height=13>Local  de pagamento</td>
  <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
  <td class=ct valign=top width=180 height=13>Vencimento</td>
</tr>
<tr>
  <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
  <td class=ct valign=top width=472 height=12><b>PREFERENCIALMENTE NAS CASAS LOT�RICAS AT� O VALOR LIMITE.<b></td>
  <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
  <td class=cp valign=top align=right width=180 height=12> 
    <span class="campo">
    <?php echo ($data_venc != "") ? $dadosboleto["data_vencimento"] : "Contra Apresenta��o" ?>
    </span>
  </td>
</tr>
<tr>
  <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
  <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
  <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
  <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
</tr>
</tbody>
</table>
<table cellspacing=0 cellpadding=0 border=0>
    <tbody>
      <tr>
        <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        <td class=ct valign=top width=358 height=13>Bemefici�rio</td>        
        <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        <td class=ct valign=top width=107 height=13>CNPJ/CPF</td>                                            
        <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        <td class=ct valign=top width=180 height=13>Ag�ncia/C�digo do cedente</td>
      </tr>

      <tr>
		  <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
		  <td class=cp valign=top width=358 height=12><span class="campo ct"><b><?php echo $dadosboleto["cedente"]?></b></span></td>		
		  <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
		  <td class=cp valign=top width=107 height=12><span class="campo"><?php echo $dadosboleto["cpf_cnpj"] ?></span></td>                      
		  <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
		  <td class=cp valign=top align=right width=180 height=12><span class="campo"><?php echo $dadosboleto["agencia_codigo"]?></span></td>
      </tr>

      <tr>
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=158 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=358 border=0></td>        
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=107 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=107 border=0></td>
        <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
        <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
      </tr>
    </tbody>
</table>
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=113 height=13>Data do documento</td>
      <td class=ct valign=top width=7 height=13> <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=133 height=13>N<u>o</u> documento</td>
      <td class=ct valign=top width=7 height=13> <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=62 height=13>Esp�cie</td>
      <td class=ct valign=top width=7 height=13> <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=34 height=13>Aceite</td><td class=ct valign=top width=7 height=13> 
      <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=102 height=13>Data processamento</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>Nosso n�mero</td>
    </tr>
    <tr>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=113 height=12><div align=left><span class="campo"><?php echo $dadosboleto["data_documento"]?></span></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top width=133 height=12><span class="campo"><?php echo $dadosboleto["numero_documento"]?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=62 height=12><div align=left><span class="campo"><?php echo $dadosboleto["especie_doc"]?></span></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=34 height=12><div align=left><span class="campo"><?php echo $dadosboleto["aceite"]?></span></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=102 height=12><div align=left><span class="campo"><?php echo $dadosboleto["data_processamento"]?></span></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12><span class="campo"><?php echo substr($dadosboleto["nosso_numero"], 0,strlen($dadosboleto["nosso_numero"])-1).'-'.substr($dadosboleto["nosso_numero"], -1)?>
           </span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
	  <td valign=top width=113 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=113 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=133 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=133 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=62 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=62 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=34 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=34 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=102 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=102 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13> <img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top COLSPAN="3" height=13>Uso 
      do banco</td><td class=ct valign=top height=13 width=7><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=83 height=13>Carteira</td><td class=ct valign=top height=13 width=7><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=43 height=13>Moeda</td>
      <td class=ct valign=top height=13 width=7><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=103 height=13>Quantidade</td>
      <td class=ct valign=top height=13 width=7><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=102 height=13>Valor</td>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=ct valign=top width=180 height=13>(=) Valor documento</td>
    </tr>
    <tr> 
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td valign=top class=cp height=12 COLSPAN="3"><div align=left></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=83><div align=left> <span class="campo"><?php echo $dadosboleto["carteira"]?></span></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=43><div align=left><span class="campo"><?php echo $dadosboleto["especie"]?></span></div></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=103><span class="campo"><?php echo $dadosboleto["quantidade"]?></span></td>
      <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top  width=102><span class="campo"><?php echo $dadosboleto["valor_unitario"]?></span></td>
      <td class=cp valign=top width=7 height=12> <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
      <td class=cp valign=top align=right width=180 height=12><span class="campo">R$<?php echo $dadosboleto["valor_boleto"]?></span></td>
    </tr>
    <tr>
      <td valign=top width=7 height=1> <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=75 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=31 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=31 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=83 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=83 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=43 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=43 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=103 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=103 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=102 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=102 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody> 
</table>
<table cellspacing=0 cellpadding=0 width=666 border=0>
  <tbody>
    <tr>
      <td align=right width=10>
        <table cellspacing=0 cellpadding=0 border=0 align=left>
          <tbody> 
            <tr>
              <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            </tr>
            <tr> 
              <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            </tr>
            <tr> 
              <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0></td>
            </tr>
          </tbody>
        </table>
      </td>
      <td valign=top width=468 rowspan=5><font class=ct>
      TEXTO DE RESPONSABILIDADE DO CEDENTE</font><br><span class=cp> <FONT class=ct>
        <br><br>
        <br><br>
        <h5>
        ESTE BOLETO SE REFERE A UMA PROPOSTA J&Agrave; FEITA A VOC&Ecirc; E O SEU PAGAMENTO N&Acirc;O � OBRIGAT&Oacute;RIO.<br>
        Deixar de pag�-lo n�o dar� causa a protesto, a cobran�a judicial ou extrajudicial,<br>
        nem a inser��o de seu nome em cadastro de restri��o ao cr�dito.<br>
        Pagar at� a data de vecimento significa aceitar a proposta.<br>
        informa��es adicionais sobre a proposta e sobre o respectivo contrato poder�o ser solicitadas a qualquer momento ao benefici�rio,<br>
        por meio de seus canais de atendimento.</h5>  
      </FONT> 
      </span></td>
        <td align=right width=188>
          <table cellspacing=0 cellpadding=0 border=0>
            <tbody>
              <tr>
                <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(-) Desconto</td>
              </tr>
              <tr> 
                <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
                <td class=cp valign=top align=right width=180 height=12></td>
              </tr>
              <tr> 
                <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
                <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0>
              </td>
              </tr>
            </tbody>
          </table>
        </td>
    </tr>
    <tr>
      <td align=right width=10> 
    <table cellspacing=0 cellpadding=0 border=0 align=left>
      <tbody>
        <tr>
          <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        </tr>
          <tr>
            <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
          </tr>
          <tr>
            <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0></td>
          </tr>
        </tbody>
      </table>
    </td>
    <td align=right width=188>
      <table cellspacing=0 cellpadding=0 border=0>
        <tbody>
          <tr>
            <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            <td class=ct valign=top width=180 height=13>(-) Outras dedu��es</td>
          </tr>
          <tr>
            <td class=cp valign=top width=7 height=12> <img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            <td class=cp valign=top align=right width=180 height=12></td>
          </tr>
          <tr>
            <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
            <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align=right width=10> 
      <table cellspacing=0 cellpadding=0 border=0 align=left>
        <tbody>
          <tr>
            <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
          </tr>
          <tr>
            <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
          </tr>
          <tr>
            <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0></td>
          </tr>
        </tbody>
      </table>
    </td>
    <td align=right width=188> 
      <table cellspacing=0 cellpadding=0 border=0>
        <tbody>
          <tr>
            <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            <td class=ct valign=top width=180 height=13>(+) Mora / Multa</td>
          </tr>
          <tr>
            <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
            <td class=cp valign=top align=right width=180 height=12></td>
          </tr>
          <tr> 
            <td valign=top width=7 height=1> <img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
            <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
<tr>
  <td align=right width=10>
    <table cellspacing=0 cellpadding=0 border=0 align=left>
      <tbody>
        <tr> 
          <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        </tr>
        <tr>
          <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td>
        </tr>
        <tr>
          <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=1 border=0></td>
        </tr>
        </tbody>
          </table>
          </td>
<td align=right width=188> 
<table cellspacing=0 cellpadding=0 border=0><tbody><tr> <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(+) 
Outros acr�scimos</td></tr><tr> <td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr><tr><td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td><td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td></tr></tbody></table></td></tr><tr><td align=right width=10><table cellspacing=0 cellpadding=0 border=0 align=left><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td></tr></tbody></table>
</td>
<td align=right width=188>
  <table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=180 height=13>(=) 
Valor cobrado</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top align=right width=180 height=12></td></tr></tbody> 
</table>
</td>
</tr>
</tbody>
</table>

<table cellspacing=0 cellpadding=0 width=666 border=0><tbody><tr><td valign=top width=666 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=666 border=0></td></tr></tbody></table><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=ct valign=top width=659 height=13>NOME DO PAGADOR/CPF/CNPJ/ENDERE�O/CIDADE/UF/CEP</td></tr><tr><td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top width=659 height=12><span class="campo">
<?php echo $dadosboleto["sacado"]?>
</span> 
</td>
</tr></tbody></table><table cellspacing=0 cellpadding=0 border=0><tbody><tr><td class=cp valign=top width=7 height=12><img height=12 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top width=659 height=12><span class="campo">
<?php echo $dadosboleto["endereco1"]?>
</span> 
</td>
</tr></tbody></table>
<table cellspacing=0 cellpadding=0 border=0>
  <tbody>
    <tr>
      <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=1 border=0></td><td class=cp valign=top width=472 height=13> 
        <span class="campo">
        <?php echo $dadosboleto["endereco2"]?>
        </span></td>
      <!-- <td class=ct valign=top width=7 height=13><img height=13 src=<?php echo base_url('assets/boletos'); ?>/imagens/1.png width=0 border=0></td>
      <td class=ct valign=top width=180 height=13></td> -->
    </tr>
    <tr>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=472 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=472 border=0></td>
      <td valign=top width=7 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=7 border=0></td>
      <td valign=top width=180 height=1><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/2.png width=180 border=0></td>
    </tr>
  </tbody>
</table>
<TABLE cellSpacing=0 cellPadding=0 border=0 width=666><TBODY><TR>
  <TD class=ct  width=7 height=12></TD>
  <TD class=ct  width=409 >Sacador/Avalista</TD><TD class=ct  width=250 ><div align=right>Autentica��o 
mec�nica - <b class=cp>Ficha de Compensa��o</b></div></TD></TR>
<TR><TD class=ct  colspan=3 ></TD></tr></tbody></table><TABLE cellSpacing=0 cellPadding=0 width=666 border=0><TBODY><TR><TD vAlign=bottom align=left height=50><?php fbarcode($dadosboleto["codigo_barras"]); ?> 
 </TD>
</tr></tbody></table><TABLE cellSpacing=0 cellPadding=0 width=666 border=0><TR><TD class=ct width=666></TD></TR><TBODY><TR><TD class=ct width=666><div align=right>Corte 
na linha pontilhada</div></TD></TR><TR><TD class=ct width=666><img height=1 src=<?php echo base_url('assets/boletos'); ?>/imagens/6.png width=665 border=0></TD></tr></tbody></table>
<div style="page-break-after: always;page-break-inside: avoid;" /></div>
</BODY></HTML>
