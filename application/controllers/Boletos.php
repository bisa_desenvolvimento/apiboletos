<?php
ini_set('memory_limit', '2048M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Retorno das requisições
	101 - As requisicoes precisam ser do tipo POST!
	102 - Token de segurança inválido!
	103 - Erro durante o processo do boleto!
	104 - Campos são requeridos
	105 - Boleto não encontrado
	106 - Erro durante o cancelamento do boleto
	107 - Banco não suportado para leitura
	200 - Acao realizada com sucesso!
 */
/*
	Status do estado do boleto
	0 - Não gerado arquivo de remessa
	1 - Gerado arquivo de remessa
	2 - Cancelado
	3 - Editado e gerado novo arquivo de remessa
	4 - Boleto dado Baixa
	5 - Cacelado por período
 */


class Boletos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Boletos_model');	
	}
	/**
	 * [cadastrarBoletos description]Função para cadastro de boletos, sendo obrigatório a requisição do tipo POST
	 * Validação do token de seguração e obrigadoriedade dos campos, em caso de cadastro o sistema retornará o código
	 * do boleto cadastrado no formato base_64 e url_encode e caso não retornará com os códigos e mensagens de erro referente a 
	 * cada problema.
	 * @return [type] [description] Json - Status,codigo do status e  Descrição 
	 */
	public function cadastrarBoletos()
	{						
		//Verifica se a requisição foi realizada pelo método post
		if($_SERVER["REQUEST_METHOD"] == 'POST'){

			$codigoValidacao = $this->input->post('token');

			$validado = $this->verificarCodigoValidacao($codigoValidacao);

			if($validado){

				$data = array(
								'BANC_CodigoBanco'           => $this->input->post('BANC_CodigoBanco'),
								'BOLE_Sequencial'            => $this->input->post('BOLE_Sequencial'),	
								'BOLE_NumDoc'                => $this->input->post('BOLE_NumDoc'),							
								'BOLE_ValorDocumento'        => $this->input->post('BOLE_ValorDocumento'),
								'BOLE_Vencimento'            => $this->input->post('BOLE_Vencimento'),																			
								'BOLE_NomeCliente'           => $this->input->post('BOLE_NomeCliente'),									
								'BOLE_CGC'                   => $this->input->post('BOLE_CGC'),								
								'BOLE_CEP'                   => $this->input->post('BOLE_CEP'),
								
								'BOLE_Agencia'               => $this->input->post('BOLE_Agencia'),
								'BOLE_DigAgencia'            => $this->input->post('BOLE_DigAgencia'),
								'BOLE_Conta'                 => $this->input->post('BOLE_Conta'),
								'BOLE_DigConta'              => $this->input->post('BOLE_DigConta'),
								'BOLE_NumCarteira'           => $this->input->post('BOLE_NumCarteira'),
								'BOLE_NumeroConvenio'        => $this->input->post('BOLE_NumeroConvenio'),
								'CLIE_codigo'                => $this->input->post('CLIE_codigo'),
								'STATUS_boleto'              => '0',
								'BISAUSUA_LastUpdate'        => date('Y-m-d H:i:s'),								
								'BOLE_NomeCedente'           => $this->input->post('BOLE_NomeCedente'),
								'BOLE_CNPJCPFCedente'        => $this->input->post('BOLE_CNPJCPFCedente'),
								'BOLE_EndeCedente'           => $this->input->post('BOLE_EndeCedente')
							);
				
				$camposVazio = '';
				foreach ($data as $campo => $valor) {
					if($valor == ''){
						$camposVazio .= "|".$campo; 
					}
				}
				
				$naoObrigatorios = array(
					'BOLE_InstrucaoBoleta'       => $this->input->post('BOLE_InstrucaoBoleta'),

					'BOLE_Tipo_Desconto'         => $this->input->post('BOLE_Tipo_DescontoDocumento'),
					'BOLE_Desconto'              => $this->input->post('BOLE_Valor_DescontoDocumento'),
					'BOLE_Deducao'               => $this->input->post('BOLE_Deducao')?:'0',
					'BOLE_Multa'                 => $this->input->post('BOLE_Multa')?:'0',
					
					'BOLE_Juros'                 => $this->input->post('BOLE_Juros')?:'0', // Bruno Magnata 20/03/17 Mantis[5194]
					
					'BOLE_Acrescimo'             => $this->input->post('BOLE_Acrescimo')?:'0',
					'BOLE_Bairro'                => $this->input->post('BOLE_Bairro')?:'NI',
					'BOLE_Titulo'                => $this->input->post('BOLE_Titulo')?:'0',
					'BOLE_DataProc'              => $this->input->post('BOLE_DataProc')?:date('Y-m-d'),
					'BOLE_Valor'                 => $this->input->post('BOLE_Valor')?:'0',
					'BOLE_ValCobrado'            => $this->input->post('BOLE_ValCobrado')?:'0',
					'BOLE_EndeCliente'           => $this->input->post('BOLE_EndeCliente')?:'NI',	
					'BOLE_Cidade'                => $this->input->post('BOLE_Cidade')?:'NI',
					'BOLE_UF'                    => $this->input->post('BOLE_UF')?:'0',
					'BOLE_AgenciaCedente'        => $this->input->post('BOLE_AgenciaCedente')?:'0',

					// GRCSU
					'BOLE_Leiaute'              => $this->input->post('BOLE_Leiaute')?:'1', // 1 - Padrão / 2 - Empresa-Prédio / 3 - Guia de Arrecadação de Investimento / 4 - Arrecadação-Recebimento D8
					'BOLE_EndeCedente'          => $this->input->post('BOLE_EndeCedente')?:'NI',
					'BOLE_BairroCedente'        => $this->input->post('BOLE_BairroCedente')?:'NI',
					'BOLE_CidadeCedente'        => $this->input->post('BOLE_CidadeCedente')?:'NI',
					'BOLE_UFCedente'            => $this->input->post('BOLE_UFCedente')?:'NI',
					'BOLE_CEPCedente'           => $this->input->post('BOLE_CEPCedente')?:'NI',					
					'BOLE_TipoContribuicao'     => $this->input->post('BOLE_TipoContribuicao')?:'NI',
					'BOLE_GRCSUcategoria' 		=> $this->input->post('BOLE_GRCSUcategoria')?:'NI',
					'BOLE_CNAE'                 => $this->input->post('BOLE_CNAE')?:'NI',

					'Bole_EntidadeSindical'     => $this->input->post('Bole_EntidadeSindical')?:'NI',

					'BOLE_grupomassa'           => $this->input->post('BOLE_grupomassa')?:'NI' // Mantis 5432  

					);

				$data = array_merge($naoObrigatorios, $data);
				
				if($camposVazio === ''){
					
					$idApi = $this->Boletos_model->cadastrarBoletos($data);

					if($idApi){
						$retorno['status']    = true;						
						$retorno['codstatus'] = 200;
						$retorno['codigoApi'] = urlencode(base64_encode($idApi));
						$retorno['msg'] = 'Acao realizada com sucesso!';				
					}else{
						$retorno['status']    = false;	
						$retorno['codstatus'] = 103;		
						$retorno['msg'] = 'Erro durante o processo de geracao do boleto!';
					}
				}else{
					$retorno['status']    = false;	
					$retorno['codstatus'] = 104;		
					$retorno['msg'] = 'Os seguinte campos são requeridos:'.$camposVazio;
				}

			}else{
				$retorno['status']    = false;	
				$retorno['codstatus'] = 102;		
				$retorno['msg'] = 'Token de segurança inválido!';
			}

		}else{
			$retorno['status']    = false;	
			$retorno['codstatus'] = 101;		
			$retorno['msg'] = 'As requisicoes precisam ser do tipo POST!';
		}

		echo json_encode($retorno);
	}
	
	public function verificarboletoAPI() {
		$data = array(
			'mes' => $this->input->post('mes'),
            'ano' => $this->input->post('ano')
		);
		$resultado = $this->Boletos_model->verificaBoleto($data);
		$resultado['resultado'] = count($resultado);
		echo json_encode($resultado);
	}

	/**
	 * [visualizarBoletos description]Função responsável de liberar a visualização do boleto, sendo necessário
	 * o token de segurança, código do cliente, código da api, é necessário que o boleto esteja no status 1 ou 3
	 * que respectivamente são de aquivo gerado para o banco e alterado e gerado arquivo.
	 * @return [type] [description]
	 */
	public function visualizarBoletos()
	{		
		//v(urlencode(base64_encode(13)));
		$codigoCliente = $this->uri->segment(3);
		$token         = $this->uri->segment(4);
		$codigoApi     = base64_decode(urldecode($this->uri->segment(5)));
		
		$validado = $this->verificarCodigoValidacao($token);

		if($validado){

				$data = array(
							'CLIE_codigo' => $codigoCliente,
							'API_codigo'   => $codigoApi
							);

				$dados = $this->Boletos_model->pegarBoletoId($data);
				
                
				if($dados){

					$data['dados'] = $dados[0];

					if($data['dados']->BANC_CodigoBanco == 104){

							switch ($data['dados']->BOLE_Leiaute) {
								case '2':									
										$this->load->view('visualizar_boletos',$data);																
									break;
								case '3':									
										$this->load->view('visualizar_boletos_grcsu',$data);																
									break;
								case '4':									
										boleto_cef_proposta($dados);																
									break;									
								default:
									$this->load->view('visualizar_boletos',$data);								
									break;
								}

					}else{
						$this->load->view('visualizar_boletos_bb',$data);
					}
					
/*
					$mpdf = new mPDF();

					$html = $this->load->view('visualizar_boletos',$data,TRUE);
					$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');

					$mpdf->writeHTML($html);

					$this->ignore_invalid_utf8 = true;	
					
					$mpdf->Output();	*/
										
																			
				}else{
					$retorno['status']    = false;	
					$retorno['codstatus'] = 105;		
					$retorno['msg'] = 'Boleto nao encotrado!';
					echo json_encode($retorno);
				}

		}else{
			$retorno['status']    = false;	
			$retorno['codstatus'] = 102;		
			$retorno['msg'] = 'Token de seguranca invalido!';
			echo json_encode($retorno);
		}
	}	

	/**
	 * [visualizarBoletos description]Função responsável de liberar a visualização do boleto, sendo necessário
	 * o token de segurança, código do cliente, código da api, é necessário que o boleto esteja no status 1 ou 3
	 * que respectivamente são de aquivo gerado para o banco e alterado e gerado arquivo.
	 * @return [type] [description]
	 */
	public function PreVisualizarBoletos()
	{		
		//v(urlencode(base64_encode(13)));
		$codigoCliente = $this->uri->segment(3);
		$token         = $this->uri->segment(4);
		$codigoApi     = base64_decode(urldecode($this->uri->segment(5)));
		
		$validado = $this->verificarCodigoValidacao($token);
		
		if($validado){

				$data = array(
							'CLIE_codigo' => $codigoCliente,
							'API_codigo'   => $codigoApi
							);							
							

				$dados = $this->Boletos_model->preVisualizarBoletoId($data);				
                
				if($dados){

					$data['dados'] = $dados[0];					
					
					if($data['dados']->BANC_CodigoBanco == 104){
						$this->load->view('visualizar_boletos',$data);
					}else{
						$this->load->view('visualizar_boletos_bb',$data);
					}										
																			
				}else{
					$retorno['status']    = false;	
					$retorno['codstatus'] = 105;		
					$retorno['msg'] = 'Boleto nao encotrado!';
					echo json_encode($retorno);
				}

		}else{
			$retorno['status']    = false;	
			$retorno['codstatus'] = 102;		
			$retorno['msg'] = 'Token de seguranca invalido!';
			echo json_encode($retorno);
		}
	}	

	/**
	 * [alterarBoletos description] Função responsável para realizar as alterações do boleto, sendo necessário
	 * o token de segurança, código do cliente e código da api para realização das alterações alem dos dados
	 * a serem alterados, não poderá ser alterado boletos com os status 0, 2, 4 e 5
	 * @return [type] [description]
	 */
	public function alterarBoletos()
	{						
		//Verifica se a requisição foi realizada pelo método post
		if($_SERVER["REQUEST_METHOD"] == 'POST'){

			$codigoValidacao = $this->input->post('token');
			$codigoApi = base64_decode(urldecode($this->input->post('codigoApi')));			

			$validado = $this->verificarCodigoValidacao($codigoValidacao);

			if($validado){

				$data = array(
								'BANC_CodigoBanco'           => $this->input->post('BANC_CodigoBanco'),
								'BOLE_Sequencial'            => $this->input->post('BOLE_Sequencial'),	
								'BOLE_NumDoc'                => $this->input->post('BOLE_NumDoc'),							
								'BOLE_ValorDocumento'        => $this->input->post('BOLE_ValorDocumento'),
								'BOLE_Vencimento'            => $this->input->post('BOLE_Vencimento'),										
								'BOLE_Desconto'              => $this->input->post('BOLE_Desconto'),
								'BOLE_Deducao'               => $this->input->post('BOLE_Deducao'),
								'BOLE_Multa'                 => $this->input->post('BOLE_Multa'),
								'BOLE_Acrescimo'             => $this->input->post('BOLE_Acrescimo'),								
								'BOLE_NomeCliente'           => $this->input->post('BOLE_NomeCliente'),
								'BOLE_EndeCliente'           => $this->input->post('BOLE_EndeCliente'),								
								'BOLE_CGC'                   => $this->input->post('BOLE_CGC'),
								'BOLE_Cidade'                => $this->input->post('BOLE_Cidade'),
								'BOLE_UF'                    => $this->input->post('BOLE_UF'),
								'BOLE_CEP'                   => $this->input->post('BOLE_CEP'),
								
								'BOLE_Agencia'               => $this->input->post('BOLE_Agencia'),
								'BOLE_DigAgencia'            => $this->input->post('BOLE_DigAgencia'),
								'BOLE_Conta'                 => $this->input->post('BOLE_Conta'),
								'BOLE_DigConta'              => $this->input->post('BOLE_DigConta'),
								'BOLE_NumCarteira'           => $this->input->post('BOLE_NumCarteira'),
								'BOLE_NumeroConvenio'        => $this->input->post('BOLE_NumeroConvenio'),
								'CLIE_codigo'                => $this->input->post('CLIE_codigo'),
								'STATUS_boleto'              => '0',
								'BISAUSUA_LastUpdate'        => date('Y-m-d H:i:s'),
								
								'BOLE_NomeCedente'           => $this->input->post('BOLE_NomeCedente'),
								'BOLE_CNPJCPFCedente'        => $this->input->post('BOLE_CNPJCPFCedente'),
								'BOLE_EndeCedente'           => $this->input->post('BOLE_EndeCedente')

							);
				
				$camposVazio = '';
				foreach ($data as $campo => $valor) {
					if($valor == ''){
						$camposVazio .= "|".$campo; 
					}
				}
				
				$naoObrigatorios = array(
					'BOLE_InstrucaoBoleta'       => $this->input->post('BOLE_InstrucaoBoleta'),
					
					'BOLE_Desconto'              => $this->input->post('BOLE_Desconto')?:'0',
					'BOLE_Deducao'               => $this->input->post('BOLE_Deducao')?:'0',
					'BOLE_Multa'                 => $this->input->post('BOLE_Multa')?:'0',
					
					'BOLE_Juros'                 => $this->input->post('BOLE_Juros')?:'0', // Bruno Magnata 20/03/17 Mantis[5194]
					
					'BOLE_Acrescimo'             => $this->input->post('BOLE_Acrescimo')?:'0',
					'BOLE_Bairro'                => $this->input->post('BOLE_Bairro')?:'NI',
					'BOLE_Titulo'                => $this->input->post('BOLE_Titulo')?:'0',
					'BOLE_DataProc'              => $this->input->post('BOLE_DataProc')?:date('Y-m-d'),
					'BOLE_Valor'                 => $this->input->post('BOLE_Valor')?:'0',
					'BOLE_ValCobrado'            => $this->input->post('BOLE_ValCobrado')?:'0',
					'BOLE_EndeCliente'           => $this->input->post('BOLE_EndeCliente')?:'NI',	
					'BOLE_Cidade'                => $this->input->post('BOLE_Cidade')?:'NI',
					'BOLE_UF'                    => $this->input->post('BOLE_UF')?:'0',
					'BOLE_AgenciaCedente'        => $this->input->post('BOLE_AgenciaCedente')?:'0',

					// GRCSU
					'BOLE_Leiaute'              => $this->input->post('BOLE_Leiaute')?:'1', // 1 - Padrão / 2 - Empresa-Prédio / 3 - Guia de Arrecadação de Investimento / 4 - Arrecadação-Recebimento D8
					'BOLE_EndeCedente'          => $this->input->post('BOLE_EndeCedente')?:'NI',
					'BOLE_BairroCedente'        => $this->input->post('BOLE_BairroCedente')?:'NI',
					'BOLE_CidadeCedente'        => $this->input->post('BOLE_CidadeCedente')?:'NI',
					'BOLE_UFCedente'            => $this->input->post('BOLE_UFCedente')?:'NI',
					'BOLE_CEPCedente'           => $this->input->post('BOLE_CEPCedente')?:'NI',					
					'BOLE_TipoContribuicao'     => $this->input->post('BOLE_TipoContribuicao')?:'NI', 
					'BOLE_CNAE'                 => $this->input->post('BOLE_CNAE')?:'NI',

					'Bole_EntidadeSindical'     => $this->input->post('Bole_EntidadeSindical')?:'NI',

					'BOLE_grupomassa'           => $this->input->post('BOLE_grupomassa')?:'NI' // Mantis 5432 

					);	


				$data = array_merge($naoObrigatorios, $data);
				
				if($camposVazio === ''){
					
					$cancelamento = $this->Boletos_model->alterarBoletos($codigoApi,$data);

					if($cancelamento){
						$retorno['status']    = true;						
						$retorno['codstatus'] = 200;						
						$retorno['msg'] = 'Acao realizada com sucesso!';				
					}else{
						$retorno['status']    = false;	
						$retorno['codstatus'] = 106;		
						$retorno['msg'] = 'Erro durante alteracao do boleto!';
					}
				}else{
					$retorno['status']    = false;	
					$retorno['codstatus'] = 104;		
					$retorno['msg'] = 'Os seguinte campos são requeridos:'.$camposVazio;
				}

			}else{
				$retorno['status']    = false;	
				$retorno['codstatus'] = 102;		
				$retorno['msg'] = 'Token de segurança inválido!';
			}

		}else{
			$retorno['status']    = false;	
			$retorno['codstatus'] = 101;		
			$retorno['msg'] = 'As requisicoes precisam ser do tipo POST!';
		}

		echo json_encode($retorno);
	}
	/**
	 * [cancelarBoletos description]Função responsável pelo cancelamento do boleto, este não será possível
	 * ser cancelado se estiver com os status 0,2,4,5
	 * @return [type] [description]
	 */
	public function cancelarBoletos()
	{
		$codigoCliente = $this->uri->segment(3);
		$token         = $this->uri->segment(4);
		$codigoApi     = base64_decode(urldecode($this->uri->segment(5)));

		$validado = $this->verificarCodigoValidacao($token);

		if($validado){

				$data = array(
							'BISAUSUA_LastUpdate'  => date('Y-d-m H:i:s'),
							'STATUS_boleto' => 2
						);

				$cancelamento = $this->Boletos_model->cancelarBoletos($codigoApi,$data);

				if($cancelamento){

					$this->arquivoRemessaCaixa($acao = 'cancelar',$codigoCliente,$codigoApi);

					$retorno['status']    = true;	
					$retorno['codstatus'] = 200;		
					$retorno['msg'] = 'Acao realizada com sucesso!';
					echo json_encode($retorno);	

				}else{

					$retorno['status']    = false;	
					$retorno['codstatus'] = 105;		
					$retorno['msg'] = 'Boleto nao encotrado!';
					echo json_encode($retorno);

				}

		}else{

			$retorno['status']    = false;	
			$retorno['codstatus'] = 102;		
			$retorno['msg'] = 'Token de seguranca invalido!';
			echo json_encode($retorno);
		}
	}	
	/**
	 * [verificarCodigoValidacao description] Função responsável em gerar um token de segurança e realizar a
	 * validação do token enviado pelos sistema que usam a api, nela consiste em pegar o dia, mês e ano atual
	 * e aconcatenar cada caracter com a letra "A" e depois codificala para sha1 pegando parte da string gerada
	 * montando assim o token de validação, é realizada a comparação pelo token enviado e retonando um booleano.
	 * 
	 * @param  [type] $codigoCliente [description]
	 * @return [type]                [description]
	 */
	public function verificarCodigoValidacao($codigoCliente)
	{
		$dataAtual = str_split(date('dmy'));

		$codigo = '';

		foreach ($dataAtual as $d) {
			$codigo .= "A".$d;
		}
		
		$codigo = substr(sha1($codigo),5,10);
		
		$retorno = ($codigo == $codigoCliente)? true:false;

		return $retorno;
	}

	public function leituraRetorno()
	{
		$retorno = array();
		$codigoCliente = $this->uri->segment(3);
		$token         = $this->uri->segment(4);
		$codigoBanco   = $this->uri->segment(5);		

		$validado = $this->verificarCodigoValidacao($token);

		if($validado){
			
			switch ($codigoBanco) {
				case 104:
				//	$retorno = $this->arquivoRetornoCaixa($codigoCliente);
                    $this->arquivoRetornoCaixa($codigoCliente);
					break;
				case 001:
				//  $retorno = $this->arquivoRetornoBBrasil($codigoCliente);
                    $this->arquivoRetornoBBrasil($codigoCliente);
					break;
				
				default:
					$retorno['status']    = false;	
					$retorno['codstatus'] = 107;		
					$retorno['msg'] = 'Banco nao suportado para leitura!';
					echo json_encode($retorno);
					break;
			}
			
	
		}else{
			$retorno['status']    = false;	
			$retorno['codstatus'] = 102;		
			$retorno['msg'] = 'Token de seguranca invalido!';			
			echo json_encode($retorno);	
		}	

	//	echo json_encode($retorno);

	}

	public function gerarBoletosMassa()
	{
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');

		
		$codigoCliente = $this->uri->segment(3);
		$token         = $this->uri->segment(4);
		$dia           = date('Y-m-'.$this->uri->segment(5));
		$grupomassa	   = ($this->uri->segment(6))?$this->uri->segment(6):'NI';  // Mantis 5432
		
		$dados = array(
						'CLIE_codigo'         => $codigoCliente,
						'STATUS_boleto'       => 1,
						'BOLE_DataProc'       => $dia,
						'BOLE_grupomassa'     => $grupomassa
						
						);
	
		$dadosBoleto = $this->Boletos_model->gerarBoletosMassa($dados);

		
		if($dadosBoleto){
		foreach ($dadosBoleto as $dados) {
			switch ($dados->BOLE_Leiaute) {
				case '2':
					
						boleto_cef_sigcb($dados);							
					
					break;
				case '3':
					
						boleto_cef_grcsu($dados);							
					
					break;
				case '4':
					
						boleto_cef_proposta($dados);							
					
					break;	
				
				default:

					if($dados->BANC_CodigoBanco == '104'){
					boleto_cef_sigcb($dados);
					}else{
					boletos_bb($dados);
					}							

					break;
				}
			}
		} else {
					
			$retorno['status']    = false;	
			$retorno['codstatus'] = 105;		
			$retorno['msg'] = 'Boleto nao encotrado!';
			echo json_encode($retorno);
			
		}

			

	}

    /**
     * [preGerarBoletosMassa description] Função responsável por permitir a pré-visualização em massa
     * dos boletos do cliente.
     * @return [type][description]
     * Jorge Roberto - Mantis 5472
     */
    public function preGerarBoletosMassa()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','2048M');


        $codigoCliente = $this->uri->segment(3);
        $token         = $this->uri->segment(4);
        $dia           = date('Y-m-'.$this->uri->segment(5));
        $grupomassa	   = ($this->uri->segment(6))?$this->uri->segment(6):'NI';  // Mantis 5432

        $dados = array(
            'CLIE_codigo'         => $codigoCliente,
            'BOLE_DataProc'       => $dia,
            'BOLE_grupomassa'     => $grupomassa

        );

        $dadosBoleto = $this->Boletos_model->preGerarBoletosMassa($dados);
        if($dadosBoleto){
            foreach ($dadosBoleto as $dados) {
                switch ($dados->BOLE_Leiaute) {
                    case '2':

                        boleto_cef_sigcb($dados);

                        break;
                    case '3':

                        boleto_cef_grcsu($dados);

                        break;
                    case '4':
					
						boleto_cef_d8($dados);							
					
					break;    

                    default:

                        boleto_cef_sigcb($dados);
                        break;
                }
            }
        } else {

            $retorno['status']    = false;
            $retorno['codstatus'] = 105;
            $retorno['msg'] = 'Boleto nao encotrado!';
            echo json_encode($retorno);

        }



    }

    public function arquivoRetornoCaixa($codigoCliente)
    {        
        $retorno = array();
        $caminho = base_url().'assets/arquivos/retorno/'.$codigoCliente.'.ret';
        $agencia = true;        

        $ponteiro = fopen ($caminho,"r");    

        $cont = 0;    

        while (!feof ($ponteiro)) {
            $linha = fgets($ponteiro,1000);

            if($agencia){
                $codigoAgencia = substr($linha, 52,5);
                $convenio = substr($linha, 58,6);
                $agencia = false;
            }

            $segmento = substr($linha, 13,1);

            if($segmento === 'T'){
                $where['BOLE_Sequencial']      = (INTEGER)substr($linha, 41,15);    
                $where['CLIE_codigo']      = $codigoCliente;
                $where['BANC_CodigoBanco']      = substr($linha, 0,3);
                $where['BOLE_Agencia']     = (INTEGER)$codigoAgencia;
                $where['BOLE_NumeroConvenio'] = $convenio;                
            }
    
            if($segmento === 'U'){
                $dados['BAIX_Valor']         = substr($linha, 77,15);                
                //$dados['BAIX_DataCreditado'] = substr($linha, 137,8);
                $dados['BAIX_DataCreditado'] =    date('Y-m-d',strtotime(vsprintf("%s%s-%s%s-%s%s%s%s", str_split(substr($linha, 137,8)))));
                $dados['STATUS_boleto']      = 4;


                $segmentoPago = substr($linha, 16,1);

                if ($segmentoPago == '6') {
                  
                    $resultado = $this->Boletos_model->baixaBoleto($where,$dados);

                } else {
                	$resultado = false ;
                }              
                
                // var_dump($resultado);die();
                
                if($resultado){
                    
                    $codigoApi = $this->Boletos_model->pegarCodigoAPi($where);                
                    
                     
                    
                    $retorno[$cont]['valorPago']         = $dados['BAIX_Valor'];
                    $retorno[$cont]['valorTitulo']       = $codigoApi[0]->BOLE_ValorDocumento;                    
                    $retorno[$cont]['titulo']            = $codigoApi[0]->BOLE_Sequencial;
                    $retorno[$cont]['dataPagamento']     = date('Y-m-d',strtotime($dados['BAIX_DataCreditado']));                    
                    $retorno[$cont]['dataVencimento']    = date('Y-m-d',strtotime($codigoApi[0]->BOLE_Vencimento));
                    $retorno[$cont]['codigoBanco']       = $where['BANC_CodigoBanco'];
                    $retorno[$cont]['agencia']           = $where['BOLE_Agencia'];
                    $retorno[$cont]['status']            = true;
                    $retorno[$cont]['codigoApi']         = urlencode(base64_encode($codigoApi[0]->API_codigo)); 		
					
				}else{
					$retorno[$cont]['valorPago']         = $dados['BAIX_Valor'];
					$retorno[$cont]['valorTitulo']       = '';
					$retorno[$cont]['titulo']            = $where['BOLE_Sequencial'];
					$retorno[$cont]['dataPagamento']     = date('Y-m-d',strtotime($dados['BAIX_DataCreditado']));
					$retorno[$cont]['dataVencimento']    = '';
					$retorno[$cont]['codigoBanco']       = $where['BANC_CodigoBanco'];
					$retorno[$cont]['agencia']           = $where['BOLE_Agencia'];
					$retorno[$cont]['status']            = false;
					$retorno[$cont]['codigoApi']         = '';
				}
				$cont++;
			}
		}

		fclose ($ponteiro);

         echo json_encode($retorno);
		//return $retorno;
	}

	public function arquivoRetornoBBrasil($codigoCliente)
	{
		$retorno = array();
	    $caminho = base_url().'assets/arquivos/retorno/'.$codigoCliente.'.ret';
	    $agencia = true;

		$ponteiro = fopen ($caminho,"r");	

		$cont = 0;	

		while (!feof ($ponteiro)) {
			$linha = fgets($ponteiro,1000);

			if($agencia){
				$codigoAgencia = substr($linha, 52,5);
				$contaCCAgencia = substr($linha, 58,12);
				$convenio = substr($linha, 35,6);
				$agencia = false;
			}

			$segmento = substr($linha, 13,1);

			if($segmento === 'T'){			

				$where['BOLE_Sequencial']  =  (string)(integer)substr($linha, 41,12);	
				$where['CLIE_codigo']       =  $codigoCliente;
				$where['BANC_CodigoBanco']  =  substr($linha, 0,3);
				$where['BOLE_Agencia']      =  $codigoAgencia;
				$where['BOLE_NumeroConvenio'] = $convenio;					 				
			}
			 
			if($segmento === 'U'){
				$dados['BAIX_Valor']         = substr($linha, 77,15);
				$dados['BAIX_DataCreditado'] = substr($linha, 137,8);
				$dados['STATUS_boleto']      = 4;
			
			   //v($where);
				$resultado = $this->Boletos_model->baixaBoleto($where,$dados);
				
				if($resultado){
					
					$codigoApi = $this->Boletos_model->pegarCodigoAPi($where);							

					$retorno[$cont]['valorPago']         = $dados['BAIX_Valor'];
					$retorno[$cont]['valorTitulo']       = $codigoApi[0]->BOLE_ValorDocumento;
					$retorno[$cont]['titulo']            = $codigoApi[0]->BOLE_Sequencial;
					$retorno[$cont]['dataPagamento']     = date('Y-m-d',strtotime($dados['BAIX_DataCreditado']));
					$retorno[$cont]['dataVencimento']    = date('Y-m-d',strtotime($codigoApi[0]->BOLE_Vencimento));
					$retorno[$cont]['codigoBanco']       = $where['BANC_CodigoBanco'];
					$retorno[$cont]['agencia']           = $where['BOLE_Agencia'];
					$retorno[$cont]['status']            = true;
					$retorno[$cont]['codigoApi']         = urlencode(base64_encode($codigoApi[0]->API_codigo));
				}else{
					$retorno[$cont]['valorPago']         = $dados['BAIX_Valor'];
					$retorno[$cont]['valorTitulo']       = '';
					$retorno[$cont]['titulo']            = '';
					$retorno[$cont]['dataPagamento']     = date('Y-m-d',strtotime($dados['BAIX_DataCreditado']));
					$retorno[$cont]['dataVencimento']    = '';
					$retorno[$cont]['codigoBanco']       = $where['BANC_CodigoBanco'];
					$retorno[$cont]['agencia']           = $where['BOLE_Agencia'];
					$retorno[$cont]['status']            = false;
					$retorno[$cont]['codigoApi']         = '';
				}
				$cont++;
			}
		}

		fclose ($ponteiro);

		//return $retorno;
		echo json_encode($retorno);
	}

	public function arquivoRemessaCaixa($acao = '',$codigoCliente = '',$codigoApi = '')
	{		
		$codigoBanco = 104;
		
		switch ($acao) {
			case 'cancelar':

				$where = array(
						'CLIE_codigo'         => $codigoCliente,
						'API_codigo'          => $codigoApi,
						'BANC_CodigoBanco'    => $codigoBanco,
						'STATUS_boleto'       => 2
					);
				$acaoRemessa = '02';
				$status     = 2;
				break;

			
			default:
				$codigoCliente = $this->uri->segment(3);
				$token         = $this->uri->segment(4);
				$convenio      = $this->uri->segment(5);
				

				$where = array(
						'CLIE_codigo'         => $codigoCliente,
						'BANC_CodigoBanco'    => $codigoBanco,
						'BOLE_NumeroConvenio' => $convenio,
						'STATUS_boleto'       => 0
					);

				$acaoRemessa = '01';
				$status      = 1;

				break;
		}
		

		$resultados = $this->Boletos_model->dadosArquivosRemessaCaixa($where);
		
		if($resultados){

			$registrosLote  = 0;
			$valorTotalLote = 0;

			$dados['header']       = $this->headerArquivoRemessaCaixa($resultados[0]);

			$dados['headerLote']   = $this->headerLoteArquivoRemessaCaixa($resultados[0]);	

			$conteudo = $dados['header'].$dados['headerLote'];		

			foreach ($resultados as $resultado) {

				
				
				$dados['dadosRemessaP'] = $this->dadosTituloCaixaRemessaTipoP($registrosLote,$resultado,$acaoRemessa);

				$dados['dadosRemessaQ'] = $this->dadosTituloCaixaRemessaTipoQ($registrosLote,$resultado);
			
      			$dados['dadosRemessaR'] = $this->dadosTituloCaixaRemessaTipoR($registrosLote,$resultado);

      			// Bruno Magnata Mantis: 5533
      			if ($resultado->BOLE_Leiaute  == '4') {
      				$dados['dadosRemessaY'] = $this->dadosTituloCaixaRemessaTipoY($registrosLote,$resultado);
      			}      			
				
				$valorTotalLote += str_replace('.', '', $resultado->BOLE_ValorDocumento);

				$registrosLote++;
							
				// Bruno Magnata Mantis: 5533
				if ($resultado->BOLE_Leiaute == '4') {				
      				$conteudo.= $dados['dadosRemessaP'].$dados['dadosRemessaQ'].$dados['dadosRemessaR'].$dados['dadosRemessaY'];
      			}else{
      				$conteudo.= $dados['dadosRemessaP'].$dados['dadosRemessaQ'].$dados['dadosRemessaR'];
      			}
			}

			$dados['trailerLote']     = $this->trailerLoteArquivoRemessaCaixa($resultados[0],$registrosLote,$valorTotalLote);
			$dados['trailerHeader']   = $this->trailerHeaderArquivoRemessaCaixa($resultados[0],$registrosLote);
			$sequencia = $this->Boletos_model->pegarSequenciaCliente($resultados[0]->CLIE_codigo);

			$sequencia = str_pad(($sequencia*1),5,0,STR_PAD_LEFT);

			$conteudo .= $dados['trailerLote'].$dados['trailerHeader'];

			$caminho     = 'assets/arquivos/remessa/'.$codigoCliente.'.txt';		

			$caminhoCopy = 'assets/arquivos/remessa/'.$codigoCliente.date('d_m_Y_H_i').'.txt';			

			$file = fopen($caminho, 'w+');
			fwrite($file, $conteudo);
			fclose($file);

			copy($caminho, $caminhoCopy);

			$resultados = $this->Boletos_model->atualizarStatusArquivosRemessaCaixa($where,$status);

			$dados = file_get_contents($caminhoCopy); // Lê o conteúdo do arquivo
			$nome = 'e'.date('d').$sequencia.'.rem';

			force_download($nome, $dados);
			
		}else{

			$retorno['status']    = false;	
			$retorno['codstatus'] = 105;		
			$retorno['msg'] = 'Boletos nao encotrado!';
			echo json_encode($retorno);
		}
	}

	public function headerArquivoRemessaCaixa($resultado)
	{
			$nsa = $this->Boletos_model->pegarSequenciaClienteUpdate($resultado->CLIE_codigo);

			$tipoInscricaoBeneficiario = (strlen($resultado->BOLE_CNPJCPFCedente) == 14)?'2':'1';
			//$nsa                     = 2; //falta fazer
			$ambiente                  = 'REMESSA-PRODUCAO';//'REMESSA-PRODUCAO' OU 'REMESSA-TESTE'
			$nomeEmpresa               = strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,30));

			$dados['codigoBanco']              	  = '104';//Preencher 104 código da CEF
			$dados['codigoLote']               	  = '0000';//Preencher com 0000
			$dados['tipoRegistro']             	  = '0';//Preencher '0' (equivale a Header de Arquivo)
			$dados['fillerItem04']             	  = str_repeat(chr(32), 9);//Preencher com espaços
			$dados['tipoInscricaoBeneficiario']	  = $tipoInscricaoBeneficiario;//Preencher com o tipo de inscrição do Beneficiário: '1', se CPF (pessoa física); ou '2' se CNPJ (pessoa jurídica)
			$dados['numeroInscricaoBeneficiario'] = $resultado->BOLE_CNPJCPFCedente;//Número de Inscrição da Empresa ou Pessoa Física Número de inscrição da Empresa (CNPJ) ou Pessoa Física (CPF).
			$dados['usoCaixaItem07'] 			  = str_repeat(0, 20);//Preencher com zeros
			$dados['agenciaMantenedora']          = str_pad((integer)$resultado->BOLE_Agencia,5,0,STR_PAD_LEFT);//Preencher com o código da agência detentora da conta, com um zero à esquerda
			$dados['digitoVerificadorAgencia']    = str_pad(substr($resultado->BOLE_DigAgencia,-1),1,0,STR_PAD_LEFT);//Preencher com o dígito verificador da agência, informado pela CAIXA
			$dados['codigoBeneficiario']          = str_pad(substr($resultado->BOLE_NumeroConvenio,0,6),6,0,STR_PAD_LEFT);//Código fornecido pela CAIXA, através da agência de relacionamento do cliente; trata-se do código do Beneficiário (6 posições)
			$dados['usoCaixaItem11']              = str_repeat(0, 7);//Preencher com zeros
			$dados['usoCaixaItem12']              = str_repeat(0, 1);//Preencher com zeros
			$dados['nomeEmpresa']                 = str_pad($nomeEmpresa,30,chr(32),STR_PAD_RIGHT);//Preencher com o Nome da empresa
			$dados['nomeBanco']                   = str_pad(strtoupper('CAIXA ECONOMICA FEDERAL'),30,chr(32),STR_PAD_RIGHT);//Preencher 'CAIXA ECONOMICA FEDERAL'
			$dados['FillerItem15']                = str_repeat(chr(32), 10);//Preencher com espaços
			$dados['codigoRemessaRetorno']        = 1;//Preencher '1'
			$dados['dataGeracaoArquivo']          = date('dmY');//Preencher com a data da criação do arquivo, no formato DDMMAAAA (Dia, Mês e Ano)
			$dados['horaGeracaoArquivo']          = str_pad(date('His'),6,0,STR_PAD_LEFT);//Preencher com a hora de criação do arquivo, no formato HHMMSS (Hora, Minuto e Segundos)
			$dados['nsa']                         = str_pad($nsa,6,0,STR_PAD_LEFT);//Número seqüencial adotado e controlado pelo responsável pela geração do arquivo para ordenar a disposição dos arquivos encaminhados; evoluir de 1 em 1 para cada Header de Arquivo
			$dados['versaoLaiauteArquivo']        = '050';//Preencher com '050'
			$dados['densidadeGravacaoArquivo']    = str_repeat(0, 5);//Preencher com zeros
			$dados['FillerItem22']                = str_repeat(chr(32), 20);//Preencher com espaços
			$dados['situacaoArquivo']             = str_pad(strtoupper($ambiente),20,chr(32),STR_PAD_RIGHT);//Preencher com ‘REMESSATESTE' na fase de testes (simulado); Preencher com ‘REMESSAPRODUCAO’, se estiver em produção
			$dados['versaoAplicativo']            = str_repeat(chr(32), 4);//Preencher com espaços
			$dados['fillerItem25']                = str_repeat(chr(32), 25);//Preencher com espaços


			$header = $dados['codigoBanco'].
					  $dados['codigoLote'].
					  $dados['tipoRegistro'].
					  $dados['fillerItem04'].
					  $dados['tipoInscricaoBeneficiario'].
					  $dados['numeroInscricaoBeneficiario'].
					  $dados['usoCaixaItem07'].
					  $dados['agenciaMantenedora'].
					  $dados['digitoVerificadorAgencia'].
					  $dados['codigoBeneficiario'].
					  $dados['usoCaixaItem11'].
					  $dados['usoCaixaItem12'].
					  $dados['nomeEmpresa'].
					  $dados['nomeBanco'].
					  $dados['FillerItem15'].
					  $dados['codigoRemessaRetorno'].
					  $dados['dataGeracaoArquivo'].
					  $dados['horaGeracaoArquivo'].
					  $dados['nsa'].
					  $dados['versaoLaiauteArquivo'].
					  $dados['densidadeGravacaoArquivo'].
					  $dados['FillerItem22'].
					  $dados['situacaoArquivo'].
					  $dados['versaoAplicativo'].
					  $dados['fillerItem25'].PHP_EOL;

		return $header;
	}

	public function headerLoteArquivoRemessaCaixa($resultado)
	{	
		$tipoInscricaoBeneficiario = (strlen($resultado->BOLE_CNPJCPFCedente) == 14)?'2':'1';
		$nomeEmpresa               = strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,30));
		$loteServico               = 1;
		$tipoServico               = '01';
		$nsa = $this->Boletos_model->pegarSequenciaCliente($resultado->CLIE_codigo);		

		$dados['codigoBanco']  	   			  = '104';//Preencher '104’
		$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
		$dados['tipoRegistro'] 	   			  = '1';//Preencher '1’ (equivale a Header de Lote)
		$dados['tipoOperacao'] 	   			  = 'R';//Preencher ‘R’ (equivale a Arquivo Remessa)
		$dados['tipoServico']  	   			  = $tipoServico;//Preencher com ‘01', se Cobrança Registrada; ou ‘02’, se Cobrança Sem Registro/Serviços
		$dados['fillerItem06'] 	   			  = str_repeat(0, 2);//Preencher com zeros
		$dados['numeroVersaoLote'] 			  = '030';//Preencher '030'
		$dados['fillerItem08']     			  = str_repeat(chr(32), 1);//Preencher com espaços
		$dados['tipoInscricaoBeneficiario']	  = $tipoInscricaoBeneficiario;//Preencher com o tipo de inscrição do Beneficiário: '1', se CPF (pessoa física); ou '2' se CNPJ (pessoa jurídica)
		$dados['numeroInscricaoBeneficiario'] = str_pad($resultado->BOLE_CNPJCPFCedente,15,0,STR_PAD_LEFT);//Número de Inscrição da Empresa ou Pessoa Física Número de inscrição da Empresa (CNPJ) ou Pessoa Física (CPF).
		$dados['codigoBeneficiario']          = str_pad(substr($resultado->BOLE_NumeroConvenio,0,6),6,0,STR_PAD_LEFT);//Código fornecido pela CAIXA, através da agência de relacionamento do cliente; trata-se do código do Beneficiário (6 posições)
		$dados['usoCaixaItem11'] 			  = str_repeat(0, 14);//Preencher com zeros
		$dados['agenciaMantenedora']          = '0'.str_pad((integer)$resultado->BOLE_Agencia,4,0,STR_PAD_LEFT);//Preencher com o código da agência detentora da conta, com um zero à esquerda
		$dados['digitoVerificadorAgencia']    = str_pad(substr($resultado->BOLE_DigAgencia,-1),1,0,STR_PAD_LEFT);//Preencher com o dígito verificador da agência, informado pela CAIXA
		$dados['codigoConvenioBanco']         = str_pad(substr($resultado->BOLE_NumeroConvenio,0,6),6,0,STR_PAD_LEFT);//Código fornecido pela CAIXA, através da agência de relacionamento do cliente; trata-se do código do Beneficiário (6 posições)
		$dados['codigoModeloPersonalizado']   = str_repeat(0, 7);//Código fornecido pela CAIXA/Gráfica, utilizado somente quando o modelo do boleto for personalizado; do contrário, preencher com zeros
		$dados['usoCaixaItem16'] 			  = str_repeat(0, 1);//Preencher com zeros
		$dados['nomeEmpresa']                 = str_pad($nomeEmpresa,30,chr(32),STR_PAD_RIGHT);//Preencher com o Nome da empresa
		$dados['mensagem1']                   = str_repeat(chr(32), 40);
		$dados['mensagem2']                   = str_repeat(chr(32), 40);
		$dados['numeroRemessa']               = str_pad($nsa,8,0,STR_PAD_LEFT);//Número seqüencial adotado e controlado pelo responsável pela geração do arquivo para ordenar a disposição dos arquivos encaminhados; evoluir de 1 em 1 para cada Header de Arquivo
		$dados['dataGravacaoRemessa']         = date('dmY');
		$dados['fillerItem22']                = str_repeat(0, 8);//Preencher com zeros
		$dados['fillerItem23']                = str_repeat(chr(32), 33);




		$headerLote =  $dados['codigoBanco'].
					   $dados['loteServico'].
					   $dados['tipoRegistro'].
					   $dados['tipoOperacao'].
					   $dados['tipoServico'].
					   $dados['fillerItem06'].
					   $dados['numeroVersaoLote'].
					   $dados['fillerItem08'].
					   $dados['tipoInscricaoBeneficiario'].
					   $dados['numeroInscricaoBeneficiario'].
					   $dados['codigoBeneficiario'].
					   $dados['usoCaixaItem11'].
					   $dados['agenciaMantenedora'].
					   $dados['digitoVerificadorAgencia'].
					   $dados['codigoConvenioBanco'].
					   $dados['codigoModeloPersonalizado'].
					   $dados['usoCaixaItem16'].
					   $dados['nomeEmpresa'].
					   $dados['mensagem1'].
					   $dados['mensagem2'].
					   $dados['numeroRemessa'].
					   $dados['dataGravacaoRemessa'].
					   $dados['fillerItem22'].
					   $dados['fillerItem23'].PHP_EOL;

		return $headerLote;
	}

	public function dadosTituloCaixaRemessaTipoP($registrosLote,$resultado,$acaoRemessa)
	{
		$loteServico               = 1;
		$sequenciaRegistro         = $registrosLote+1;

		
	

			$dados['codigoBanco']  	   			  = '104';//Preencher '104’
			$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
			$dados['tipoRegistro'] 	   			  = '3';//Preencher '3’ (equivale a Header de Lote)
			$dados['sequenciaRegistro']           = str_pad($sequenciaRegistro,5,0,STR_PAD_LEFT);//Ver Nota Explicativa G038; evoluir de 1 em 1 para cada Segmento do Lote
			$dados['segmentoRegistroDetalhe']     = 'P';//Preencher 'P’
			$dados['fillerItem06']                = str_repeat(chr(32), 1);//Preencher com espaços
	/*ver*/	$dados['codigoMovimentoRemessa']      = /*'07';*/$acaoRemessa;//Ver Nota Explicativa C004
			$dados['agenciaMantenedora']          = '0'.str_pad((integer)$resultado->BOLE_Agencia,4,0,STR_PAD_LEFT);//Preencher com o código da agência detentora da conta, com um zero à esquerda
			//$dados['digitoVerificadorAgencia']    = $resultado->BOLE_DigAgencia;//Preencher com o dígito verificador da agência, informado pela CAIXA
			$dados['digitoVerificadorAgencia']    = str_pad(substr($resultado->BOLE_DigAgencia,-1),1,0,STR_PAD_LEFT);//Preencher com o dígito verificador da agência, informado pela CAIXA
			$dados['codigoConvenioBanco']         = str_pad(substr($resultado->BOLE_NumeroConvenio,0,6),6,0,STR_PAD_LEFT);//Código fornecido pela CAIXA, através da agência de relacionamento do cliente; trata-se do código do Beneficiário (6 posições)
			$dados['usoCaixaItem11']              = str_repeat(0, 8);//Preencher com zeros
			$dados['fillerItem12']                = str_repeat(0, 3);//Preencher com zeros
			$dados['modalidadeNossoNumero']       = '14';
			$dados['identificacaoTituloBanco']    = str_pad($resultado->BOLE_Sequencial,15,0,STR_PAD_LEFT);			
			$dados['codigoCarteira']              = '1';//Preencher '1’, equivalente a Cobrança Simples
			$dados['formaCadastramentoBanco']     = '1';//‘1’ Cobrança Registrada
			$dados['tipoDocumento']               = '2';//Preencher '2’ (equivale a Escritural)
			$dados['identificacaoEmissaoBoleto']  = '2';//Ver Nota Explicativa C009
			$dados['identificacaoEntregaBoleto']  = '0';//Ver Nota Explicativa C010
			$dados['numeroDocumentoCobranca']     = str_pad($resultado->BOLE_Sequencial,11,chr(32),STR_PAD_RIGHT);
			$dados['fillerItem19']                = str_repeat(chr(32), 4);//Preencher com espaços
			$dados['dataVencimento']              = date('dmY',strtotime($resultado->BOLE_Vencimento));
			$dados['valorTitulo']                 = str_pad(str_replace('.', '',$resultado->BOLE_ValorDocumento),15,0,STR_PAD_LEFT);
			$dados['agenciaCobranca']             = str_repeat(0, 5);//Preencher com zeros
			$dados['digitoAgencia']               = str_repeat(0, 1);//Preencher com zeros
			$dados['especieTitulo']               = '24';//Ver Nota Explicativa C015
			$dados['aceite']                      = 'A';//Indica se o título de cobrança possui aceite do pagador; preencher com ‘A’ (Aceite) ou ‘N’ (Não Aceite)
			$dados['dataEmissaoTitulo']           = date('dmY',strtotime($resultado->BOLE_DataProc));
			$dados['codigoJurosMora']             = '2';//‘1’ (Valor por Dia); ou ‘2’ (Taxa Mensal); ou ‘3’ (Isento)
			$dados['dataJurosMora']               = date('dmY',strtotime('+1 days',strtotime($resultado->BOLE_Vencimento)));//Preencher com a data indicativa do início da cobrança de Juros de Mora, no formato DDMMAAAA (Dia, Mês e Ano), devendo ser maior que a Data de Vencimento; ATENÇÃO, caso a informação seja inválida ou não informada, o sistema assumirá data igual à Data de Vencimento + 1
			// Bruno Magnata 20/03/17 Mantis[5194]
			$dados['jurosMoraDiaTaxa']            = str_pad(str_replace(',', '',str_replace('.', '',$resultado->BOLE_Juros)),15,0,STR_PAD_LEFT); // '000000000000100'; Repeat(0, 15);
    	
        	// $dados['codigoDesconto']           = str_repeat(0, 1);
		    // $dados['dataDesconto']             = str_repeat(0, 8);
		    // $dados['valorDesconto']            = str_repeat(0, 15);

			if ($resultado->BOLE_Tipo_Desconto == NULL) {
            	$dados['codigoDesconto']              = str_repeat(1,1);
            }else{
                $dados['codigoDesconto']              = $resultado->BOLE_Tipo_Desconto;
            }
			$dados['dataDesconto']                = date('dmY',strtotime($resultado->BOLE_Vencimento));
			$dados['valorDesconto']               = str_pad(str_replace('.', '',$resultado->BOLE_Desconto),15,0,STR_PAD_LEFT);

			$dados['valorIOF']                    = str_repeat(0, 15);
			$dados['valorAbatimento']             = str_repeat(0, 15);
			$dados['identificacaoTituloEmpresea'] = str_pad($resultado->BOLE_Sequencial,25,chr(32),STR_PAD_RIGHT);
			$dados['codigoProtesto']              = '3';
			$dados['numeroDiasProtesto']          = '00';
			$dados['codigoBaixaDevolucao']        = '1';
			$dados['numerodiasBaixaDevolucao']    = '120';
			$dados['codigoMoeda']                 = '09';
			$dados['fillerItem41']                = str_repeat(0, 10);
			
			$dados['fillerItem42']                = str_repeat(chr(32), 1);	
			
			

			$conteudo = $dados['codigoBanco']				 .
						$dados['loteServico']  	   			 .
						$dados['tipoRegistro'] 	   			 .
						$dados['sequenciaRegistro']          .
						$dados['segmentoRegistroDetalhe']    .
						$dados['fillerItem06']               .
						$dados['codigoMovimentoRemessa']     .
						$dados['agenciaMantenedora']         .
						$dados['digitoVerificadorAgencia']   .
						$dados['codigoConvenioBanco']        .
						$dados['usoCaixaItem11']             .
						$dados['fillerItem12']               .
						$dados['modalidadeNossoNumero']      .
						$dados['identificacaoTituloBanco']   .						
						$dados['codigoCarteira']             .
						$dados['formaCadastramentoBanco']    .
						$dados['tipoDocumento']              .
						$dados['identificacaoEmissaoBoleto'] .
						$dados['identificacaoEntregaBoleto'] .
						$dados['numeroDocumentoCobranca']    .
						$dados['fillerItem19']               .
						$dados['dataVencimento']             .
						$dados['valorTitulo']                .
						$dados['agenciaCobranca']            .
						$dados['digitoAgencia']              .
						$dados['especieTitulo']              .
						$dados['aceite']                     .
						$dados['dataEmissaoTitulo']          .
						$dados['codigoJurosMora']            .
						$dados['dataJurosMora']              .
						$dados['jurosMoraDiaTaxa']           .
						$dados['codigoDesconto']             .
						$dados['dataDesconto']               .
						$dados['valorDesconto']              .
						$dados['valorIOF']                   .
						$dados['valorAbatimento']            .
						$dados['identificacaoTituloEmpresea'].
						$dados['codigoProtesto']             .
						$dados['numeroDiasProtesto']         .
						$dados['codigoBaixaDevolucao']        .
						$dados['numerodiasBaixaDevolucao']   .
						$dados['codigoMoeda']                .
						$dados['fillerItem41']               .
						$dados['fillerItem42'].PHP_EOL;


		return $conteudo;

	}

	public function dadosTituloCaixaRemessaTipoQ($registrosLote,$resultado)
	{	
		$documentoPagador          = str_replace('/', '', $resultado->BOLE_CGC);
		$documentoPagador          = str_replace('-', '', $documentoPagador);
		$documentoPagador          = str_replace('.', '', $documentoPagador);

		$documentoSacador          = str_replace('/', '', $resultado->BOLE_CNPJCPFCedente);
		$documentoSacador          = str_replace('-', '', $documentoSacador);
		$documentoSacador          = str_replace('.', '', $documentoSacador);

		$tipoInscricaoSacador      = (strlen($documentoSacador) == 14)?'2':'1';

		$loteServico               = 1;
		$sequenciaRegistro         = $registrosLote+1;
		$tipoInscricaoPagador      = (strlen($documentoPagador) == 14)?'2':'1';
		$nomeEmpresa               = str_pad(strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,40)),40,chr(32),STR_PAD_RIGHT);

			$dados['codigoBanco']  	   			  = '104';//Preencher '104’
			$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
			$dados['tipoRegistro'] 	   			  = '3';//Preencher '3’ (equivale a Header de Lote)
			$dados['sequenciaRegistro']           = str_pad($sequenciaRegistro,5,0,STR_PAD_LEFT);//Ver Nota Explicativa G038; evoluir de 1 em 1 para cada Segmento do Lote
			$dados['segmentoRegistroDetalhe']     = 'Q';//Preencher 'P’
			$dados['fillerItem06']                = str_repeat(chr(32), 1);//Preencher com espaços
	/*ver*/	$dados['codigoMovimentoRemessa']      = /*'07';*/ '01'; //Ver Nota Explicativa C004
			$dados['tipoInscricaoPagador']	      = $tipoInscricaoPagador;//Preencher com o tipo de inscrição do Beneficiário: '1', se CPF (pessoa física); ou '2' se CNPJ (pessoa jurídica)
			$dados['numeroInscricaoPagador']      = str_pad(trim($documentoPagador),15,0,STR_PAD_LEFT);	
			$dados['nomePagador']                 = str_pad(removerAcentos(substr($resultado->BOLE_NomeCliente,0,40)),40,chr(32),STR_PAD_RIGHT);
			$dados['enderecoPagador']             = str_pad(removerAcentos(substr($resultado->BOLE_EndeCliente,0,40)),40,chr(32),STR_PAD_RIGHT);
			$dados['bairroPagador']               = str_pad(removerAcentos(substr($resultado->BOLE_Bairro,0,15)),15,chr(32),STR_PAD_RIGHT);
			$dados['cepPagador']                  = str_pad(removerAcentos(substr($resultado->BOLE_CEP,0,5)),5,0,STR_PAD_LEFT);
			$dados['sufixoCepPagador']            = str_pad(removerAcentos(substr($resultado->BOLE_CEP,-3)),3,0,STR_PAD_LEFT);
			$dados['cidadePagador']               = str_pad(removerAcentos(substr($resultado->BOLE_Cidade,0,15)),15,chr(32),STR_PAD_RIGHT);
			$dados['unidadeFederacaoPagador']     = str_pad(removerAcentos(substr(strtoupper($resultado->BOLE_UF),0,2)),2,chr(32),STR_PAD_RIGHT);
			$dados['tiposInscricaoSacador']       = $tipoInscricaoSacador;
			$dados['numeroInscricaoSacador']      = str_pad($resultado->BOLE_CNPJCPFCedente,15,0,STR_PAD_LEFT);
			$dados['nomeSacador']                 = $nomeEmpresa;
			$dados['bancoCorrespndente']          = str_repeat(chr(32), 3);
			$dados['nossoBancoCorrespondente']    = str_repeat(chr(32), 20);
			$dados['fillerItem22']                = str_repeat(chr(32), 8);
			

			$conteudo = $dados['codigoBanco']  	   			 .
						$dados['loteServico']  	   			 .
						$dados['tipoRegistro'] 	   			 .
						$dados['sequenciaRegistro']      .
						$dados['segmentoRegistroDetalhe'].
						$dados['fillerItem06']           .
						$dados['codigoMovimentoRemessa'] .
						$dados['tipoInscricaoPagador']	  .
						$dados['numeroInscricaoPagador'] .
						$dados['nomePagador']            .
						$dados['enderecoPagador']        .
						$dados['bairroPagador']          .
						$dados['cepPagador']             .
						$dados['sufixoCepPagador']       .
						$dados['cidadePagador']          .
						$dados['unidadeFederacaoPagador'].
						$dados['tiposInscricaoSacador']  .
						$dados['numeroInscricaoSacador'] .
						$dados['nomeSacador']            .
						$dados['bancoCorrespndente']     .
						$dados['nossoBancoCorrespondente'].
						$dados['fillerItem22'].PHP_EOL;

			return $conteudo;
	}	

	public function dadosTituloCaixaRemessaTipoR($registrosLote,$resultado)
	{	
		$documentoPagador          = str_replace('/', '', $resultado->BOLE_CGC);
		$documentoPagador          = str_replace('-', '', $documentoPagador);
		$documentoPagador          = str_replace('.', '', $documentoPagador);

		$documentoSacador          = str_replace('/', '', $resultado->BOLE_CNPJCPFCedente);
		$documentoSacador          = str_replace('-', '', $documentoSacador);
		$documentoSacador          = str_replace('.', '', $documentoSacador);

		$tipoInscricaoSacador      = (strlen($documentoSacador) == 14)?'2':'1';

		$loteServico               = 1;
		$sequenciaRegistro         = $registrosLote+1;
		$tipoInscricaoPagador      = (strlen($documentoPagador) == 14)?'2':'1';
		$nomeEmpresa               = str_pad(strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,40)),40,chr(32),STR_PAD_RIGHT);

			$dados['codigoBanco']  	   			  = '104';//Preencher '104’
			$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
			$dados['tipoRegistro'] 	   			  = '3';//Preencher '3’ (equivale a Header de Lote)
			$dados['sequenciaRegistro']           = str_pad($sequenciaRegistro,5,0,STR_PAD_LEFT);//Ver Nota Explicativa G038; evoluir de 1 em 1 para cada Segmento do Lote
			$dados['segmentoRegistroDetalhe']     = 'R';//Preencher 'R’
			$dados['fillerItem06']                = str_repeat(chr(32), 1); //Preencher com espaços
	/*ver*/	$dados['codigoMovimentoRemessa']      = /*'07';*/ '01'; //Ver Nota Explicativa C004 // 01 -Entrada de Título - 07 Concessão de Desconto 
			
			$dados['CodigoDesconto2']	          = '0';// ‘0’ (Sem Desconto); ou ‘1’ (Valor Fixo até a Data Desconto informada); ou ‘2’ (Percentual até a Data do Desconto informada)
			$dados['DataDesconto2']               = str_repeat(0, 8); //  Se 32.3P = ‘0’, preencher com zeros; Se = '1' ou '2', informar a data limite do desconto
			$dados['ValorPercentualConcedido']    = str_repeat(0, 15);// 08.3R = '0', preencher com zeros; Se ='1', informar Valor; Se = ‘2’, informar percentual
			$dados['CodigoDesconto3']	          = '0';// ‘0’ (Sem Desconto); ou ‘1’ (Valor Fixo até a Data Desconto informada); ou ‘2’ (Percentual até a Data do Desconto informada)
			$dados['DataDesconto3']               = str_repeat(0, 8); //  Se 32.3P = ‘0’, preencher com zeros; Se = '1' ou '2', informar a data limite do desconto
			$dados['ValorPercentualConcedido3']   = str_repeat(0, 15);// 08.3R = '0', preencher com zeros; Se ='1', informar Valor; Se = ‘2’, informar percentual
			$dados['CodigoMulta']                 = '2'; //'0' (Sem Multa); ou '1' (Valor Fixo); ou '2'(Percentual);
			$dados['DataMulta']                   = date('dmY',strtotime('+1 days',strtotime($resultado->BOLE_Vencimento))); // str_repeat(0, 8);
			// Bruno Magnata 20/03/17 Mantis[5194]
			$dados['ValorPercentualAplicado']     = str_pad(str_replace('.', '',$resultado->BOLE_Multa),15,0,STR_PAD_LEFT);
			 // '000000000000200'; //str_repeat(0, 15); // Se 14.3R = '0', preencher com zeros; Se = '1', informar Valor; Se = '2', informar percentual
			
			$dados['InformacaoPagador']     	   = str_repeat(chr(32), 10); //Preencher com espaços
			$dados['Mensagem3']     	           = str_repeat(chr(32), 40); //Preencher com espaços
			$dados['Mensagem4']                    = str_repeat(chr(32), 40);
			$dados['EmailPagadorEnvioInformacoes'] = str_repeat(chr(32), 50);
			$dados['Filler']     				   = str_repeat(chr(32), 11); //Preencher com espaços
			

		
			

			$conteudo = $dados['codigoBanco']  	   			 .
						$dados['loteServico']  	   			 .
						$dados['tipoRegistro'] 	   			 .
						$dados['sequenciaRegistro']      .
						$dados['segmentoRegistroDetalhe'].
						$dados['fillerItem06']           .
						$dados['codigoMovimentoRemessa'] .

						$dados['CodigoDesconto2']	     .
						$dados['DataDesconto2']          .
						$dados['ValorPercentualConcedido']   .
						$dados['CodigoDesconto3']            .
						$dados['DataDesconto3']              .
						$dados['ValorPercentualConcedido3']  .

						$dados['CodigoMulta']           .
						$dados['DataMulta']             .
						$dados['ValorPercentualAplicado']    .

						$dados['InformacaoPagador']     .
						$dados['Mensagem3']             .
						$dados['Mensagem4']             .
						$dados['EmailPagadorEnvioInformacoes']     .
						$dados['Filler'].PHP_EOL;

			return $conteudo;
	}	

	// Bruno Magnata Mantis: 5533
	public function dadosTituloCaixaRemessaTipoY($registrosLote,$resultado)
	{	
	
			$loteServico               = 1;
			$sequenciaRegistro         = $registrosLote+1;
	

			$dados['codigoBanco']  	   			  = '104';//Preencher '104’
			$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
			$dados['tipoRegistro'] 	   			  = '3';//Preencher '3’ (equivale a Header de Lote)
			$dados['sequenciaRegistro']           = str_pad($sequenciaRegistro,5,0,STR_PAD_LEFT);//Ver Nota Explicativa G038; evoluir de 1 em 1 para cada Segmento do Lote
			$dados['segmentoRegistroDetalhe']     = 'Y';//Preencher 'Y’
			$dados['fillerItem06']                = str_repeat(chr(32), 1); //Preencher com espaços
	/*ver*/	$dados['codigoMovimentoRemessa']      = /*'07';*/ '01'; //Ver Nota Explicativa C004 // 01 -Entrada de Título - 07 Concessão de Desconto 
			
			$dados['registroOpcional']	          = '53';// ‘01’ = Informação de dados do Sacador/Avalista ‘03’ = Informação de dados do Pagador ‘04’ = Serviços (E-mail e SMS) ‘08’ = Serviços ‘50’ = Informação de Dados para Rateio de Crédito ‘53’ = identificação de Tipo de Pagamento  

			$dados['tipoPagamento']               = '02'; //  

			$dados['qtdPagamentopossiveis']       = '00'; // Ver Nota Explicativa C094. Para campo 42.3P = 1, informar ‘00’
			
			$dados['tipoValorinformadoMax']       = '2'; // '1' % (Percentual); ou '2' Valor 
			$dados['valorMaximo']                 = str_pad('10000000',15,0,STR_PAD_LEFT); // Se campo 11.3Y = '2', valor, com 2 casas decimais 

			$dados['tipoValorinformadoMin']       = '2'; // '1' % (Percentual); ou '2' Valor
			$dados['valorMinimo']                 = str_pad('100',15,0,STR_PAD_LEFT); // Se campo 11.3Y = '2', valor, com 2 casas decimais 

			$dados['Filler']     				   = str_repeat(chr(32), 185); //Preencher com espaços
		
			

			$conteudo = $dados['codigoBanco']  	   		   .
						$dados['loteServico']  	   		   .
						$dados['tipoRegistro'] 	   		   .
						$dados['sequenciaRegistro']        .
						$dados['segmentoRegistroDetalhe']  .
						$dados['fillerItem06']             .
						$dados['codigoMovimentoRemessa']   .

						$dados['registroOpcional']	       .
						$dados['tipoPagamento']            .
						$dados['qtdPagamentopossiveis']    .
						
						$dados['tipoValorinformadoMax']    .
						$dados['valorMaximo']              .
						$dados['tipoValorinformadoMin']    .
						$dados['valorMinimo']              .					
						
						$dados['Filler'].PHP_EOL;

			return $conteudo;
	}		

	public function trailerLoteArquivoRemessaCaixa($resultado,$registros,$valorTotalLote)
	{
		//$tipoInscricaoBeneficiario = (strlen($resultado->BOLE_CNPJCPFCedente) == 14)?'2':'1';
		//$nomeEmpresa               = strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,30));
		$loteServico               = 1;
		//$tipoServico               = '01';
		//$nsa                       = 1; //falta fazer

		$dados['codigoBanco']  	   			  = '104';
		$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);
		$dados['tipoRegistro'] 	   			  = '5';
		$dados['fillerItem04'] 	   			  = str_repeat(chr(32), 9);
		//$dados['quantidadeRegistros']         = str_pad(($registros*2)+2,6,0,STR_PAD_LEFT);//$registro é a quantidade de interações *2 são dois registro por intereração e +2 é o header e o trailer
		$dados['quantidadeRegistros']         = str_pad(($registros*3)+2,6,0,STR_PAD_LEFT); // Bruno Magnata 20/02/17 Mantis: 05173
		$dados['quantidadeTituloLote']        = str_pad($registros,6,0,STR_PAD_LEFT);
		$dados['valorTotalLote']              = str_pad(str_replace('.', '', $valorTotalLote),17,0,STR_PAD_LEFT);
		$dados['quantidadeCaucionada']        = str_repeat(0, 6);
		$dados['valorCaucionada']             = str_repeat(0, 17);
		$dados['quantidadeCobranca']          = str_repeat(0, 6);
		$dados['quantidadeCarteira']          = str_repeat(0, 17);
		$dados['fillerItem12']                = str_repeat(chr(32), 31);
		$dados['fillerItem13']                = str_repeat(chr(32), 117);

		$conteudo = $dados['codigoBanco']  	   			.
					$dados['loteServico']  	   			.
					$dados['tipoRegistro'] 	   			.
					$dados['fillerItem04'] 	   			.
					$dados['quantidadeRegistros'] .
					$dados['quantidadeTituloLote'].
					$dados['valorTotalLote']      .
					$dados['quantidadeCaucionada'].
					$dados['valorCaucionada']     .
					$dados['quantidadeCobranca']  .
					$dados['quantidadeCarteira']  .
					$dados['fillerItem12']        .
					$dados['fillerItem13'].PHP_EOL;

		return $conteudo;

	}
	public function trailerHeaderArquivoRemessaCaixa($resultado,$registrosLote)

	{
		$dados['codigoBanco']  	   			  = '104';
		$dados['loteServico']  	   			  = '9999';
		$dados['tipoRegistro'] 	   			  = '9';
		$dados['fillerItem04'] 	   			  = str_repeat(chr(32), 9);
		$dados['quantidadeLotes']             = str_pad('1',6,0,STR_PAD_LEFT);
		// Antes de implementar a linha "R"
		// $dados['quantidadeRegistros']         = str_pad(($registrosLote*2)+2+2,6,0,STR_PAD_LEFT);

		$dados['quantidadeRegistros']         = str_pad(($registrosLote*3)+2+2,6,0,STR_PAD_LEFT);
		
		$dados['fillerItem07']                = str_repeat(chr(32), 6);
		$dados['fillerItem08']                = str_repeat(chr(32), 205);

		$conteudo = $dados['codigoBanco']  	   		.
					$dados['loteServico']  	   		.
					$dados['tipoRegistro'] 	   		.
					$dados['fillerItem04'] 	   		.
					$dados['quantidadeLotes']    .
					$dados['quantidadeRegistros'].
					$dados['fillerItem07']       .
					$dados['fillerItem08'].PHP_EOL;

		return $conteudo;

	}

	public function nomeArquivoRemessa()
	{
		$codigoCliente = $this->uri->segment(3);

		// $sequencia = $this->Boletos_model->pegarSequenciaCliente($codigoCliente);
		$sequencia = str_pad($this->Boletos_model->pegarSequenciaCliente($codigoCliente),5,0,STR_PAD_LEFT);

		$nome['nome'] = 'e'.date('d').$sequencia.'.rem';

		echo json_encode($nome);
	}


public function arquivoRemessaBB($acao = '',$codigoCliente = '',$codigoApi = '')
	{		
		$codigoBanco = '001';
	
				$codigoCliente = $this->uri->segment(3);
				$token         = $this->uri->segment(4);
				$convenio      = $this->uri->segment(5);
				

				$where = array(
						'CLIE_codigo'         => $codigoCliente,
						'BANC_CodigoBanco'    => $codigoBanco,
						'BOLE_NumeroConvenio' => $convenio,
						'STATUS_boleto'       => 0
					);

				$acaoRemessa = '01';
				$status      = 1;

			

		$resultados = $this->Boletos_model->dadosArquivosRemessaBB($where);
		
		if($resultados){

			$registrosLote  = 0;
			$valorTotalLote = 0;

			$dados['header']       = $this->headerArquivoRemessaBB($resultados[0]);

			$dados['headerLote']   = $this->headerLoteArquivoRemessaBB($resultados[0]);

			$conteudo = $dados['header'].$dados['headerLote'];			

			foreach ($resultados as $resultado) {
				
				$dados['dadosRemessaP'] = $this->dadosTituloBBRemessaTipoP($registrosLote,$resultado,$acaoRemessa);

				$dados['dadosRemessaQ'] = $this->dadosTituloBBRemessaTipoQ($registrosLote,$resultado);
				
				$valorTotalLote += str_replace('.', '', $resultado->BOLE_ValorDocumento);

				$registrosLote++;

				$conteudo.= $dados['dadosRemessaP'].$dados['dadosRemessaQ'];
	
		}

			$dados['trailerLote']     = $this->trailerLoteArquivoRemessaBB($resultados[0],$registrosLote,$valorTotalLote);
			$dados['trailerHeader']   = $this->trailerHeaderArquivoRemessaBB($resultados[0],$registrosLote);
			$sequencia = $this->Boletos_model->pegarSequenciaCliente($resultados[0]->CLIE_codigo);

			$sequencia = str_pad(($sequencia*1),5,0,STR_PAD_LEFT);

			$conteudo .= $dados['trailerLote'].$dados['trailerHeader'];

			$caminho     = 'assets/arquivos/remessa/'.$codigoCliente.'.txt';		

			$caminhoCopy = 'assets/arquivos/remessa/'.$codigoCliente.date('d_m_Y_H_i').'.txt';			

			$file = fopen($caminho, 'w+');
			fwrite($file, $conteudo);
			fclose($file);

			copy($caminho, $caminhoCopy);

			$resultados = $this->Boletos_model->atualizarStatusArquivosRemessaBB($where,$status);

			$dados = file_get_contents($caminho); // Lê o conteúdo do arquivo
			$nome = 'e'.date('d').$sequencia.'.rem';

			force_download($nome, $dados);
			
		}else{

			$retorno['status']    = false;	
			$retorno['codstatus'] = 105;		
			$retorno['msg'] = 'Boletos nao encotrado!';
			echo json_encode($retorno);
		}
	}

	public function headerArquivoRemessaBB($resultado)
	{
        
        $nsa = $this->Boletos_model->pegarSequenciaClienteUpdate($resultado->CLIE_codigo);

        $tipoInscricaoBeneficiario = (strlen($resultado->BOLE_CNPJCPFCedente) == 14)?'3':'2';
		$ambiente = 'TESTE';//'(REMESSA ou TESTE)
		$VariacaoCarteira = '027'; // 07.0 -> Convenio -> Variacao da carteira



		$dados['CodigoBancoCompensacao']      =  '001'; // 01.0 -> Cod. do banco - (G001) 
		$dados['LoteServico']                 =  str_pad($nsa,4,0,STR_PAD_LEFT); // 02.0 -> Cod. do lote
		$dados['TipoRegistro']                =  '0'; // 03.0 -> Tipo de Registro
		$dados['UsoExclusivoFEBRABANCNA1']    =   str_repeat(chr(32), 9); // 04.0 -> CNAB literal remessa escr. extenso 003 009 X(07)
		$dados['TipoInscricaoEmpresa']        =  $tipoInscricaoBeneficiario; // 05.0 -> Tipo de inscrição do beneficiario : um se pessoa fisico (1) ou juridica (2)
		$dados['NumeroInscricaoEmpresa']      =  $resultado->BOLE_CNPJCPFCedente;  // 06.0 -> Nº de Inscrição do  Beneficiario cpf ou cnpj
		
		$dados['CodigoConvenioBanco'] 		  =  str_pad(substr($resultado->BOLE_NumeroConvenio,0,9),9,0,STR_PAD_LEFT);  // 07.0 -> Convenio -> Codigo do convenio no banco
		$dados['CodigoConvenioBanco'] 		  =  $dados['CodigoConvenioBanco'] .'0014'; // 07.0 -> Convenio -> Cobranca cedente
		 

		$dados['NumeroCarteiraCobrancaBB']          = $resultado->BOLE_NumCarteira; // 07.0 -> Convenio -> Numero da carteira
		$dados['NumeroVariacaoCarteiraCobrancaBB']  = $VariacaoCarteira;   // 07.0 -> Convenio -> Variacao da carteira

		$dados['CodigoConvenioBancoXX'] 		  =   str_repeat(chr(32),2); // 07.0 -> Convenio -> Complemento BB	

		$dados['AgenciaMantenedoraConta']     =  str_pad(substr($resultado->BOLE_Agencia,0,5),5,0,STR_PAD_LEFT);// Código da agência do BB responsável pela conta do convênio (onde serão debitadas as parcelas dos empréstimos).
		$dados['DigitoVerificadorAgencia']    =  $resultado->BOLE_DigAgencia; // Código adotado pelo BB para verificação da autenticidade do código da agência.
		$dados['NumeroContaCorrente']         =  str_pad((integer)$resultado->BOLE_Conta,12,0,STR_PAD_LEFT);// Número da conta corrente do convênio no BB (conta em que serão debitadas as parcelas dos empréstimos).
		$dados['NumeroDigContaCorrente']      = str_pad($resultado->BOLE_DigConta,1,0,STR_PAD_LEFT); // Código adotado pelo BB para verificação da autenticidade do número da conta corrente
		$dados['DigitoVerificadorAgConta']    = str_repeat(0,1); // Código adotado pelo BB para verificação da autenticidade do par código da agência/ número da conta corrente do convênio.
		$dados['NomeEmpresa']                 = str_pad(strtoupper(substr($resultado->BOLE_NomeCedente,0,30)),30,chr(32),STR_PAD_RIGHT); // Nome do Cedente  
		$dados['NomeBanco']                   = str_pad('001BANCODOBRASIL',30,chr(32),STR_PAD_RIGHT); // 001BANCODOBRASIL
		$dados['UsoExclusivoFEBRABANCNA2']    = str_repeat(chr(32),10); // Branco
		$dados['CodigoRemessaRetorno']        = '1'; // 1 (Remessa)
		$dados['dataGeracaoArquivo']          = date('dmY');//Preencher com a data da criação do arquivo, no formato DDMMAAAA (Dia, Mês e Ano)
		$dados['horaGeracaoArquivo']          = str_pad(date('His'),6,0,STR_PAD_LEFT);//Preencher com a hora de criação do arquivo, no formato HHMMSS (Hora, Minuto e Segundos)
		$dados['nsa']                         = str_pad($nsa,6,0,STR_PAD_LEFT);//Número seqüencial adotado e controlado pelo responsável pela geração do arquivo para ordenar a disposição dos arquivos encaminhados; evoluir de 1 em 1 para cada Header de Arquivo
		$dados['NVersaoLayoutArquivo']        = '084'; // Complemento do Registro: “Brancos”
		$dados['DensidadeGravacaoArquivo']    = str_repeat(0,5); // zero
		
		$dados['ParaUsoReservadoBanco']       = 'CDC240'; // Posição 172 a 177 = CDC240 (Nome do arquivo).
		$dados['ParaUsoReservadoBanco']       = $dados['ParaUsoReservadoBanco'].str_repeat(0,9); // Posição 178 a 186 = Zeros (Nº de controle do BB).
		$dados['ParaUsoReservadoBanco']       = $dados['ParaUsoReservadoBanco'].str_repeat(chr(32),5); // Posição 187 a 191 = Brancos
		
		$dados['ParaUsoReservadoEmpresa']     = str_repeat(chr(32),20); // Complemento do Registro: “Brancos”
		$dados['UsoExclusivoFEBRABANCNA3']     = str_repeat(chr(32),29); // Complemento do Registro: “Brancos”



		$conteudo = $dados['CodigoBancoCompensacao']    .
					$dados['LoteServico']  	   		    .
					$dados['TipoRegistro'] 	            .
					$dados['UsoExclusivoFEBRABANCNA1'] 	.
					$dados['TipoInscricaoEmpresa']      .
					$dados['NumeroInscricaoEmpresa']    .
					$dados['CodigoConvenioBanco']       .

					$dados['NumeroCarteiraCobrancaBB']  .
					$dados['NumeroVariacaoCarteiraCobrancaBB'] .
					$dados['CodigoConvenioBancoXX']      .

					$dados['AgenciaMantenedoraConta'] 	 .
					$dados['DigitoVerificadorAgencia'] 	 .

					$dados['NumeroContaCorrente']        .
					$dados['NumeroDigContaCorrente']     .
					$dados['DigitoVerificadorAgConta']   .

					$dados['NomeEmpresa']                .					
					$dados['NomeBanco']                  .
					$dados['UsoExclusivoFEBRABANCNA2']   .
					$dados['CodigoRemessaRetorno']       .
					$dados['dataGeracaoArquivo']         .
					$dados['horaGeracaoArquivo']         .
					$dados['nsa']                        .

					$dados['NVersaoLayoutArquivo']       .
					$dados['DensidadeGravacaoArquivo']   .
					$dados['ParaUsoReservadoBanco']      .
					$dados['ParaUsoReservadoEmpresa']    .
					$dados['UsoExclusivoFEBRABANCNA3'].PHP_EOL;


 //v($dados);

		return $conteudo;



	}


    public function headerLoteArquivoRemessaBB($resultado)
	{	
		$tipoInscricaoBeneficiario = (strlen($resultado->BOLE_CNPJCPFCedente) == 14)?'2':'1';
		$nomeEmpresa               = strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,30));
		$loteServico               = 1;
		$tipoServico               = '01';
		$nsa = $this->Boletos_model->pegarSequenciaCliente($resultado->CLIE_codigo);
		$VariacaoCarteira = '027'; // 07.0 -> Convenio -> Variacao da carteira		

		
		$dados['CodigoBancoCompensacao']  	  = '001';//Preencher '001
		$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica

		$dados['TipoRegistro']  	   		  = '1';
		$dados['TipoOperacao']  	   		  = 'R';
		$dados['tipoServico']  	   			  = $tipoServico;//Preencher com ‘01', se Cobrança Registrada; ou ‘02’, se Cobrança Sem Registro/Serviços
        $dados['UsoExclusivoFEBRABANCNA1']     = str_repeat(chr(32),2); // Complemento do Registro: “Brancos”		
		$dados['numeroVersaoLote'] 			  = '043';//Preencher '030'
		$dados['UsoExclusivoFEBRABANCNA2']     = str_repeat(chr(32),1); // Complemento do Registro: “Brancos”
		$dados['TipoInscricaoEmpresa']	      = $tipoInscricaoBeneficiario;//Preencher com o tipo de inscrição do Beneficiário: '1', se CPF (pessoa física); ou '2' se CNPJ (pessoa jurídica)
		$dados['numeroInscricaoBeneficiario'] = str_pad($resultado->BOLE_CNPJCPFCedente,15,0,STR_PAD_LEFT);//Número de Inscrição da Empresa ou Pessoa Física Número de inscrição da Empresa (CNPJ) ou Pessoa Física (CPF).

		$dados['NumeroConvenioCobrancaBB']    = str_pad(substr($resultado->BOLE_NumeroConvenio,0,9),9,0,STR_PAD_LEFT);//Informar o número do convênio de cobrança, alinhado à direita com zeros à esquerda. 
		$dados['CobrancaCedenteBB'] 		        = '0014';//Informar 0014 para cobrança cedente 
		$dados['NumeroCarteiraCobrancaBB']          = $resultado->BOLE_NumCarteira;//Informar o número da carteira de cobrança
		$dados['NumeroVariacaoCarteiraCobrancaBB']  = $VariacaoCarteira; //Informar o número da variação da carteira de cobrança
		$dados['CampoIdentificaRemessaTestes']      = str_repeat(chr(32),2); // "TS".(TESTE)  Caso não queira realizar os testes, informe brancos.
        
        $dados['AgenciaMantenedoraConta']     = str_pad((integer)$resultado->BOLE_Agencia,5,0,STR_PAD_LEFT);//Preencher com o código da agência detentora da conta, com um zero à esquerda
		$dados['DigitoVerificadorAgencia']    = strtoupper($resultado->BOLE_DigAgencia);//Obs. Em caso de dígito X informar maiúsculo
		$dados['NumeroContaCorrente']         = str_pad(substr($resultado->BOLE_Conta,0,12),12,0,STR_PAD_LEFT);//
		$dados['DigitoVerificadorConta']      = strtoupper($resultado->BOLE_DigConta);///Obs. Em caso de dígito X informar maiúsculo.
		$dados['DigitoVerificadorAgConta'] 	  = str_repeat(0,1);//Campo não tratado pelo Banco do Brasil. Informar 'branco' (espaço) OU zero		
		$dados['nomeEmpresa']                 = str_pad($nomeEmpresa,30,chr(32),STR_PAD_RIGHT);// Preencher com o Nome da empresa
		$dados['mensagem1']                   = str_repeat(chr(32), 40);
		$dados['mensagem2']                   = str_repeat(chr(32), 40);
		$dados['numeroRemessa']               = str_pad($nsa,8,0,STR_PAD_LEFT);//Número seqüencial adotado e controlado pelo responsável pela geração do arquivo para ordenar a disposição dos arquivos encaminhados; evoluir de 1 em 1 para cada Header de Arquivo
		$dados['DataGravacaoRemessaRetorno']  = date('dmY');
		$dados['DataCredito']                 = date('dmY');
		$dados['UsoExclusivoFEBRABANCNA3']    = str_repeat(chr(32), 33);




		$headerLote =  $dados['CodigoBancoCompensacao'].
					   $dados['loteServico'].
					   $dados['TipoRegistro'].
					   $dados['TipoOperacao'].
					   $dados['tipoServico'].
					   $dados['UsoExclusivoFEBRABANCNA1'].					   
					   $dados['numeroVersaoLote'].
					   $dados['UsoExclusivoFEBRABANCNA2'].
					   $dados['TipoInscricaoEmpresa'].
					   $dados['numeroInscricaoBeneficiario'].
					   $dados['NumeroConvenioCobrancaBB'].

					   $dados['CobrancaCedenteBB'].
					   $dados['NumeroCarteiraCobrancaBB'].
					   $dados['NumeroVariacaoCarteiraCobrancaBB'].
					   $dados['CampoIdentificaRemessaTestes'].
					   $dados['AgenciaMantenedoraConta'].
					   $dados['DigitoVerificadorAgencia'].
					   $dados['NumeroContaCorrente'].
					   $dados['DigitoVerificadorConta'].
					   $dados['DigitoVerificadorAgConta'].
					   $dados['nomeEmpresa'].
					   $dados['mensagem1'].
					   $dados['mensagem2'].
					   $dados['numeroRemessa'].
					   $dados['DataGravacaoRemessaRetorno'].
					   $dados['DataCredito'].
					   $dados['UsoExclusivoFEBRABANCNA3'].PHP_EOL;

   // v($dados);

		return $headerLote;
	}


public function dadosTituloBBRemessaTipoP($registrosLote,$resultado,$acaoRemessa)
	{
		$loteServico               = 1;
		$sequenciaRegistro         = $registrosLote+1;

		
	

		$dados['CodigoBancoCompensacao']  	  = '001';//Preencher '001’
		$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
		$dados['tipoRegistro'] 	   			  = '3';//Preencher '3’ (equivale a Header de Lote)
		$dados['NSequencialRegistroLote']     = str_pad($sequenciaRegistro,5,0,STR_PAD_LEFT);//Começar com 00001 e ir incrementando em 1 a cada nova linha de registro detalhe
		$dados['CodSegmentoRegistroDetalhe']  = 'P';//Preencher 'P’
		$dados['UsoExclusivoFEBRABANCNAB1']   = str_repeat(chr(32), 1);//Preencher com espaços
/*ver*/	$dados['codigoMovimentoRemessa']      = $acaoRemessa;//  01 – Entrada de títulos, 02 – Pedido de baixa  -- Ver Nota Explicativa C004 
		$dados['agenciaMantenedora']          = '0'.str_pad((integer)$resultado->BOLE_Agencia,4,0,STR_PAD_LEFT);//Preencher com o código da agência detentora da conta, com um zero à esquerda
		$dados['digitoVerificadorAgencia']    = strtoupper($resultado->BOLE_DigAgencia);//Obs. Em caso de dígito X informar maiúsculo
		$dados['NumeroContaCorrente']         = str_pad(substr($resultado->BOLE_Conta,0,12),12,0,STR_PAD_LEFT);//Número adotado pelo Banco, para identificar univocamente a conta corrente utilizada pelo Cliente
		$dados['DigitoVerificadorConta']      = strtoupper($resultado->BOLE_DigConta);///Obs. Em caso de dígito X informar maiúsculo.
		$dados['DigitoVerificadorAgConta'] 	  = str_repeat(0,1);//Campo não tratado pelo Banco do Brasil. Informar 'branco' (espaço) OU zero.
		
		


		$dados['IdentificacaoTituloBanco']    = str_pad($resultado->BOLE_NumeroConvenio,7,0,STR_PAD_RIGHT);

		$dados['IdentificacaoTituloBanco']    = $dados['IdentificacaoTituloBanco']. str_pad($resultado->BOLE_Sequencial,10,0,STR_PAD_LEFT);
		$dados['TipoDocumento']               = str_repeat(chr(32), 3);
		
		$dados['CodigoCarteira']              = '7';//1 – para carteira 11/12 na modalidade Simples; 2 ou 3 – para carteira 11/17 modalidade Vinculada/Caucionada e carteira 31; 4 – para carteira 11/17 modalidade Descontada e carteira 51; e 7 – para carteira 17 modalidade Simples.
		$dados['FormaCadastrTituloBanco']     = '1';//Campo não tratado pelo sistema do Banco do Brasil. Pode ser informado 'branco', Zero, 1 – com cadastramento (Cobrança registrada) ou 2 – sem cadastramento (Cobrança sem registro).

		
		//$dados['TipoDocumento']               = str_repeat(chr(32), 1);

		$dados['identificacaoEmissaoBoleto']  = '2';// 16.3P -> Nao tratado pelo banco, pode ser tipo de Documento - Preencher '2’ (equivale a Escritural)
		$dados['IdentificacaoDistribuicao']   = '2';// 17.3P -> Identificação da Emissao do boleto. 1 = Banco emite/ 2 = Cliente emite (C009)
		$dados['IdentificacaoDistribuicao']   = $dados['IdentificacaoDistribuicao']. '2';// 18.3P -> Identificacao da Entrega/distribuicao do boleto. => Carteira 17 = 1 (banco) ou 2 (cliente) (C010)

		

		$dados['numeroDocumentoCobranca']     = str_pad($resultado->BOLE_Sequencial,15,chr(32),STR_PAD_RIGHT);		

			
		$dados['dataVencimento']              = date('dmY',strtotime($resultado->BOLE_Vencimento));
		$dados['ValorNominalTitulo']          = str_pad(str_replace('.', '',$resultado->BOLE_ValorDocumento),15,0,STR_PAD_LEFT);
		

		$dados['agenciaCobranca']             = str_repeat(0, 5);//Preencher com zeros
		$dados['DigitoVerificadorAgencia']    = str_repeat(chr(32), 1);//Preencher com zeros

		$dados['especieTitulo']               = '24';//Ver Nota Explicativa C015
		$dados['IdentificTituloAceitoNaoAceito'] = 'A';//Indica se o título de cobrança possui aceite do pagador; preencher com ‘A’ (Aceite) ou ‘N’ (Não Aceite)
		$dados['dataEmissaoTitulo']           = date('dmY',strtotime($resultado->BOLE_DataProc));
		$dados['codigoJurosMora']             = '3';//‘1’ (Valor por Dia); ou ‘2’ (Taxa Mensal); ou ‘3’ (Isento)
		$dados['dataJurosMora']               = str_repeat(0, 8);//Preencher com a data indicativa do início da cobrança de Juros de Mora, no formato DDMMAAAA (Dia, Mês e Ano), devendo ser maior que a Data de Vencimento; ATENÇÃO, caso a informação seja inválida ou não informada, o sistema assumirá data igual à Data de Vencimento + 1
		$dados['jurosMoraDiaTaxa']         = str_repeat(0, 15);

		if( $valor_desconto >0 ){
		$dados['codigoDesconto']           = str_repeat(1, 1);
		$dados['dataDesconto']             = date('dmY',strtotime($resultado->BOLE_Vencimento));
		$dados['valorDesconto']            = str_pad(str_replace('.', '',$resultado->BOLE_Desconto),15,0,STR_PAD_LEFT);
		}else{
		$dados['codigoDesconto']           = str_repeat(0, 1);
		$dados['dataDesconto']             = str_repeat(0, 8);
		$dados['valorDesconto']            = str_repeat(0, 15);
		}

		$dados['valorIOF']                    = str_repeat(0, 15);
		$dados['valorAbatimento']             = str_repeat(0, 15);
		$dados['identificacaoTituloEmpresea'] = str_pad($resultado->BOLE_Sequencial,25,chr(32),STR_PAD_RIGHT);
		$dados['codigoProtesto']              = '3';
		$dados['numeroDiasProtesto']          = '00';
		$dados['codigoBaixaDevolucao']        = '1';
		$dados['numerodiasBaixaDevolucao']    = '120';
		$dados['codigoMoeda']                 = '09';
		$dados['NContratoOperacaoCred']       = str_repeat(0, 10);
		$dados['UsoExclusivoFEBRABANCNAB2']   = str_repeat(chr(32), 1);

			$conteudo = $dados['CodigoBancoCompensacao']	 .
						$dados['loteServico']  	   			 .
						$dados['tipoRegistro'] 	   			 .
						$dados['NSequencialRegistroLote']    .
						$dados['CodSegmentoRegistroDetalhe'] .
						$dados['UsoExclusivoFEBRABANCNAB1']   .
						$dados['codigoMovimentoRemessa']     .
						$dados['agenciaMantenedora']         .


						$dados['digitoVerificadorAgencia']   .
						$dados['NumeroContaCorrente']        .
						$dados['DigitoVerificadorConta']     .
						$dados['DigitoVerificadorAgConta']   .
						
						$dados['IdentificacaoTituloBanco']   .
						$dados['TipoDocumento']              .
						$dados['CodigoCarteira']             .
						$dados['FormaCadastrTituloBanco']    .
						

						
						$dados['identificacaoEmissaoBoleto'] .
						$dados['IdentificacaoDistribuicao']  .

						$dados['numeroDocumentoCobranca']    .
						$dados['dataVencimento']             .
						$dados['ValorNominalTitulo']         .
						$dados['agenciaCobranca']            .
						$dados['DigitoVerificadorAgencia']   .

						$dados['especieTitulo']              .
						$dados['IdentificTituloAceitoNaoAceito']         .

						$dados['dataEmissaoTitulo']         .
						$dados['codigoJurosMora']           .
						$dados['dataJurosMora']             .

						$dados['jurosMoraDiaTaxa']        	.
						$dados['codigoDesconto']        	.
						$dados['dataDesconto']        		.
						$dados['valorDesconto']     		.
						$dados['valorIOF']        			.
						$dados['valorAbatimento']        	.

						$dados['identificacaoTituloEmpresea']   .
						$dados['codigoProtesto']        		.
						$dados['numeroDiasProtesto']        	.
						$dados['codigoBaixaDevolucao']        	.
						$dados['numerodiasBaixaDevolucao']      .
						$dados['codigoMoeda']        			.
						$dados['NContratoOperacaoCred']         .
						$dados['UsoExclusivoFEBRABANCNAB2'].PHP_EOL;

       // v($dados);

		return $conteudo;

	}



	public function dadosTituloBBRemessaTipoQ($registrosLote,$resultado)
	{	
		$documentoPagador          = str_replace('/', '', $resultado->BOLE_CGC);
		$documentoPagador          = str_replace('-', '', $documentoPagador);
		$documentoPagador          = str_replace('.', '', $documentoPagador);

		$documentoSacador          = str_replace('/', '', $resultado->BOLE_CNPJCPFCedente);
		$documentoSacador          = str_replace('-', '', $documentoSacador);
		$documentoSacador          = str_replace('.', '', $documentoSacador);

		$tipoInscricaoSacador      = (strlen($documentoSacador) == 14)?'2':'1';

		$loteServico               = 1;
		$sequenciaRegistro         = $registrosLote+1;
		$tipoInscricaoPagador      = (strlen($documentoPagador) == 14)?'2':'1';
		$nomeEmpresa               = str_pad(strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,40)),40,chr(32),STR_PAD_RIGHT);

			$dados['CodigoBancoCompensacao'] 	  = '001';//Preencher '001'
			$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT);//Após o fechamento de um Lote, o próximo Lote deve possuir o número do lote anterior acrescido de 1, e a mesma lógica do segmentos do novo lote seguirem o número dele se aplica
			$dados['tipoRegistro'] 	   			  = '3';//Preencher '3’ (equivale a Header de Lote)
			$dados['NSequencialRegistroLote']     = str_pad($sequenciaRegistro,5,0,STR_PAD_LEFT);//Ver Nota Explicativa G038; evoluir de 1 em 1 para cada Segmento do Lote
			$dados['CodSegmentoRegistroDetalhe']  = 'Q';//Preencher 'P’
			$dados['UsoExclusivoFEBRABANCNABQ1']   = str_repeat(chr(32), 1);//Preencher com espaços
	/*ver*/	$dados['codigoMovimentoRemessa']      = '17';//Ver Nota Explicativa C004
			$dados['tipoInscricaoPagador']	      = $tipoInscricaoPagador;//Preencher com o tipo de inscrição do Beneficiário: '1', se CPF (pessoa física); ou '2' se CNPJ (pessoa jurídica)
			$dados['numeroInscricaoPagador']      = str_pad($documentoPagador,15,0,STR_PAD_LEFT);
			$dados['nomePagador']                 = str_pad(removerAcentos(substr($resultado->BOLE_NomeCliente,0,40)),40,chr(32),STR_PAD_RIGHT);
			$dados['enderecoPagador']             = str_pad(removerAcentos(substr($resultado->BOLE_EndeCliente,0,40)),40,chr(32),STR_PAD_RIGHT);
			$dados['bairroPagador']               = str_pad(removerAcentos(substr($resultado->BOLE_Bairro,0,15)),15,chr(32),STR_PAD_RIGHT);
			$dados['cepPagador']                  = str_pad(removerAcentos(substr($resultado->BOLE_CEP,0,5)),5,0,STR_PAD_LEFT);
			$dados['sufixoCepPagador']            = str_pad(removerAcentos(substr($resultado->BOLE_CEP,-3)),3,0,STR_PAD_LEFT);
			$dados['cidadePagador']               = str_pad(removerAcentos(substr($resultado->BOLE_Cidade,0,15)),15,chr(32),STR_PAD_RIGHT);
			$dados['unidadeFederacaoPagador']     = str_pad(removerAcentos(substr(strtoupper($resultado->BOLE_UF),0,2)),2,chr(32),STR_PAD_RIGHT);
			$dados['tiposInscricaoSacador']       = $tipoInscricaoSacador;
			$dados['numeroInscricaoSacador']      = str_pad($resultado->BOLE_CNPJCPFCedente,15,0,STR_PAD_LEFT);
			$dados['nomeSacador']                 = $nomeEmpresa;
			$dados['bancoCorrespndente']          = str_repeat(0, 3);
			$dados['nossoBancoCorrespondente']    = str_repeat(chr(32), 20);
			$dados['UsoExclusivoFEBRABANCNAB']    = str_repeat(chr(32), 8);
			

			$conteudo = $dados['CodigoBancoCompensacao']     .
						$dados['loteServico']  	   			 .
						$dados['tipoRegistro'] 	   			 .
						$dados['NSequencialRegistroLote']    .
						$dados['CodSegmentoRegistroDetalhe'] .
						$dados['UsoExclusivoFEBRABANCNABQ1']   .
						$dados['codigoMovimentoRemessa'] .
						$dados['tipoInscricaoPagador']	 .
						$dados['numeroInscricaoPagador'] .
						$dados['nomePagador']            .
						$dados['enderecoPagador']        .
						$dados['bairroPagador']          .
						$dados['cepPagador']             .
						$dados['sufixoCepPagador']       .
						$dados['cidadePagador']          .
						$dados['unidadeFederacaoPagador'].
						$dados['tiposInscricaoSacador']  .
						$dados['numeroInscricaoSacador'] .
						$dados['nomeSacador']            .
						$dados['bancoCorrespndente']     .
						$dados['nossoBancoCorrespondente'].
						$dados['UsoExclusivoFEBRABANCNAB'].PHP_EOL;


      //  v($dados);


		return $conteudo;
	}	


	public function trailerLoteArquivoRemessaBB($resultado,$registros,$valorTotalLote)
	{
		//$tipoInscricaoBeneficiario = (strlen($resultado->BOLE_CNPJCPFCedente) == 14)?'2':'1';
		//$nomeEmpresa               = strtoupper(substr(removerAcentos($resultado->BOLE_NomeCedente),0,30));
		$loteServico               = 1;
		//$tipoServico               = '01';
		//$nsa                       = 1; //falta fazer

		$dados['CodigoBancoCompensacao']  	  = '001'; // 001 para Banco do Brasil S.A
		$dados['loteServico']  	   			  = str_pad($loteServico,4,0,STR_PAD_LEFT); // Informar mesmo número do header de lote
		$dados['tipoRegistro'] 	   			  = '5';
		$dados['UsoExclusivoFEBRABANCNAB1'] 	  = str_repeat(chr(32), 9);
		$dados['quantidadeRegistros']         = str_pad(($registros*2)+2,6,0,STR_PAD_LEFT);//$registro é a quantidade de interações *2 são dois registro por intereração e +2 é o header e o trailer
		$dados['UsoExclusivoFEBRABANCNAB2']        = str_pad($registros,217,chr(32),STR_PAD_RIGHT); // Informar Zeros e 'brancos'.


		$conteudo = $dados['CodigoBancoCompensacao']  	.
					$dados['loteServico']  	   			.
					$dados['tipoRegistro'] 	   			.
					$dados['UsoExclusivoFEBRABANCNAB1']  .
					$dados['quantidadeRegistros']       .									
					$dados['UsoExclusivoFEBRABANCNAB2'].PHP_EOL;

	// v($dados);


		return $conteudo;

	}


	public function trailerHeaderArquivoRemessaBB($resultado,$registrosLote)

	{
		$dados['codigoBanco']  	   			  = '001';
		$dados['loteServico']  	   			  = '9999';
		$dados['tipoRegistro'] 	   			  = '9';
		$dados['fillerItem04'] 	   			  = str_repeat(chr(32), 9);

		$dados['quantidadeLotes']             = str_pad('1',6,0,STR_PAD_LEFT);
		$dados['quantidadeRegistros']         = str_pad(($registrosLote*2)+2+2,6,0,STR_PAD_LEFT);

		$dados['fillerItem07']                = str_repeat(chr(32), 6);
		$dados['fillerItem08']                = str_repeat(chr(32), 205);

		$conteudo = $dados['codigoBanco']  	   		.
					$dados['loteServico']  	   		.
					$dados['tipoRegistro'] 	   		.
					$dados['fillerItem04'] 	   		.
					$dados['quantidadeLotes']    .
					$dados['quantidadeRegistros'].
					$dados['fillerItem07']       .
					$dados['fillerItem08'].PHP_EOL;

		//v($dados);			

		return $conteudo;

	}



public function headerArquivoRemessaBB400($resultado)
	{
        
        $nsa = $this->Boletos_model->pegarSequenciaClienteUpdate($resultado->CLIE_codigo);

		$ambiente = 'TESTE';//'(REMESSA ou TESTE)


		$dados['IdentificacaoRegistroHeader']        =  '0';  //Identificação do Registro Header: “0” (zero)x
		$dados['TipoOperacao']                       =  '1'; // Tipo de Operação: “1” (um)
		$dados['IdentificacaoExtensoTipoOperacao']   = str_pad(strtoupper($ambiente),7,chr(32),STR_PAD_RIGHT);// Identificação por Extenso do Tipo de Operação
		$dados['IdentificacaoTipoServico']           =  '01'; // Identificação do Tipo de Serviço: “01”
		$dados['IdentificacaoExtensoTipoServico']    =  'COBRANCA'; // Identificação por Extenso do Tipo de Serviço: “COBRANCA”
		$dados['ComplementoRegistro06']              =  str_repeat(chr(32), 7); //Complemento do Registro: “Brancos”
		$dados['NumeroAgencia'] 					 =  str_pad((integer)$resultado->BOLE_Agencia,4,0,STR_PAD_LEFT); // Prefixo da Agência: Número da Agência onde está cadastrado o convênio líder do cedente - NOTA 02
		$dados['DigitoVerificadorAgencia']           = $resultado->BOLE_DigAgencia;// Dígito Verificador - D.V. - do Prefixo da Agência.
		$dados['NumeroContaCorrente']                = str_pad((integer)$resultado->BOLE_Conta,8,0,STR_PAD_LEFT);// Número da Conta Corrente: Número da conta onde está cadastrado o Convênio Líder do Cedente
		$dados['NumeroDigContaCorrente']             = str_pad((integer)$resultado->BOLE_DigConta,1,0,STR_PAD_LEFT); //Dígito Verificador - D.V. – do Número da Conta Corrente do Cedente
		$dados['ComplementoRegistro11']              = str_repeat(0,6); // Complemento do Registro: “000000”
		$dados['NomeCedente']                        = str_pad(strtoupper(substr($resultado->BOLE_NomeCedente,0,30)),30,chr(32),STR_PAD_RIGHT); // Nome do Cedente  
		$dados['BancoBrasil']                        =  chr(32).chr(32).'001BANCODOBRASIL'; // 001BANCODOBRASIL
		$dados['DataGravacao']                       = date('dmy'); // Data da Gravação: Informe no formato “DDMMAA”
		$dados['SequencialRemessa']                  = str_pad($nsa,7,0,STR_PAD_LEFT); // Seqüencial da Remessa
		$dados['ComplementoRegistro16']              = str_repeat(chr(32), 22); // Complemento do Registro: “Brancos”
		$dados['NumeroConvenioLider']                = str_pad(substr($resultado->BOLE_NumeroConvenio,0,7),7,0,STR_PAD_LEFT); // Número do Convênio Líder (numeração acima de 1.000.000 um milhão)"
		$dados['ComplementoRegistro18']              = str_repeat(chr(32),258); // Complemento do Registro: “Brancos”
		$dados['SequencialRegistro']                 = '000001'; // Seqüencial do Registro:”000001”




		$conteudo = $dados['IdentificacaoRegistroHeader']  	    .
					$dados['TipoOperacao']  	   		        .
					$dados['IdentificacaoExtensoTipoOperacao'] 	.
					$dados['IdentificacaoTipoServico'] 	   		.
					$dados['IdentificacaoExtensoTipoServico']   .
					$dados['ComplementoRegistro06']      .
					$dados['NumeroAgencia']              .

					$dados['DigitoVerificadorAgencia'] 	 .
					$dados['NumeroContaCorrente']        .
					$dados['NumeroDigContaCorrente']     .
					$dados['ComplementoRegistro11']      .
					$dados['NomeCedente']                .
					
					$dados['BancoBrasil']                .
					$dados['DataGravacao']               .
					$dados['SequencialRemessa']          .
					$dados['ComplementoRegistro16']      .
					$dados['NumeroConvenioLider']        .
					$dados['ComplementoRegistro18']      .
					$dados['SequencialRegistro'].PHP_EOL;

		return $conteudo;

		
	}





	public function dadosTituloBB400Remessa($registrosLote,$resultado)
	{

		$documentoPagador          = str_replace('/', '', $resultado->BOLE_CNPJCPFCedente);
		$documentoPagador          = str_replace('-', '', $documentoPagador);
		$documentoPagador          = str_replace('.', '', $documentoPagador);

		$documentoSacador          = str_replace('/', '', $resultado->BOLE_CGC);
		$documentoSacador          = str_replace('-', '', $documentoSacador);
		$documentoSacador          = str_replace('.', '', $documentoSacador);

		$tipoInscricaoSacador      = (strlen($documentoSacador) == 14)?'02':'01'; // Quem vai pagar 

		$loteServico               = 1;
		$sequenciaRegistro         = $registrosLote+1;
		$tipoInscricaoPagador      = (strlen($documentoPagador) == 14)?'02':'01'; // Cedente

		$comando                   = '01'; // 01 - Registro de títulos / 02 - Solicitação de baixa / 03 - Pedido de débito em conta  Etc.. 
	
					

			$dados['IdentificacaoRegistro']  	 = '7'; // Identificação do Registro Detalhe: 7 (sete)
			$dados['TipoInscricaoCedente']  	 =  $tipoInscricaoPagador; //Tipo de Inscrição do Cedente (01 - CPF 02 - CNPJ)
			$dados['NumeroCPF_CNPJCedente'] 	 =  str_pad($resultado->BOLE_CNPJCPFCedente,14,0,STR_PAD_LEFT); // Número do CPF/CNPJ do Cedente

			$dados['PrefixoAgencia'] 			 =  str_pad((integer)$resultado->BOLE_Agencia,4,0,STR_PAD_LEFT); // Prefixo da Agência: Número da Agência onde está cadastrado o convênio líder do cedente - NOTA 02
		    $dados['DigitoVerificadorAgencia']   =  str_pad($resultado->BOLE_DigAgencia,1,chr(32),STR_PAD_RIGHT);// Dígito Verificador - D.V. - do Prefixo da Agência.		    
		    $dados['NumeroContaCorrente']        = str_pad((integer)$resultado->BOLE_Conta,8,0,STR_PAD_LEFT);// Número da Conta Corrente: Número da conta onde está cadastrado o Convênio Líder do Cedente
		    $dados['NumeroDigContaCorrente']     = str_pad($resultado->BOLE_DigConta,1,chr(32),STR_PAD_RIGHT); //Dígito Verificador - D.V. – do Número da Conta Corrente do Cedente

		    $dados['NumeroConvenioCobrancaCedente'] = str_pad($resultado->BOLE_NumeroConvenio,7,0,STR_PAD_LEFT);  // Número do Convênio de Cobrança do Cedente

		    $dados['CodigoControleEmpresa']  	 = str_pad(substr($resultado->BOLE_Titulo,0,25),25,chr(32),STR_PAD_RIGHT);// Código de Controle da Empresa

		    $dados['NossoNumero']                = str_pad($resultado->BOLE_Sequencial,17,0,STR_PAD_LEFT);// Nosso-Número

		    // NOSSO-NÚMERO/DV:
			// a) CARTEIRAS 11, 31 e 51:
			// Preencher com zeros
			// b) CARTEIRAS 12, 15 e 17:
			// I - Se numeração a cargo do Banco: Preencher com zeros
			// II - Se numeração a cargo da empresa: Preencher da seguinte forma:
			// - Posição 064 a 070 – Número do Convênio
			// - Posição 071 a 080 – Número seqüencial a partir de 0000000001, não sendo admitida
			// reutilização ou duplicidade.
			// Observações:
			// Não há DV - Dígito Verificador - para o Nosso-Número, quando o número convênio de cobrança for acima de 1.000.000 (um milhão).



		    $dados['NumeroPrestacao']                     = '00';   // Número da Prestação: “00” (Zeros)
		    $dados['GrupoValor']                          = '00';// Grupo de Valor: “00” (Zeros)
		    $dados['ComplementoRegistro13']      		  = str_repeat(chr(32),3); // Complemento do Registro: “Brancos”
            $dados['IndicativoMensagemSacadorAvalista']   =  str_repeat(chr(32),1);  // Indicativo de Mensagem ou Sacador/Avalista
            $dados['PrefixoTitulo']                       = str_repeat(chr(32),3); // Prefixo do Título: “Brancos”
            $dados['VariacaoCarteira']                    = '027';// Variação da Carteira: Os dados necessários para preenchimento desses campos serão fornecidos pelo Banco do Brasil.  BISA = 1
            $dados['ContaCaucao']                         = '0';  // Conta Caução: “0” (Zero)
            $dados['NumeroBordero']                       = '000000'; // Número do Borderô: “000000” (Zeros)
            $dados['TipoCobranca']                        = str_repeat(chr(32),5); // Tipo de Cobrança {04DSC - 08VDR - 02VIN ou Vazio}
			$dados['CarteiraCobranca']                    = $resultado->BOLE_NumCarteira;  // Carteira de Cobrança = 11,17 <-> 12, 31, 51 
			$dados['Comando']                             = $comando; // Comando
			$dados['NumeroTituloAtribuidoCedente']        = str_pad($resultado->BOLE_Sequencial,10,0,STR_PAD_LEFT);	// Seu Número/Número do Título Atribuído pelo Cedente
			$dados['DataVencimento']                      = date('dmy',strtotime($resultado->BOLE_Vencimento)); // Data de Vencimento
			$dados['ValorTitulo']                         = str_pad(str_replace('.', ',', $resultado->BOLE_ValorDocumento),13,0,STR_PAD_LEFT); // Valor do Título
			$dados['NumeroBanco']                         = '001'; // Número do Banco: “001”
			$dados['PrefixoAgenciaCobradora']             = str_pad((integer)$resultado->BOLE_Agencia,4,0,STR_PAD_LEFT); // Prefixo da Agência Cobradora: “0000”
			$dados['DigVerificadorPrefixoAgenciaCobradora'] = str_pad($resultado->BOLE_DigAgencia,1,chr(32),STR_PAD_RIGHT);  // Dígito Verificador do Prefixo da Agência Cobradora: “Brancos”
			$dados['EspecieTitulo'] 					  = '01'; // Espécie de Titulo (01 - Duplicata Mercantil / 02 - Nota Promissória / 03 - Nota de Seguro / 05 – Recibo /08 - Letra de)
			$dados['AceiteTitulo'] 						  =  'N';// Aceite do Título: (N - Sem aceite / A - Com aceite) 
			$dados['DataEmissao'] 						  =  date('dmy',strtotime($resultado->BOLE_DataProc)); // Data de Emissão: Informe no formato “DDMMAA”

			$dados['InstrucaoCodificada31'] 			  = '07';  // Instrução Codificada - NOTAS 09 (07 - Não protestar)
			$dados['InstrucaoCodificada32'] 			  = '07';  // Instrução Codificada - NOTAS 09 (07 - Não protestar)

			$dados['JurosMoraDiaAtraso'] 				  = '0000000000,00'; // Juros de Mora por Dia de Atraso
			$dados['DataLimiteConcessaoDesconto'] 		  = '000000'; // Data Limite para Concessão de Desconto/Data de Operação do BBVendor/Juros de Mora.
			$dados['ValorDesconto'] 			     	  = '0000000000,00'; // Valor do Desconto
			$dados['ValorIOFUnidadeVariavel'] 		   	  = '0000000000,00'; // Valor do IOF/Qtde Unidade Variável.
			$dados['ValorAbatimento'] 			     	  = '0000000000,00'; // Valor do Abatimento
			$dados['TipoInscricaoSacado'] 			      = $tipoInscricaoSacador; // Tipo de Inscrição do Sacado			
			$dados['NumeroCNPJ_CPFSacado'] 			      = str_pad($documentoSacador,14,0,STR_PAD_LEFT);// Número do CNPJ ou CPF do Sacado
			$dados['NomeSacado'] 		         	      = str_pad(substr($resultado->BOLE_NomeCliente,0,37),37,chr(32),STR_PAD_RIGHT); // Nome do Sacado
			$dados['ComplementoRegistro'] 		  	      = str_repeat(chr(32),3); // Complemento do Registro: “Brancos”
			$dados['EnderecoSacado'] 		  	          = str_pad(substr($resultado->BOLE_EndeCliente,0,40),40,chr(32),STR_PAD_RIGHT); // Endereço do Sacado
			$dados['BairroSacado'] 		  	              = str_pad(substr($resultado->BOLE_Bairro,0,12),12,chr(32),STR_PAD_RIGHT); // Bairro do Sacado
			$dados['CepSacado'] 		  	              = str_pad($resultado->BOLE_CEP,8,0,STR_PAD_LEFT); // CEP do Endereço do Sacado
			$dados['CidadeSacado'] 		  	              = str_pad(substr($resultado->BOLE_Cidade,0,15),15,chr(32),STR_PAD_RIGHT);  // Cidade do Sacado
			$dados['UfSacado'] 	      	  	              = str_pad(substr($resultado->BOLE_UF,0,2),2,chr(32),STR_PAD_RIGHT); // UF da Cidade do Sacado
			$dados['Observacoes'] 	  	  	              = str_pad(substr($resultado->BOLE_InstrucaoBoleta,0,40),40,chr(32),STR_PAD_RIGHT); // Observações/Mensagem ou Sacador/Avalista
			$dados['NumeroDiasProtesto'] 	  	  	      = str_repeat(chr(32),2);// Número de Dias Para Protesto
			$dados['ComplementoRegistro49']  	  	      = str_repeat(chr(32),1); // Complemento do Registro: “Brancos”
			$dados['SequencialRegistro']  	     	      = str_pad($sequenciaRegistro,6,0,STR_PAD_LEFT); // Seqüencial de Registro


		   // v($dados);

			$conteudo = $dados['IdentificacaoRegistro']		 		.
						$dados['TipoInscricaoCedente']  	 		.
						$dados['NumeroCPF_CNPJCedente'] 	 		.
						$dados['PrefixoAgencia'] 			 		.
						$dados['DigitoVerificadorAgencia']   		.
						$dados['NumeroContaCorrente']        		.
						$dados['NumeroDigContaCorrente']     		  .
						$dados['NumeroConvenioCobrancaCedente']       . 
						$dados['CodigoControleEmpresa']  	          .
						$dados['NossoNumero']                         .
						$dados['NumeroPrestacao']                     .
						$dados['GrupoValor']                          .
						$dados['ComplementoRegistro13']      		  .
						$dados['IndicativoMensagemSacadorAvalista']   .
						$dados['PrefixoTitulo']                       .
						$dados['VariacaoCarteira']                    .
						$dados['ContaCaucao']                         .
						$dados['NumeroBordero']                       .
						$dados['TipoCobranca']                        .
						$dados['CarteiraCobranca']                    .
						$dados['Comando']                             .
						$dados['NumeroTituloAtribuidoCedente']        .
						$dados['DataVencimento']                      .
						$dados['ValorTitulo']                         .
						$dados['NumeroBanco']                         .
						$dados['PrefixoAgenciaCobradora']             .
						$dados['DigVerificadorPrefixoAgenciaCobradora'] .
						$dados['EspecieTitulo'] 						.
						$dados['AceiteTitulo'] 						  .
						$dados['DataEmissao'] 						  .
						$dados['InstrucaoCodificada31'] 			  .
						$dados['InstrucaoCodificada32'] 			  .
						$dados['JurosMoraDiaAtraso'] 				  .
						$dados['DataLimiteConcessaoDesconto'] 		  .
						$dados['ValorDesconto'] 			     	  .
						$dados['ValorIOFUnidadeVariavel'] 		   	  .
						$dados['ValorAbatimento'] 			     	  .
						$dados['TipoInscricaoSacado'] 			      .
						$dados['NumeroCNPJ_CPFSacado'] 			      .
						$dados['NomeSacado'] 		         	      .
						$dados['ComplementoRegistro'] 		  	      .
						$dados['EnderecoSacado'] 		  	          .
						$dados['BairroSacado'] 		  	              .
						$dados['CepSacado'] 		  	              .
						$dados['CidadeSacado'] 		  	              .
						$dados['UfSacado'] 	      	  	              .
						$dados['Observacoes'] 	  	  	              .
						$dados['NumeroDiasProtesto'] 	  	  	      .
						$dados['ComplementoRegistro49']  	  	      .
						$dados['SequencialRegistro']  	     	      .PHP_EOL;


	

		return $conteudo;

	}


	public function trailerHeaderArquivoRemessaBB400($resultado,$registrosLote)

	{

		$nsa = $this->Boletos_model->pegarSequenciaCliente($resultado->CLIE_codigo);

		$dados['IdentificacaoRegistroTrailer'] 	      = '9'; // Identificação do Registro Trailer: “9
		$dados['ComplementoRegistro']  	   			  = str_repeat(chr(32),393); // Complemento do Registro: “Brancos” 
		$dados['NumeroSequencialRegistroArquivo'] 	  = str_pad($nsa,6,0,STR_PAD_LEFT); // Número Seqüencial do Registro no Arquivo 


		$conteudo = $dados['IdentificacaoRegistroTrailer']  .
					$dados['ComplementoRegistro']  	   		.
					$dados['NumeroSequencialRegistroArquivo'].PHP_EOL;

		return $conteudo;

	}


	public function gerarChaveOab()
	{
		$this->Boletos_model->gerarChaveOab();
	}

	public function uploadRetorno() {
		$arquivo    = $_FILES['arquivo'];
		$codigo_cliente    = $this->input->post('codigo_cliente');
		if (move_uploaded_file($arquivo['tmp_name'], 'assets/arquivos/retorno/'.$codigo_cliente.'.ret'))
			return true;
		else
			return false;
	}

	


}

/* End of file Boletos.php */
/* Location: ./application/controllers/Boletos.php */
